package internal

import (
	"fmt"
	"io"
	"os"
	"runtime/debug"
	"strings"
	"time"

	"github.com/rs/zerolog"
	"github.com/tendermint/tendermint/libs/log"
)

func NewLogger() log.Logger {
	var w io.Writer = &zerolog.ConsoleWriter{
		Out:        os.Stderr,
		TimeFormat: time.RFC3339,
		// PartsExclude: []string{"time"},
		FormatLevel: func(i interface{}) string {
			if ll, ok := i.(string); ok {
				return strings.ToUpper(ll)
			}
			return "????"
		},
	}

	zl := zerolog.New(w).Level(zerolog.InfoLevel).With().Timestamp().Logger()
	return &TendermintZeroLogger{zl, false}
}

type TendermintZeroLogger struct {
	Zerolog zerolog.Logger
	Trace   bool
}

func (l *TendermintZeroLogger) recover() {
	r := recover()
	if r == nil {
		return
	}

	l.Zerolog.Error().Err(fmt.Errorf("%v", r)).Str("stack", string(debug.Stack())).Msg("Panicked while logging")
}

func (l *TendermintZeroLogger) Info(msg string, keyVals ...interface{}) {
	defer l.recover()
	l.Zerolog.Info().Fields(getLogFields(keyVals...)).Msg(msg)
}

func (l *TendermintZeroLogger) Error(msg string, keyVals ...interface{}) {
	defer l.recover()
	e := l.Zerolog.Error()
	if l.Trace {
		e = e.Stack()
	}

	e.Fields(getLogFields(keyVals...)).Msg(msg)
}

func (l *TendermintZeroLogger) Debug(msg string, keyVals ...interface{}) {
	defer l.recover()
	l.Zerolog.Debug().Fields(getLogFields(keyVals...)).Msg(msg)
}

func (l *TendermintZeroLogger) With(keyVals ...interface{}) log.Logger {
	return &TendermintZeroLogger{
		Zerolog: l.Zerolog.With().Fields(getLogFields(keyVals...)).Logger(),
		Trace:   l.Trace,
	}
}

func getLogFields(keyVals ...interface{}) map[string]interface{} {
	if len(keyVals)%2 != 0 {
		return nil
	}

	fields := make(map[string]interface{}, len(keyVals))
	for i := 0; i < len(keyVals); i += 2 {
		fields[fmt.Sprint(keyVals[i])] = keyVals[i+1]
	}

	return fields
}
