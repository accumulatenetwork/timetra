// File holds interfaces and constants

package app

import (
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
)

const ( // Account Types
	Data = "Data"

	SecondsPerMajor = 5
)

type Accumulate interface {
	Run()                                                          // Start the monitor (or simulation)
	GetParameters() (*Parameters, error)                           // Get Staking App parameters from the protocol
	GetBlock(index int64, accounts map[string]int) (*Block, error) // Get the Major Block at the given index, and return account info
	GetTokensIssued() (int64, error)                               // Return the Acme Tokens Issued
	TokensIssued(int64)                                            // Report tokens issued. Simulator needs this, not the protocol
}

// Important Staking URLs.
var ServiceADI,
	ServiceKeyBook,
	RequestsDataAccount,
	StateDataAccount,
	ServiceValidatorLiveAccount,
	DisputeAccount,
	Approved,
	Registered,
	Operators,
	ParametersUrl *url.URL

func init() {
	ServiceADI = url.MustParse("daggit.acme")
	ServiceKeyBook = ServiceADI.JoinPath("ServiceKeyBook")
	RequestsDataAccount = ServiceADI.JoinPath("Requests")
	StateDataAccount = ServiceADI.JoinPath("State")
	ServiceValidatorLiveAccount = ServiceADI.JoinPath("Live")
	DisputeAccount = ServiceADI.JoinPath("Disputes")
	Approved = ServiceADI.JoinPath("Approved")
	Registered = ServiceADI.JoinPath("Registered")
	ParametersUrl = ServiceADI.JoinPath("Parameters")
	Operators = ServiceADI.JoinPath("Operators")
}
