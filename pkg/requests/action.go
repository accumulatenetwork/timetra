package requests

import (
	"bytes"
	"encoding/json"
	"strings"

	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
)

// ParseActions parses actions as key=value pairs.
func ParseActions(parts [][]byte) ([]Action, error) {
	var raw []map[string]any
	for i, part := range parts {
		// If there's no '=', parse it as an action type
		parts := bytes.SplitN(part, []byte{'='}, 2)
		if len(parts) == 1 || bytes.EqualFold(parts[0], []byte("actionType")) {
			typ, ok := ActionTypeByName(string(part))
			if !ok {
				return nil, errors.BadRequest.WithFormat("part %d %q is not a supported action type", i, part)
			}
			raw = append(raw, map[string]any{"actionType": typ})
			continue
		}

		// Parse the field
		if len(raw) == 0 {
			return nil, errors.BadRequest.WithFormat("part %d %q is not an action type", i, part)
		}

		fields := raw[len(raw)-1]
		field := strings.ToLower(string(parts[0]))
		value := string(parts[1])

		// All of the (current) action fields are marshalled as strings
		// (including bigints) so don't parse the value
		fields[field] = value
	}

	actions := make([]Action, len(raw))
	for i, fields := range raw {
		// Create a new action from the action type
		act, err := NewAction(fields["actionType"].(ActionType))
		if err != nil {
			return nil, errors.InternalError.Wrap(err)
		}

		// This is hacky but it avoids having to write a bunch of custom
		// marshalling code
		b, err := json.Marshal(fields)
		if err != nil {
			return nil, errors.InternalError.WithFormat("cannot encode request")
		}

		// Unmarshal and reject unknown fields
		dec := json.NewDecoder(bytes.NewBuffer(b))
		dec.DisallowUnknownFields()
		err = dec.Decode(act)
		if err != nil {
			return nil, errors.BadRequest.With("decode request: %w", err)
		}

		actions[i] = act
	}

	// If an action references account, propagate that account to later requests if they don't specify an account
	var lastAccount *url.URL
	for _, a := range actions {
		switch a := a.(type) {
		case *AddAccount:
			propagateAccount(&lastAccount, &a.Account)
		//case *ChangeDelegate:
		//	propagateAccount(&lastAccount, &a.Account)
		//case *ChangePayout:
		//	propagateAccount(&lastAccount, &a.Account)
		//case *ChangeType:
		//	propagateAccount(&lastAccount, &a.Account)
		//case *TransferTokens:
		//	propagateAccount(&lastAccount, &a.Account)
		case *WithdrawTokens:
			propagateAccount(&lastAccount, &a.Account)

		default:
			// If the action is not something that references an account, stop
			// propagating
			lastAccount = nil
		}
	}

	return actions, nil
}

func propagateAccount(last, this **url.URL) {
	if *this == nil {
		*this = *last
	} else {
		*last = *this
	}
}
