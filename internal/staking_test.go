package internal_test

import (
	"context"
	"crypto/ed25519"
	"encoding/json"
	"math/big"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/accumulatenetwork/accumulate/exp/light"
	"gitlab.com/accumulatenetwork/accumulate/pkg/build"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/network"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	. "gitlab.com/accumulatenetwork/accumulate/protocol"
	. "gitlab.com/accumulatenetwork/accumulate/test/harness"
	. "gitlab.com/accumulatenetwork/accumulate/test/helpers"
	"gitlab.com/accumulatenetwork/accumulate/test/simulator"
	acctesting "gitlab.com/accumulatenetwork/accumulate/test/testing"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/cmd/daggit/app"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/internal"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/internal/testutils"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/pkg/types"
)

func init() {
	acctesting.EnableDebugFeatures()
}

func TestStakingWithdrawBeforeEpoch(t *testing.T) {
	var timestamp uint64 = 1
	const initBalance = 10
	alice := AccountUrl("alice")
	aliceKey := acctesting.GenerateKey(alice)
	bob := acctesting.AcmeLiteAddressStdPriv(acctesting.GenerateKey("lite"))
	params := new(app.Parameters)
	params.Init()
	params.FullStakeMinimum = new(big.Int)
	sim, _, valKeys := initForTest(t, params, alice, aliceKey, initBalance)

	// Major block(s)
	sim.StepUntilN(1000, testutils.MajorBlock(1))

	// Withdraw
	st := sim.BuildAndSubmitTxnSuccessfully(
		build.Transaction().For(alice, "stake").
			SendTokens(1, AcmePrecisionPower).To(bob).
			SignWith(params.Account.Validators).Version(1).Timestamp(&timestamp).PrivateKey(valKeys[0]).
			SignWith(params.Account.Validators).Version(1).PrivateKey(valKeys[1]))

	sim.StepUntil(
		Txn(st.TxID).Completes())

	// Major block(s)
	sim.StepUntilN(1000, testutils.MajorBlock(2))
	sim.StepUntilN(1000, testutils.MajorBlock(3))
	sim.StepUntilN(1000, testutils.MajorBlock(4))

	// Verify a withdraw that occurs prior to the period affects the balance
	account := runStaking(t, sim, params)[0]
	requireAcmeEqual(t, initBalance-1, account.Balance)
	requireAcmeEqual(t, 0, account.Withdraws)
}

func TestStakingWithdrawDuringEpoch(t *testing.T) {
	var timestamp uint64 = 1
	const initBalance = 10
	alice := AccountUrl("alice")
	aliceKey := acctesting.GenerateKey(alice)
	bob := acctesting.AcmeLiteAddressStdPriv(acctesting.GenerateKey("lite"))
	params := new(app.Parameters)
	params.Init()
	params.FullStakeMinimum = new(big.Int)
	sim, _, valKeys := initForTest(t, params, alice, aliceKey, initBalance)

	// Major block(s)
	sim.StepUntilN(1000, testutils.MajorBlock(1))
	sim.StepUntilN(1000, testutils.MajorBlock(2))
	sim.StepUntilN(1000, testutils.MajorBlock(3))

	// Withdraw
	st := sim.BuildAndSubmitTxnSuccessfully(
		build.Transaction().For(alice, "stake").
			SendTokens(1, AcmePrecisionPower).To(bob).
			SignWith(params.Account.Validators).Version(1).Timestamp(&timestamp).PrivateKey(valKeys[0]).
			SignWith(params.Account.Validators).Version(1).PrivateKey(valKeys[1]))

	sim.StepUntil(
		Txn(st.TxID).Completes())

	// Major block(s)
	sim.StepUntilN(1000, testutils.MajorBlock(4))

	// Verify a withdraw that occurs during the period affects the balance
	account := runStaking(t, sim, params)[0]
	requireAcmeEqual(t, initBalance-1, account.Balance)
	requireAcmeEqual(t, 1, account.Withdraws)
}

func TestStakingWithdrawAfterEpoch(t *testing.T) {
	var timestamp uint64 = 1
	const initBalance = 10
	alice := AccountUrl("alice")
	aliceKey := acctesting.GenerateKey(alice)
	bob := acctesting.AcmeLiteAddressStdPriv(acctesting.GenerateKey("lite"))
	params := new(app.Parameters)
	params.Init()
	params.FullStakeMinimum = new(big.Int)
	sim, _, valKeys := initForTest(t, params, alice, aliceKey, initBalance)

	// Major block(s)
	sim.StepUntilN(1000, testutils.MajorBlock(1))
	sim.StepUntilN(1000, testutils.MajorBlock(2))
	sim.StepUntilN(1000, testutils.MajorBlock(3))
	sim.StepUntilN(1000, testutils.
		MajorBlock(4))

	// Withdraw
	st := sim.BuildAndSubmitTxnSuccessfully(
		build.Transaction().For(alice, "stake").
			SendTokens(1, AcmePrecisionPower).To(bob).
			SignWith(params.Account.Validators).Version(1).Timestamp(&timestamp).PrivateKey(valKeys[0]).
			SignWith(params.Account.Validators).Version(1).PrivateKey(valKeys[1]))

	sim.StepUntil(
		Txn(st.TxID).Completes())

	// Major block(s)
	sim.StepUntilN(1000, testutils.MajorBlock(5))

	// Verify a withdraw that occurs after the period does *not* affect the balance
	account := runStaking(t, sim, params)[0]
	requireAcmeEqual(t, initBalance, account.Balance)
	requireAcmeEqual(t, 0, account.Withdraws)
}

func TestStakingDepositBeforeEpoch(t *testing.T) {
	var timestamp uint64 = 1
	const initBalance = 10
	alice := AccountUrl("alice")
	aliceKey := acctesting.GenerateKey(alice)
	bobKey := acctesting.GenerateKey("lite")
	bob := acctesting.AcmeLiteAddressStdPriv(bobKey)
	params := new(app.Parameters)
	params.Init()
	params.FullStakeMinimum = new(big.Int)
	sim, _, _ := initForTest(t, params, alice, aliceKey, initBalance)

	MakeAccount(t, sim.DatabaseFor(bob),
		&LiteIdentity{Url: bob.RootIdentity(), CreditBalance: 1e9},
		&LiteTokenAccount{Url: bob, TokenUrl: AcmeUrl(), Balance: *big.NewInt(1e18)})

	// Major block(s)
	sim.StepUntilN(1000, testutils.MajorBlock(1))

	// Deposit
	st := sim.BuildAndSubmitTxnSuccessfully(
		build.Transaction().For(bob).
			SendTokens(1, AcmePrecisionPower).To(alice, "stake").
			SignWith(bob).Version(1).Timestamp(&timestamp).PrivateKey(bobKey))

	sim.StepUntil(
		Txn(st.TxID).Completes())

	// Major block(s)
	sim.StepUntilN(1000, testutils.MajorBlock(2))
	sim.StepUntilN(1000, testutils.MajorBlock(3))
	sim.StepUntilN(1000, testutils.MajorBlock(4))

	// Verify a deposit that occurs prior to the period affects the balance
	account := runStaking(t, sim, params)[0]
	requireAcmeEqual(t, initBalance+1, account.Balance)
	requireAcmeEqual(t, 0, account.Deposits)
}

func TestStakingDepositDuringEpoch(t *testing.T) {
	var timestamp uint64 = 1
	const initBalance = 10
	alice := AccountUrl("alice")
	aliceKey := acctesting.GenerateKey(alice)
	bobKey := acctesting.GenerateKey("lite")
	bob := acctesting.AcmeLiteAddressStdPriv(bobKey)
	params := new(app.Parameters)
	params.Init()
	params.FullStakeMinimum = new(big.Int)
	sim, _, _ := initForTest(t, params, alice, aliceKey, initBalance)

	MakeAccount(t, sim.DatabaseFor(bob),
		&LiteIdentity{Url: bob.RootIdentity(), CreditBalance: 1e9},
		&LiteTokenAccount{Url: bob, TokenUrl: AcmeUrl(), Balance: *big.NewInt(1e18)})

	// Major block(s)
	sim.StepUntilN(1000, testutils.MajorBlock(1))
	sim.StepUntilN(1000, testutils.MajorBlock(2))
	sim.StepUntilN(1000, testutils.MajorBlock(3))

	// Deposit
	st := sim.BuildAndSubmitTxnSuccessfully(
		build.Transaction().For(bob).
			SendTokens(1, AcmePrecisionPower).To(alice, "stake").
			SignWith(bob).Version(1).Timestamp(&timestamp).PrivateKey(bobKey))

	sim.StepUntil(
		Txn(st.TxID).Completes())

	// Major block(s)
	sim.StepUntilN(1000, testutils.MajorBlock(4))

	// Verify a deposit that occurs during the period does *not* affect the balance
	account := runStaking(t, sim, params)[0]
	requireAcmeEqual(t, initBalance, account.Balance)
	requireAcmeEqual(t, 1, account.Deposits)
}

func TestStakingDepositAfterEpoch(t *testing.T) {
	var timestamp uint64 = 1
	const initBalance = 10
	alice := AccountUrl("alice")
	aliceKey := acctesting.GenerateKey(alice)
	bobKey := acctesting.GenerateKey("lite")
	bob := acctesting.AcmeLiteAddressStdPriv(bobKey)
	params := new(app.Parameters)
	params.Init()
	params.FullStakeMinimum = new(big.Int)
	sim, _, _ := initForTest(t, params, alice, aliceKey, initBalance)

	MakeAccount(t, sim.DatabaseFor(bob),
		&LiteIdentity{Url: bob.RootIdentity(), CreditBalance: 1e9},
		&LiteTokenAccount{Url: bob, TokenUrl: AcmeUrl(), Balance: *big.NewInt(1e18)})

	// Major block(s)
	sim.StepUntilN(1000, testutils.MajorBlock(1))
	sim.StepUntilN(1000, testutils.MajorBlock(2))
	sim.StepUntilN(1000, testutils.MajorBlock(3))
	sim.StepUntilN(1000, testutils.MajorBlock(4))

	// Deposit
	st := sim.BuildAndSubmitTxnSuccessfully(
		build.Transaction().For(bob).
			SendTokens(1, AcmePrecisionPower).To(alice, "stake").
			SignWith(bob).Version(1).Timestamp(&timestamp).PrivateKey(bobKey))

	sim.StepUntil(
		Txn(st.TxID).Completes())

	// Major block(s)
	sim.StepUntilN(1000, testutils.MajorBlock(5))

	// Verify a deposit that occurs after the period does *not* affect the balance
	account := runStaking(t, sim, params)[0]
	requireAcmeEqual(t, initBalance, account.Balance)
	requireAcmeEqual(t, 0, account.Deposits)
}

func initForTest(t testing.TB, params *app.Parameters, alice *url.URL, aliceKey ed25519.PrivateKey, initBalance int64) (_ *Sim, stakingKey ed25519.PrivateKey, valKeys []ed25519.PrivateKey) {
	g := new(network.GlobalValues)
	g.ExecutorVersion = ExecutorVersionV1
	g.Globals = new(NetworkGlobals)
	g.Globals.MajorBlockSchedule = "* * * * *"
	g.Routing = new(RoutingTable)
	g.Routing.Overrides = []RouteOverride{{
		Account:   params.ADI.Service,
		Partition: Directory,
	}}

	// Initialize the simulator
	sim := NewSim(t,
		simulator.MemoryDatabase,
		simulator.SimpleNetwork(t.Name(), 1, 1),
		simulator.GenesisWith(GenesisTime, g),
	)

	var err error
	// Set up staking
	stakingWallet, valWallets := testutils.InitDaggits(t, sim, params, 2)
	require.NoError(t, stakingWallet.Open())
	stakingKey, _, err = stakingWallet.GetKey()
	require.NoError(t, err)
	valKeys = make([]ed25519.PrivateKey, len(valWallets))
	for i, wallet := range valWallets {
		require.NoError(t, wallet.Open())
		valKeys[i], _, err = wallet.GetKey()
		require.NoError(t, err)
	}

	// Set up alice
	MakeIdentity(t, sim.DatabaseFor(alice), alice, aliceKey[32:])
	CreditCredits(t, sim.DatabaseFor(alice), alice.JoinPath("book", "1"), 1e9)
	MakeAccount(t, sim.DatabaseFor(alice), &TokenAccount{Url: alice.JoinPath("stake"), TokenUrl: AcmeUrl(), Balance: *big.NewInt(initBalance * AcmePrecision)})

	// Register stake
	UpdateAccount(t, sim.DatabaseFor(alice), alice.JoinPath("stake"), func(t *TokenAccount) {
		t.Authorities[0].Disabled = true
		t.AddAuthority(params.Account.Authority)
	})
	b, err := json.Marshal(&types.Identity{
		Identity: alice,
		Accounts: []*types.Account{{
			Type:   types.AccountTypeCoreValidator,
			Url:    alice.JoinPath("stake"),
			Payout: alice.JoinPath("stake"),
		}},
	})
	require.NoError(t, err)
	st := sim.BuildAndSubmitTxnSuccessfully(
		build.Transaction().For(params.Account.RegisteredADIs).
			WriteData(b).
			SignWith(params.Account.Validators).Version(1).Timestamp(1).PrivateKey(valKeys[0]).
			SignWith(params.Account.Validators).Version(1).Timestamp(1).PrivateKey(valKeys[1]))

	sim.StepUntil(
		Txn(st.TxID).Completes())

	return sim, stakingKey, valKeys
}

func runStaking(t testing.TB, sim *Sim, params *app.Parameters) []*internal.StakingInfo {
	sim.StepN(10)
	lc, err := light.NewClient(
		light.Logger(acctesting.NewTestLogger(t)),
		light.MemoryStore(),
		light.ClientV2(sim.S.ClientV2(Directory)),
	)
	require.NoError(t, err)
	require.NoError(t, internal.PullStaking(context.Background(), lc, params))
	accounts, _, err := internal.Staking(lc, params, [2]uint64{2, 4}, new(big.Rat))
	require.NoError(t, err)
	require.Len(t, accounts, 1)
	return accounts
}

func requireAcmeEqual(t testing.TB, expect uint64, actual *big.Int) {
	t.Helper()
	require.Equal(t, FormatAmount(expect*AcmePrecision, AcmePrecisionPower), FormatBigAmount(actual, AcmePrecisionPower))
}
