package fsm

import (
	"gitlab.com/accumulatenetwork/accumulate/pkg/build"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/vdk/node"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/internal"
)

var _ StateDelegate = (*processFinalization)(nil)

type processFinalization struct {
}

func (s *processFinalization) execute(ctx *StateContext, d *node.DidCommitBlock) StateDelegate {
	//sign the finalization entry
	key, keyType, err := ctx.Executor.Wallet.GetLockedKey()
	if err != nil {
		return ctx.Fatal(errors.FatalError).WithFormat("failed to get key in process finalization %v", err)
	}
	defer key.Destroy()

	env, err := build.SignatureForTransaction(ctx.toFinalize).
		Url(ctx.Executor.Params.Account.Validators).
		Version(ctx.validators.Version).Timestamp(&ctx.timestamp).
		Type(keyType).Signer(internal.SecureSigner(key)).
		Done()

	if err != nil {
		return ctx.Fatal(errors.FatalError).WithFormat("failed to dispatch request %v", err)
	}

	ctx.Executor.Logger.Info("Signed request", "txid", ctx.toFinalize.ID())

	//submit the request
	err = ctx.Executor.dispatchTransaction(env)
	if err != nil {
		return ctx.Fatal(errors.FatalError).WithFormat("failed to dispatch request %v", err)
	}

	return ctx.MakeCurrent(StateDidProcessFinalization)
}
