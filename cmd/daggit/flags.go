package main

import (
	"fmt"
	"reflect"

	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
)

type urlFlag struct {
	v **url.URL
}

func (f urlFlag) Type() string   { return "acc-url" }
func (f urlFlag) String() string { return fmt.Sprint(*f.v) }
func (f urlFlag) Set(s string) error {
	u, err := url.Parse(s)
	if err != nil {
		return err
	}
	*f.v = u
	return nil
}

type enumFlag[T any] struct {
	V *T
	F func(string) (T, bool)
}

func NewEnum[T any](v *T, f func(string) (T, bool)) *enumFlag[T] {
	return &enumFlag[T]{v, f}
}

func (f *enumFlag[T]) String() string {
	return fmt.Sprint(*f.V)
}

func (f *enumFlag[T]) Set(s string) error {
	var ok bool
	*f.V, ok = f.F(s)
	if !ok {
		return fmt.Errorf("%q is not a valid %s", s, f.Type())
	}
	return nil
}

func (f *enumFlag[T]) Type() string {
	return reflect.TypeOf(new(T)).Elem().Name()
}
