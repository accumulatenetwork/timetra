package main

import (
	"context"
	"log"
	"os/user"
	"path/filepath"
	"time"

	"github.com/fatih/color"
	"github.com/kardianos/service"
	"github.com/spf13/cobra"
	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/accumulate/vdk/logger"
	node2 "gitlab.com/accumulatenetwork/accumulate/vdk/node"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/cmd/daggit/node"
)

var currentUser = func() *user.User {
	usr, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}
	return usr
}()

var defaultWorkDir = filepath.Join(currentUser.HomeDir, ".accumulate")

var cmdNode = &cobra.Command{
	Use:   "node",
	Short: "Initialize or Run a staking node",
	Args:  cobra.NoArgs,
}

var cmdNodeRun = &cobra.Command{
	Use:   "run",
	Short: "Run a staking node",
	Args:  cobra.NoArgs,
	Run:   runNode,
}

var cmdNodeInit = &cobra.Command{
	Use:   "init",
	Short: "Run a staking node",
	Args:  cobra.NoArgs,
	Run:   initNode,
}
var flagNode node.Options
var CiStopAfter time.Duration
var SeedNode string

func init() {
	cmd.AddCommand(cmdNode)
	cmdNode.AddCommand(cmdNodeInit, cmdNodeRun)
	cmdNode.PersistentFlags().StringVarP(&flagNode.WorkDir, "work-dir", "w", defaultWorkDir, "Working directory for configuration and data")
	cmdNode.PersistentFlags().StringVarP(&flagNode.LogFile, "log", "l", filepath.Join(defaultWorkDir, "staking"), "default log file")
	cmdNode.PersistentFlags().IntVarP(&flagNode.NodeId, "node-id", "n", 0, "node id")
	cmdNode.PersistentFlags().StringVarP(&flagNode.WalletAddr, "wallet-url", "w", "http://localhost:26661", "walletd server address")
	cmdNode.PersistentFlags().StringVarP(&flagNode.KeyName, "key-name", "k", "", "name of key stored in walletd")
	cmdNodeRun.PersistentFlags().DurationVarP(&CiStopAfter, "stop-after", "s", 0, "if non-zero, the service will shutdown after X seconds")
	cmdNodeInit.PersistentFlags().StringVarP(&SeedNode, "seed", "s", "tcp://bvn0-seed.mainnet.accumulatenetwork.io:16592", "seed url of the Directory node")
}

func initNode(*cobra.Command, []string) {
	err := node2.InitializeFollowerFromSeed(flagNode.LogFile, protocol.PartitionTypeDirectory, SeedNode)
	if err != nil {
		log.Fatal(err)
	}
}

var serviceConfig = &service.Config{
	Name:        "staking",
	DisplayName: "Staking",
	Description: cmdNode.Short,
	Arguments:   []string{"run"},
}

func runNode(*cobra.Command, []string) {
	lwc := logger.LogWriterConfig{Service: nil, LogFile: flagNode.LogFile}
	lw, err := logger.NewLogWriter(lwc)
	if err != nil {
		fatalf("cannot create logger %v", err)
	}

	n, err := node2.NewNode(flagNode.WorkDir, lw, flagNode.NodeId)
	if err != nil {
		fatalf("cannot create node %v", err)
	}

	ex, err := node.Init(n, flagNode.WalletAddr, flagNode.KeyName)
	if err != nil {
		fatalf("cannot initialize node %v", err)
		return
	}

	// A queue depth of 600 allows the FSM to lag the core by about 10 minutes
	ch := ex.Start(context.Background(), n.EventBus(), 600, nil)
	go func() {
		for env := range ch {
			res, err := ex.APIv2.ExecuteDirect(context.Background(), &client.ExecuteRequest{Envelope: env})

			// TODO Dying is probably not the right choice here
			check(err)
			if res.Message != "" {
				fatalf(res.Message)
			}
		}
	}()

	color.HiGreen("------ starting staking node ------")

	err = n.Start()
	check(err)
}
