// Copyright 2023 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

package internal

import (
	"fmt"

	"github.com/awnumar/memguard"
	btc "github.com/btcsuite/btcd/btcec"
	"gitlab.com/accumulatenetwork/accumulate/pkg/client/signing"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
)

var _ signing.Signer = (*SecurePrivateKey)(nil)

type SecurePrivateKey struct {
	locked *memguard.LockedBuffer
}

func SecureSigner(locked *memguard.LockedBuffer) signing.Signer {
	return signing.Signer(&SecurePrivateKey{locked})
}

func (k *SecurePrivateKey) SetPublicKey(sig protocol.Signature) error {
	if k.locked == nil {
		return fmt.Errorf("cannot set public key: memory enclave locked buffer key has not been set")
	}

	switch sig := sig.(type) {
	case *protocol.LegacyED25519Signature:
		sig.PublicKey = make([]byte, 32)
		copy(sig.PublicKey, k.locked.Data()[32:])

	case *protocol.ED25519Signature:
		sig.PublicKey = make([]byte, 32)
		copy(sig.PublicKey, k.locked.Data()[32:])

	case *protocol.RCD1Signature:
		sig.PublicKey = make([]byte, 32)
		copy(sig.PublicKey, k.locked.Data()[32:])

	case *protocol.BTCSignature:
		_, pubKey := btc.PrivKeyFromBytes(btc.S256(), k.locked.Data())
		sig.PublicKey = pubKey.SerializeCompressed()

	case *protocol.BTCLegacySignature:
		_, pubKey := btc.PrivKeyFromBytes(btc.S256(), k.locked.Data())
		sig.PublicKey = pubKey.SerializeUncompressed()

	case *protocol.ETHSignature:
		_, pubKey := btc.PrivKeyFromBytes(btc.S256(), k.locked.Data())
		sig.PublicKey = pubKey.SerializeUncompressed()

	default:
		return fmt.Errorf("cannot set the public key on a %T", sig)
	}
	return nil
}

func (k *SecurePrivateKey) Sign(sig protocol.Signature, sigMdHash, message []byte) error {
	if k.locked == nil {
		return fmt.Errorf("cannot sign: memory enclave locked buffer key has not been set")
	}

	var err error
	switch sig := sig.(type) {
	case *protocol.LegacyED25519Signature:
		protocol.SignLegacyED25519(sig, k.locked.Data(), sigMdHash, message)

	case *protocol.ED25519Signature:
		protocol.SignED25519(sig, k.locked.Data(), sigMdHash, message)

	case *protocol.RCD1Signature:
		protocol.SignRCD1(sig, k.locked.Data(), sigMdHash, message)

	case *protocol.BTCSignature:
		err = protocol.SignBTC(sig, k.locked.Data(), sigMdHash, message)

	case *protocol.BTCLegacySignature:
		err = protocol.SignBTCLegacy(sig, k.locked.Data(), sigMdHash, message)

	case *protocol.ETHSignature:
		err = protocol.SignETH(sig, k.locked.Data(), sigMdHash, message)

	default:
		return fmt.Errorf("cannot sign %T with a key", sig)
	}

	if err != nil {
		return err
	}

	//call our version of set public key to ensure public key is replaced with a copy
	return k.SetPublicKey(sig)
}
