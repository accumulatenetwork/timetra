package fsm

import (
	"context"

	"gitlab.com/accumulatenetwork/accumulate/pkg/api/v3"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/accumulate/vdk/node"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/internal"
)

var _ StateDelegate = (*canBegin)(nil)

type canBegin struct {
}

// define state delegates
func (s *canBegin) execute(ctx *StateContext, d *node.DidCommitBlock) StateDelegate {
	// Do we have a new major block?
	if d.Index <= ctx.Executor.PrevBlock.Index || d.Index == 0 {
		return s
	}

	// Do we have a new epoch?
	next := ctx.Executor.PrevEpochBlock.Time.Add(ctx.Executor.Options.Epoch)
	if !d.Time.After(next) {
		return s
	}

	// Pull changes
	err := internal.PullStaking(context.TODO(), ctx.Executor.Light, ctx.Executor.Params)
	if err != nil {
		return ctx.Fatal(errors.UnknownError).WithFormat("pull staking: %w", err)
	}

	// We are entering a new epoch
	ctx.Executor.CurrentEpochBlock = *d

	// Get the anchor hash
	batch := ctx.Executor.Light.OpenDB(false)
	defer batch.Discard()
	hash, err := internal.GetDirectoryAnchor(batch, d.Index)
	if err != nil {
		return ctx.Fatal(errors.UnknownError).WithFormat("get block anchor: %w", err)
	}

	ctx.Executor.CurrentEpochHash = *(*[32]byte)(hash)

	ns, err := ctx.Executor.APIv3.NetworkStatus(context.TODO(), api.NetworkStatusOptions{Partition: protocol.Directory})
	if err != nil {
		return ctx.Fatal(errors.UnknownError).WithFormat("query network description: %w", err)
	}
	ctx.executorVersion = ns.ExecutorVersion

	// Start processing
	return ctx.MakeCurrent(StateSelectLeader)
}
