package types

// Normalize updates an identity to the new multi-account form
func (id *Identity) Normalize() {
	// If Stake is unset, there's nothing to do
	if id.Stake == nil {
		return
	}

	// Wipe the old fields
	orig := id.Copy()
	id.Type = 0
	id.Stake = nil
	id.Rewards = nil
	id.Delegate = nil
	id.Lockup = 0
	id.HardLock = false
	id.AcceptingDelegates = 0

	// If an account exists that matches Stake, do nothing
	for _, a := range id.Accounts {
		if a.Url.Equal(orig.Stake) {
			return
		}
	}

	// Convert the old fields to an account
	a := &Account{
		Type:     orig.Type,
		Url:      orig.Stake,
		Payout:   orig.Rewards,
		Delegate: orig.Delegate,
		Lockup:   orig.Lockup,
		HardLock: orig.HardLock,
	}
	id.Accounts = append(id.Accounts, a)

	switch {
	case id.RejectDelegates || id.DelegatorPayout != nil:
		// Make no change; the identity has been configured to explicitly accept
		// or reject delegates

	case orig.AcceptingDelegates == AcceptingDelegatesUnknown:
		// Make no change; keep the default behavior

	case orig.AcceptingDelegates == AcceptingDelegatesNo:
		id.RejectDelegates = true

	case orig.Rewards != nil:
		id.DelegatorPayout = orig.Rewards

	default:
		id.DelegatorPayout = orig.Stake
	}
}
