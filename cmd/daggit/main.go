package main

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
)

func main() {
	_ = cmd.Execute()
}

var cmd = &cobra.Command{
	Use:   "staking",
	Short: "Run the staking app",
	//	Args:  cobra.NoArgs,
	//	Run:   run,
}

var flag = struct {
	Debug      bool
	Simulator  bool
	Network    string
	Parameters *url.URL
}{
	Parameters: protocol.AccountUrl("staking.acme", "parameters"),
}

var theClient *client.Client

func init() {
	cmd.PersistentFlags().BoolVarP(&flag.Debug, "debug", "d", false, "debug API requests")
	cmd.Flags().BoolVar(&flag.Simulator, "sim", false, "Use the simulator")
	cmd.PersistentFlags().StringVarP(&flag.Network, "net", "n", "https://mainnet.accumulatenetwork.io/v2", "The network to connect to")
	cmd.PersistentFlags().Var(urlFlag{&flag.Parameters}, "params", "Staking parameters account URL")
	cmd.MarkFlagsMutuallyExclusive("sim", "net")

	cmd.PersistentPreRunE = func(*cobra.Command, []string) error {
		if !flag.Simulator {
			var err error
			theClient, err = client.New(flag.Network)
			if err != nil {
				return errors.UnknownError.WithFormat("--net: %w", err)
			}

			theClient.DebugRequest = flag.Debug
		}

		return nil
	}
}

//func run(*cobra.Command, []string) {
//	u, _ := user.Current()
//	app.ReportDirectory = path.Join(u.HomeDir + "/StakingReports")
//	err := os.MkdirAll(app.ReportDirectory, os.ModePerm)
//	check(err)
//
//	s := new(app.StakingApp)
//	if flag.Simulator {
//		sim := new(sim.Simulator)
//		s.Run(sim)
//		return
//	}
//
//	net, err := network.New(flag.Network)
//	check(err)
//
//	if flag.Debug {
//		net.Debug()
//	}
//	s.Run(net)
//}

func fatalf(format string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, "Error: "+format+"\n", args...)
	os.Exit(1)
}

func check(err error) {
	if err != nil {
		fatalf("%v", err)
	}
}

func checkf(err error, format string, otherArgs ...interface{}) {
	if err != nil {
		fatalf(format+": %v", append(otherArgs, err)...)
	}
}
