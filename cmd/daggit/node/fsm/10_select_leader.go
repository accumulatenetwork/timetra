package fsm

import (
	"encoding/binary"
	"math/rand"

	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/accumulate/vdk/node"
)

var _ StateDelegate = (*selectLeader)(nil)

type selectLeader struct {
}

// ShufflePage will take a key page and shuffle the keys within it to randomize the order based upon a seed
func ShufflePage(keys []*protocol.KeySpec, seedHash []byte) {
	//make a seed from the hash
	seed := binary.BigEndian.Uint64(seedHash)
	//set the seed
	rand.Seed(int64(seed))
	//do a little random shuffle on the entries
	rand.Shuffle(len(keys), func(i, j int) { keys[i], keys[j] = keys[j], keys[i] })
}

func (s *selectLeader) execute(ctx *StateContext, d *node.DidCommitBlock) StateDelegate {
	//select a leader, after we have a leader, we can move on...
	//pull the staking page and set as the leader pool
	batch := ctx.Executor.Light.OpenDB(true)
	defer batch.Discard()
	ctx.Executor.Logger.Info("Select leader", "major", d.Major, "minor", d.Index, "time", d.Time.String())
	//get the directory anchor hash to use as the seed

	if ctx.leaderPool == nil {
		//first try so fetch validators
		err := batch.Account(ctx.Executor.Params.Account.Validators).Main().GetAs(&ctx.validators)
		if err != nil {
			//Transition to error state, this is fatal since we cannot continue without it
			return ctx.Fatal(errors.UnknownError).WithFormat("load %v: %w", ctx.Executor.Params.Account.Validators, err)
		}

		//make a copy of the leaders to manipulate order
		ctx.leaderPool = make([]*protocol.KeySpec, 0, len(ctx.validators.Keys))
		for _, entry := range ctx.validators.Keys {
			if entry.PublicKeyHash != nil {
				ctx.leaderPool = append(ctx.leaderPool, entry)
			}
		}

		//randomly shuffle the available candidates in the page
		ShufflePage(ctx.leaderPool, ctx.Executor.CurrentEpochHash[:])
	}

	//TODO: revisit to search delegation path for key
	var self protocol.KeyEntry
	_, self, ctx.AmValidator = ctx.validators.EntryByKeyHash(ctx.Executor.Wallet.GetKeyHash())
	if !ctx.AmValidator {
		return ctx.MakeCurrent(StateProcessUserStakingRequests)
	}

	if ctx.retryAttempts > len(ctx.leaderPool) {
		return ctx.Fatal(errors.UnknownError).WithFormat(
			"retry attempts exceeds available staking validators (%d>%d)",
			ctx.retryAttempts, len(ctx.leaderPool))
	}

	//identify the leader KeySpec
	ctx.leader = ctx.leaderPool[ctx.retryAttempts]

	// Leader selection
	if self == ctx.leader {
		ctx.AmLeader = true
	}

	// Set the timestamp
	ts := uint64(d.Time.UnixMilli())
	if ts <= self.GetLastUsedOn() {
		ts = self.GetLastUsedOn() + 1
	}
	ctx.timestamp = ts

	return ctx.MakeCurrent(StateProcessUserStakingRequests)
}
