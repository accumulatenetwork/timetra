package app

import (
	"math/big"
	"time"

	"gitlab.com/accumulatenetwork/accumulate/protocol"
)

// init()
// Initialize stuff in the package
// Initialize the starting values for Parameters
func (p *Parameters) Init() {
	p.ADI.Service = protocol.AccountUrl(ServiceADI.Authority, ServiceADI.Path)
	p.Account.Parameters = p.ADI.Service.JoinPath("Parameters")
	p.Account.TokenIssuance = protocol.AcmeUrl()
	p.Account.Authority = p.ADI.Service.JoinPath("book")
	p.Account.Validators = p.ADI.Service.JoinPath("book", "2")
	p.Account.Operators = p.ADI.Service.JoinPath("Operators")
	p.Account.Requests = p.ADI.Service.JoinPath("Requests")
	p.Account.Actions = p.ADI.Service.JoinPath("State", "Actions")
	p.Account.Epoch = p.ADI.Service.JoinPath("State", "Epoch")
	p.Account.RegisteredADIs = p.ADI.Service.JoinPath("Registered")
	p.Account.ApprovedADIs = p.ADI.Service.JoinPath("Approved")
	p.Account.Disputes = p.ADI.Service.JoinPath("Disputes")

	p.DelegatedMinimum = big.NewInt(1000 * protocol.AcmePrecision)
	p.FullStakeMinimum = big.NewInt(50_000 * protocol.AcmePrecision)

	t := time.Now().UTC()                            // Get current time in UTC
	hours, minutes, seconds := t.Clock()             // Get our offset into the day, UTC
	t = t.Add(time.Duration(-hours) * time.Hour)     // Back t up by the hours
	t = t.Add(time.Duration(-minutes) * time.Minute) // and by the minutes
	t = t.Add(time.Duration(-seconds) * time.Second) // and by the seconds
	p.StartTime = t                                  // Start time at the start of today
	p.MajorBlockTime = time.Second / 4               // Should be 1 for the real app, ~13000 for testing
	p.PayOutFreq = 4                                 // The frequency at which payouts are made. Only used by simulation
	//p.TokenIssuanceRate = &WeightParameter{Numerator: 16, Denominator: 100} // The percentage of unissued tokens paid out per year (limit - issued)
	//p.DelegationFee = &WeightParameter{Numerator: 1, Denominator: 10}
	//p.StakingWeight = []*WeightParameter{
	//	{Type: types.AccountTypeCoreFollower, Numerator: 13, Denominator: 10},
	//	{Type: types.AccountTypeCoreValidator, Numerator: 13, Denominator: 10},
	//	{Type: types.AccountTypeStakingValidator, Numerator: 13, Denominator: 10},
	//}
}

//func (w *WeightParameter) Float64() float64 {
//	return float64(w.Numerator) / float64(w.Denominator)
//}
//
//func (w *WeightParameter) BigRat() *big.Rat {
//	return big.NewRat(int64(w.Numerator), int64(w.Denominator))
//}
