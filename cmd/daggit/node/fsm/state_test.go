package fsm

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/accumulatenetwork/accumulate/exp/light"
	. "gitlab.com/accumulatenetwork/accumulate/protocol"
	. "gitlab.com/accumulatenetwork/accumulate/test/harness"
	"gitlab.com/accumulatenetwork/accumulate/test/simulator"
	acctesting "gitlab.com/accumulatenetwork/accumulate/test/testing"
	"gitlab.com/accumulatenetwork/accumulate/vdk/node"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/cmd/daggit/app"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/internal"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/internal/testutils"
)

func TestStateExecutor_DidCommitBlock(t *testing.T) {
	t.Skip("Needs to be updated")

	sim := NewSim(t,
		simulator.MemoryDatabase,
		simulator.SimpleNetwork(t.Name(), 1, 1),
		simulator.Genesis(GenesisTime),
	)

	params := new(app.Parameters)
	params.Init()
	testutils.InitDaggits(t, sim, params, 1)

	var d node.DidCommitBlock

	opts := Options{Wallet: internal.NewWalletFromKey(acctesting.GenerateKey("follower")),
		TransitionRate: 1, Epoch: 10 * time.Second}
	e := NewStateExecutor(opts)
	e.Params = params

	var err error
	e.Light, err = light.NewClient(
		light.Logger(acctesting.NewTestLogger(t)),
		light.MemoryStore(),
		light.ClientV2(sim.S.ClientV2(Directory)),
	)
	require.NoError(t, err)

	//we currently have no leader selection logic, so test the follower state path first
	followerStatePath := []State{
		StateReset,
		StateCanBegin,
		StateSelectLeader,
		StateProcessUserStakingRequests,
		StateDidInitiateStakingRequests,
		StateProcessStakingRequests,
		StateDidProcessStakingRequests,
		StateComputePayoutTransaction,
		StateDidInitiatePayoutsTransaction,
		StateProcessPayoutsTransaction,
		StateDidProcessPayoutsTransaction,
		StateFinalizeEpoch,
	}

	for i, s := range followerStatePath {
		//need to test the current state is the expected state...
		require.Equal(t, s.String(), e.getOrCreateStateContext().GetCurrent().String())
		d.Index = uint64(i)
		d.Major = d.Index % 10
		d.Time = time.Now()
		e.currentState = e.currentState.execute(e.getOrCreateStateContext(), &d)
	}
	//need to make sure it goes back to the initial state after cycling through the epoch states
	require.Equal(t, e.getOrCreateStateContext().GetCurrent(), followerStatePath[0])

	/*
		%%           |__ ProcessUserStakingRequests (Build Transaction Body)
		%%           |__ CanInitiateStakingRequests
		%%           |__ InitiateStakingRequests (submit transaction)
		%%           |__ DidProcessStakingRequests (i.e. wait for multisig to be processed by followers)
		%%              |__ Success
		%%                 |__ ComputePayoutTransaction
		%%                    |__ Success
		%%                       |__ InitiatePayoutsTransaction
		%%                    |__ Fail
		%%  	        |__ Fail
		%%                 |__ goto SelectLeader and try again...
	*/

	//now test the leader path and force the leader since leader logic isn't implemented.
	e = NewStateExecutor(opts)
	e.Params = params

	e.Light, err = light.NewClient(
		light.Logger(acctesting.NewTestLogger(t)),
		light.MemoryStore(),
		light.ClientV2(sim.S.ClientV2(Directory)),
	)
	require.NoError(t, err)

	leaderStatePath := []State{
		StateReset,
		StateCanBegin,
		StateSelectLeader,
		StateProcessUserStakingRequests,
		StateInitiateStakingRequests,
		StateDidProcessStakingRequests,
		StateComputePayoutTransaction,
		//todo: do we need StateCanInitiatePayoutsTransaction?
		StateInitiatePayoutsTransaction,
		StateDidProcessPayoutsTransaction,
		StateFinalizeEpoch,
	}

	for i, s := range leaderStatePath {
		//need to test the current state is the expected state...
		require.Equal(t, s.String(), e.getOrCreateStateContext().GetCurrent().String())
		d.Index = uint64(i)
		d.Major = d.Index % 10
		d.Time = time.Now()
		//we are forcing the leader path for this test
		e.getOrCreateStateContext().AmLeader = true
		e.currentState = e.currentState.execute(e.getOrCreateStateContext(), &d)
	}

	require.Equal(t, e.getOrCreateStateContext().GetCurrent().String(), followerStatePath[0].String())
}
