package fsm

import "gitlab.com/accumulatenetwork/accumulate/pkg/errors"

func (s *StateContext) Fatal(code errors.Status) stateErrorBuilder {
	return stateErrorBuilder{s, code.Skip(1), StateFatalError}
}

func (s *StateContext) TryAgain(code errors.Status) stateErrorBuilder {
	return stateErrorBuilder{s, code.Skip(1), StateFatalError}
}

type stateErrorBuilder struct {
	ctx     *StateContext
	builder errors.Skip
	state   State
}

func (b stateErrorBuilder) Wrap(err error) StateDelegate {
	err = b.builder.Wrap(err)
	return b.ctx.makeCurrentError(b.state, err)
}

func (b stateErrorBuilder) With(v ...any) StateDelegate {
	err := b.builder.With(v...)
	return b.ctx.makeCurrentError(b.state, err)
}

func (b stateErrorBuilder) WithCauseAndFormat(cause error, format string, args ...any) StateDelegate {
	err := b.builder.WithCauseAndFormat(cause, format, args...)
	return b.ctx.makeCurrentError(b.state, err)
}

func (b stateErrorBuilder) WithFormat(format string, args ...any) StateDelegate {
	err := b.builder.WithFormat(format, args...)
	return b.ctx.makeCurrentError(b.state, err)
}
