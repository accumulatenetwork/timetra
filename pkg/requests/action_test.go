package requests

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestParseActions(t *testing.T) {
	t.Skip("Skipping action tests for now until actions are updated")
	actions, err := ParseActions([][]byte{
		[]byte("registerIdentity"),
		[]byte("identity=ethan.acme"),

		[]byte("addAccount"),
		[]byte("account=ethan.acme/stake"),
		[]byte("type=delegated"),

		[]byte("changePayout"),
		[]byte("destination=ethan.acme/rewards"),

		[]byte("changeDelegate"),
		[]byte("delegate=highstakes.acme"),
	})
	require.NoError(t, err)
	require.Len(t, actions, 4)

	assert.IsType(t, (*RegisterIdentity)(nil), actions[0])
	assert.IsType(t, (*AddAccount)(nil), actions[1])
	//assert.IsType(t, (*ChangePayout)(nil), actions[2])
	//assert.IsType(t, (*ChangeDelegate)(nil), actions[3])

	assert.Equal(t, "ethan.acme", actions[0].(*RegisterIdentity).Identity.ShortString())
	assert.Equal(t, "ethan.acme/stake", actions[1].(*AddAccount).Account.ShortString())
	assert.Equal(t, "delegated", actions[1].(*AddAccount).Type.String())
	//assert.Equal(t, "ethan.acme/stake", actions[2].(*ChangePayout).Account.ShortString())
	//assert.Equal(t, "ethan.acme/rewards", actions[2].(*ChangePayout).Destination.ShortString())
	//assert.Equal(t, "ethan.acme/stake", actions[3].(*ChangeDelegate).Account.ShortString())
	//assert.Equal(t, "highstakes.acme", actions[3].(*ChangeDelegate).Delegate.ShortString())
}
