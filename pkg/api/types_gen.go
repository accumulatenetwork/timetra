// Copyright 2022 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

package api

// GENERATED BY go run ./tools/cmd/gen-types. DO NOT EDIT.

//lint:file-ignore S1001,S1002,S1008,SA4013 generated code

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"math/big"
	"strings"
	"time"

	"gitlab.com/accumulatenetwork/accumulate/pkg/types/encoding"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/pkg/types"
)

type AccountStatus struct {
	fieldsSet  []bool
	Account    *types.Account `json:"account,omitempty" form:"account" query:"account" validate:"required"`
	Balance    *big.Int       `json:"balance,omitempty" form:"balance" query:"balance" validate:"required"`
	Deposits   *big.Int       `json:"deposits,omitempty" form:"deposits" query:"deposits" validate:"required"`
	Registered *url.TxID      `json:"registered,omitempty" form:"registered" query:"registered" validate:"required"`
	Staked     *TxWhen        `json:"staked,omitempty" form:"staked" query:"staked" validate:"required"`
	LastChange *TxWhen        `json:"lastChange,omitempty" form:"lastChange" query:"lastChange" validate:"required"`
	extraData  []byte
}

type AccountStatusOptions struct {
	fieldsSet []bool
	extraData []byte
}

type AccountStatusRequest struct {
	fieldsSet []bool
	AccountStatusOptions
	Account   *url.URL `json:"account,omitempty" form:"account" query:"account" validate:"required"`
	extraData []byte
}

type AccountStatusResponse struct {
	fieldsSet []bool
	Value     *AccountStatus `json:"value,omitempty" form:"value" query:"value" validate:"required"`
	extraData []byte
}

type EventNotify struct {
	fieldsSet      []bool
	ConversationId string `json:"conversationId,omitempty" form:"conversationId" query:"conversationId" validate:"required"`
	EventId        string `json:"eventId,omitempty" form:"eventId" query:"eventId" validate:"required"`
	extraData      []byte
}

type TxWhen struct {
	fieldsSet []bool
	TxID      *url.TxID `json:"txID,omitempty" form:"txID" query:"txID" validate:"required"`
	Time      time.Time `json:"time,omitempty" form:"time" query:"time" validate:"required"`
	extraData []byte
}

func (*AccountStatusRequest) Type() MessageType { return MessageTypeAccountStatusRequest }

func (*AccountStatusResponse) Type() MessageType { return MessageTypeAccountStatusResponse }

func (*EventNotify) Type() MessageType { return MessageTypeEventNotify }

func (v *AccountStatus) Copy() *AccountStatus {
	u := new(AccountStatus)

	if v.Account != nil {
		u.Account = (v.Account).Copy()
	}
	if v.Balance != nil {
		u.Balance = encoding.BigintCopy(v.Balance)
	}
	if v.Deposits != nil {
		u.Deposits = encoding.BigintCopy(v.Deposits)
	}
	if v.Registered != nil {
		u.Registered = v.Registered
	}
	if v.Staked != nil {
		u.Staked = (v.Staked).Copy()
	}
	if v.LastChange != nil {
		u.LastChange = (v.LastChange).Copy()
	}
	if len(v.extraData) > 0 {
		u.extraData = make([]byte, len(v.extraData))
		copy(u.extraData, v.extraData)
	}

	return u
}

func (v *AccountStatus) CopyAsInterface() interface{} { return v.Copy() }

func (v *AccountStatusOptions) Copy() *AccountStatusOptions {
	u := new(AccountStatusOptions)

	if len(v.extraData) > 0 {
		u.extraData = make([]byte, len(v.extraData))
		copy(u.extraData, v.extraData)
	}

	return u
}

func (v *AccountStatusOptions) CopyAsInterface() interface{} { return v.Copy() }

func (v *AccountStatusRequest) Copy() *AccountStatusRequest {
	u := new(AccountStatusRequest)

	u.AccountStatusOptions = *v.AccountStatusOptions.Copy()
	if v.Account != nil {
		u.Account = v.Account
	}
	if len(v.extraData) > 0 {
		u.extraData = make([]byte, len(v.extraData))
		copy(u.extraData, v.extraData)
	}

	return u
}

func (v *AccountStatusRequest) CopyAsInterface() interface{} { return v.Copy() }

func (v *AccountStatusResponse) Copy() *AccountStatusResponse {
	u := new(AccountStatusResponse)

	if v.Value != nil {
		u.Value = (v.Value).Copy()
	}
	if len(v.extraData) > 0 {
		u.extraData = make([]byte, len(v.extraData))
		copy(u.extraData, v.extraData)
	}

	return u
}

func (v *AccountStatusResponse) CopyAsInterface() interface{} { return v.Copy() }

func (v *EventNotify) Copy() *EventNotify {
	u := new(EventNotify)

	u.ConversationId = v.ConversationId
	u.EventId = v.EventId
	if len(v.extraData) > 0 {
		u.extraData = make([]byte, len(v.extraData))
		copy(u.extraData, v.extraData)
	}

	return u
}

func (v *EventNotify) CopyAsInterface() interface{} { return v.Copy() }

func (v *TxWhen) Copy() *TxWhen {
	u := new(TxWhen)

	if v.TxID != nil {
		u.TxID = v.TxID
	}
	u.Time = v.Time
	if len(v.extraData) > 0 {
		u.extraData = make([]byte, len(v.extraData))
		copy(u.extraData, v.extraData)
	}

	return u
}

func (v *TxWhen) CopyAsInterface() interface{} { return v.Copy() }

func (v *AccountStatus) Equal(u *AccountStatus) bool {
	switch {
	case v.Account == u.Account:
		// equal
	case v.Account == nil || u.Account == nil:
		return false
	case !((v.Account).Equal(u.Account)):
		return false
	}
	switch {
	case v.Balance == u.Balance:
		// equal
	case v.Balance == nil || u.Balance == nil:
		return false
	case !((v.Balance).Cmp(u.Balance) == 0):
		return false
	}
	switch {
	case v.Deposits == u.Deposits:
		// equal
	case v.Deposits == nil || u.Deposits == nil:
		return false
	case !((v.Deposits).Cmp(u.Deposits) == 0):
		return false
	}
	switch {
	case v.Registered == u.Registered:
		// equal
	case v.Registered == nil || u.Registered == nil:
		return false
	case !((v.Registered).Equal(u.Registered)):
		return false
	}
	switch {
	case v.Staked == u.Staked:
		// equal
	case v.Staked == nil || u.Staked == nil:
		return false
	case !((v.Staked).Equal(u.Staked)):
		return false
	}
	switch {
	case v.LastChange == u.LastChange:
		// equal
	case v.LastChange == nil || u.LastChange == nil:
		return false
	case !((v.LastChange).Equal(u.LastChange)):
		return false
	}

	return true
}

func (v *AccountStatusOptions) Equal(u *AccountStatusOptions) bool {

	return true
}

func (v *AccountStatusRequest) Equal(u *AccountStatusRequest) bool {
	if !v.AccountStatusOptions.Equal(&u.AccountStatusOptions) {
		return false
	}
	switch {
	case v.Account == u.Account:
		// equal
	case v.Account == nil || u.Account == nil:
		return false
	case !((v.Account).Equal(u.Account)):
		return false
	}

	return true
}

func (v *AccountStatusResponse) Equal(u *AccountStatusResponse) bool {
	switch {
	case v.Value == u.Value:
		// equal
	case v.Value == nil || u.Value == nil:
		return false
	case !((v.Value).Equal(u.Value)):
		return false
	}

	return true
}

func (v *EventNotify) Equal(u *EventNotify) bool {
	if !(v.ConversationId == u.ConversationId) {
		return false
	}
	if !(v.EventId == u.EventId) {
		return false
	}

	return true
}

func (v *TxWhen) Equal(u *TxWhen) bool {
	switch {
	case v.TxID == u.TxID:
		// equal
	case v.TxID == nil || u.TxID == nil:
		return false
	case !((v.TxID).Equal(u.TxID)):
		return false
	}
	if !((v.Time).Equal(u.Time)) {
		return false
	}

	return true
}

var fieldNames_AccountStatus = []string{
	1: "Account",
	2: "Balance",
	3: "Deposits",
	4: "Registered",
	5: "Staked",
	6: "LastChange",
}

func (v *AccountStatus) MarshalBinary() ([]byte, error) {
	if v == nil {
		return []byte{encoding.EmptyObject}, nil
	}

	buffer := new(bytes.Buffer)
	writer := encoding.NewWriter(buffer)

	if !(v.Account == nil) {
		writer.WriteValue(1, v.Account.MarshalBinary)
	}
	if !(v.Balance == nil) {
		writer.WriteBigInt(2, v.Balance)
	}
	if !(v.Deposits == nil) {
		writer.WriteBigInt(3, v.Deposits)
	}
	if !(v.Registered == nil) {
		writer.WriteTxid(4, v.Registered)
	}
	if !(v.Staked == nil) {
		writer.WriteValue(5, v.Staked.MarshalBinary)
	}
	if !(v.LastChange == nil) {
		writer.WriteValue(6, v.LastChange.MarshalBinary)
	}

	_, _, err := writer.Reset(fieldNames_AccountStatus)
	if err != nil {
		return nil, encoding.Error{E: err}
	}
	buffer.Write(v.extraData)
	return buffer.Bytes(), nil
}

func (v *AccountStatus) IsValid() error {
	var errs []string

	if len(v.fieldsSet) > 0 && !v.fieldsSet[0] {
		errs = append(errs, "field Account is missing")
	} else if v.Account == nil {
		errs = append(errs, "field Account is not set")
	}
	if len(v.fieldsSet) > 1 && !v.fieldsSet[1] {
		errs = append(errs, "field Balance is missing")
	} else if v.Balance == nil {
		errs = append(errs, "field Balance is not set")
	}
	if len(v.fieldsSet) > 2 && !v.fieldsSet[2] {
		errs = append(errs, "field Deposits is missing")
	} else if v.Deposits == nil {
		errs = append(errs, "field Deposits is not set")
	}
	if len(v.fieldsSet) > 3 && !v.fieldsSet[3] {
		errs = append(errs, "field Registered is missing")
	} else if v.Registered == nil {
		errs = append(errs, "field Registered is not set")
	}
	if len(v.fieldsSet) > 4 && !v.fieldsSet[4] {
		errs = append(errs, "field Staked is missing")
	} else if v.Staked == nil {
		errs = append(errs, "field Staked is not set")
	}
	if len(v.fieldsSet) > 5 && !v.fieldsSet[5] {
		errs = append(errs, "field LastChange is missing")
	} else if v.LastChange == nil {
		errs = append(errs, "field LastChange is not set")
	}

	switch len(errs) {
	case 0:
		return nil
	case 1:
		return errors.New(errs[0])
	default:
		return errors.New(strings.Join(errs, "; "))
	}
}

var fieldNames_AccountStatusOptions = []string{}

func (v *AccountStatusOptions) MarshalBinary() ([]byte, error) {
	if v == nil {
		return []byte{encoding.EmptyObject}, nil
	}

	buffer := new(bytes.Buffer)
	writer := encoding.NewWriter(buffer)

	_, _, err := writer.Reset(fieldNames_AccountStatusOptions)
	if err != nil {
		return nil, encoding.Error{E: err}
	}
	buffer.Write(v.extraData)
	return buffer.Bytes(), nil
}

func (v *AccountStatusOptions) IsValid() error {
	var errs []string

	switch len(errs) {
	case 0:
		return nil
	case 1:
		return errors.New(errs[0])
	default:
		return errors.New(strings.Join(errs, "; "))
	}
}

var fieldNames_AccountStatusRequest = []string{
	1: "Type",
	2: "AccountStatusOptions",
	3: "Account",
}

func (v *AccountStatusRequest) MarshalBinary() ([]byte, error) {
	if v == nil {
		return []byte{encoding.EmptyObject}, nil
	}

	buffer := new(bytes.Buffer)
	writer := encoding.NewWriter(buffer)

	writer.WriteEnum(1, v.Type())
	writer.WriteValue(2, v.AccountStatusOptions.MarshalBinary)
	if !(v.Account == nil) {
		writer.WriteUrl(3, v.Account)
	}

	_, _, err := writer.Reset(fieldNames_AccountStatusRequest)
	if err != nil {
		return nil, encoding.Error{E: err}
	}
	buffer.Write(v.extraData)
	return buffer.Bytes(), nil
}

func (v *AccountStatusRequest) IsValid() error {
	var errs []string

	if len(v.fieldsSet) > 0 && !v.fieldsSet[0] {
		errs = append(errs, "field Type is missing")
	}
	if err := v.AccountStatusOptions.IsValid(); err != nil {
		errs = append(errs, err.Error())
	}
	if len(v.fieldsSet) > 2 && !v.fieldsSet[2] {
		errs = append(errs, "field Account is missing")
	} else if v.Account == nil {
		errs = append(errs, "field Account is not set")
	}

	switch len(errs) {
	case 0:
		return nil
	case 1:
		return errors.New(errs[0])
	default:
		return errors.New(strings.Join(errs, "; "))
	}
}

var fieldNames_AccountStatusResponse = []string{
	1: "Type",
	2: "Value",
}

func (v *AccountStatusResponse) MarshalBinary() ([]byte, error) {
	if v == nil {
		return []byte{encoding.EmptyObject}, nil
	}

	buffer := new(bytes.Buffer)
	writer := encoding.NewWriter(buffer)

	writer.WriteEnum(1, v.Type())
	if !(v.Value == nil) {
		writer.WriteValue(2, v.Value.MarshalBinary)
	}

	_, _, err := writer.Reset(fieldNames_AccountStatusResponse)
	if err != nil {
		return nil, encoding.Error{E: err}
	}
	buffer.Write(v.extraData)
	return buffer.Bytes(), nil
}

func (v *AccountStatusResponse) IsValid() error {
	var errs []string

	if len(v.fieldsSet) > 0 && !v.fieldsSet[0] {
		errs = append(errs, "field Type is missing")
	}
	if len(v.fieldsSet) > 1 && !v.fieldsSet[1] {
		errs = append(errs, "field Value is missing")
	} else if v.Value == nil {
		errs = append(errs, "field Value is not set")
	}

	switch len(errs) {
	case 0:
		return nil
	case 1:
		return errors.New(errs[0])
	default:
		return errors.New(strings.Join(errs, "; "))
	}
}

var fieldNames_EventNotify = []string{
	1: "Type",
	2: "ConversationId",
	3: "EventId",
}

func (v *EventNotify) MarshalBinary() ([]byte, error) {
	if v == nil {
		return []byte{encoding.EmptyObject}, nil
	}

	buffer := new(bytes.Buffer)
	writer := encoding.NewWriter(buffer)

	writer.WriteEnum(1, v.Type())
	if !(len(v.ConversationId) == 0) {
		writer.WriteString(2, v.ConversationId)
	}
	if !(len(v.EventId) == 0) {
		writer.WriteString(3, v.EventId)
	}

	_, _, err := writer.Reset(fieldNames_EventNotify)
	if err != nil {
		return nil, encoding.Error{E: err}
	}
	buffer.Write(v.extraData)
	return buffer.Bytes(), nil
}

func (v *EventNotify) IsValid() error {
	var errs []string

	if len(v.fieldsSet) > 0 && !v.fieldsSet[0] {
		errs = append(errs, "field Type is missing")
	}
	if len(v.fieldsSet) > 1 && !v.fieldsSet[1] {
		errs = append(errs, "field ConversationId is missing")
	} else if len(v.ConversationId) == 0 {
		errs = append(errs, "field ConversationId is not set")
	}
	if len(v.fieldsSet) > 2 && !v.fieldsSet[2] {
		errs = append(errs, "field EventId is missing")
	} else if len(v.EventId) == 0 {
		errs = append(errs, "field EventId is not set")
	}

	switch len(errs) {
	case 0:
		return nil
	case 1:
		return errors.New(errs[0])
	default:
		return errors.New(strings.Join(errs, "; "))
	}
}

var fieldNames_TxWhen = []string{
	1: "TxID",
	2: "Time",
}

func (v *TxWhen) MarshalBinary() ([]byte, error) {
	if v == nil {
		return []byte{encoding.EmptyObject}, nil
	}

	buffer := new(bytes.Buffer)
	writer := encoding.NewWriter(buffer)

	if !(v.TxID == nil) {
		writer.WriteTxid(1, v.TxID)
	}
	if !(v.Time == (time.Time{})) {
		writer.WriteTime(2, v.Time)
	}

	_, _, err := writer.Reset(fieldNames_TxWhen)
	if err != nil {
		return nil, encoding.Error{E: err}
	}
	buffer.Write(v.extraData)
	return buffer.Bytes(), nil
}

func (v *TxWhen) IsValid() error {
	var errs []string

	if len(v.fieldsSet) > 0 && !v.fieldsSet[0] {
		errs = append(errs, "field TxID is missing")
	} else if v.TxID == nil {
		errs = append(errs, "field TxID is not set")
	}
	if len(v.fieldsSet) > 1 && !v.fieldsSet[1] {
		errs = append(errs, "field Time is missing")
	} else if v.Time == (time.Time{}) {
		errs = append(errs, "field Time is not set")
	}

	switch len(errs) {
	case 0:
		return nil
	case 1:
		return errors.New(errs[0])
	default:
		return errors.New(strings.Join(errs, "; "))
	}
}

func (v *AccountStatus) UnmarshalBinary(data []byte) error {
	return v.UnmarshalBinaryFrom(bytes.NewReader(data))
}

func (v *AccountStatus) UnmarshalBinaryFrom(rd io.Reader) error {
	reader := encoding.NewReader(rd)

	if x := new(types.Account); reader.ReadValue(1, x.UnmarshalBinaryFrom) {
		v.Account = x
	}
	if x, ok := reader.ReadBigInt(2); ok {
		v.Balance = x
	}
	if x, ok := reader.ReadBigInt(3); ok {
		v.Deposits = x
	}
	if x, ok := reader.ReadTxid(4); ok {
		v.Registered = x
	}
	if x := new(TxWhen); reader.ReadValue(5, x.UnmarshalBinaryFrom) {
		v.Staked = x
	}
	if x := new(TxWhen); reader.ReadValue(6, x.UnmarshalBinaryFrom) {
		v.LastChange = x
	}

	seen, err := reader.Reset(fieldNames_AccountStatus)
	if err != nil {
		return encoding.Error{E: err}
	}
	v.fieldsSet = seen
	v.extraData, err = reader.ReadAll()
	if err != nil {
		return encoding.Error{E: err}
	}
	return nil
}

func (v *AccountStatusOptions) UnmarshalBinary(data []byte) error {
	return v.UnmarshalBinaryFrom(bytes.NewReader(data))
}

func (v *AccountStatusOptions) UnmarshalBinaryFrom(rd io.Reader) error {
	reader := encoding.NewReader(rd)

	seen, err := reader.Reset(fieldNames_AccountStatusOptions)
	if err != nil {
		return encoding.Error{E: err}
	}
	v.fieldsSet = seen
	v.extraData, err = reader.ReadAll()
	if err != nil {
		return encoding.Error{E: err}
	}
	return nil
}

func (v *AccountStatusRequest) UnmarshalBinary(data []byte) error {
	return v.UnmarshalBinaryFrom(bytes.NewReader(data))
}

func (v *AccountStatusRequest) UnmarshalBinaryFrom(rd io.Reader) error {
	reader := encoding.NewReader(rd)

	var vType MessageType
	if x := new(MessageType); reader.ReadEnum(1, x) {
		vType = *x
	}
	if !(v.Type() == vType) {
		return fmt.Errorf("field Type: not equal: want %v, got %v", v.Type(), vType)
	}

	return v.UnmarshalFieldsFrom(reader)
}

func (v *AccountStatusRequest) UnmarshalFieldsFrom(reader *encoding.Reader) error {
	reader.ReadValue(2, v.AccountStatusOptions.UnmarshalBinaryFrom)
	if x, ok := reader.ReadUrl(3); ok {
		v.Account = x
	}

	seen, err := reader.Reset(fieldNames_AccountStatusRequest)
	if err != nil {
		return encoding.Error{E: err}
	}
	v.fieldsSet = seen
	v.extraData, err = reader.ReadAll()
	if err != nil {
		return encoding.Error{E: err}
	}
	return nil
}

func (v *AccountStatusResponse) UnmarshalBinary(data []byte) error {
	return v.UnmarshalBinaryFrom(bytes.NewReader(data))
}

func (v *AccountStatusResponse) UnmarshalBinaryFrom(rd io.Reader) error {
	reader := encoding.NewReader(rd)

	var vType MessageType
	if x := new(MessageType); reader.ReadEnum(1, x) {
		vType = *x
	}
	if !(v.Type() == vType) {
		return fmt.Errorf("field Type: not equal: want %v, got %v", v.Type(), vType)
	}

	return v.UnmarshalFieldsFrom(reader)
}

func (v *AccountStatusResponse) UnmarshalFieldsFrom(reader *encoding.Reader) error {
	if x := new(AccountStatus); reader.ReadValue(2, x.UnmarshalBinaryFrom) {
		v.Value = x
	}

	seen, err := reader.Reset(fieldNames_AccountStatusResponse)
	if err != nil {
		return encoding.Error{E: err}
	}
	v.fieldsSet = seen
	v.extraData, err = reader.ReadAll()
	if err != nil {
		return encoding.Error{E: err}
	}
	return nil
}

func (v *EventNotify) UnmarshalBinary(data []byte) error {
	return v.UnmarshalBinaryFrom(bytes.NewReader(data))
}

func (v *EventNotify) UnmarshalBinaryFrom(rd io.Reader) error {
	reader := encoding.NewReader(rd)

	var vType MessageType
	if x := new(MessageType); reader.ReadEnum(1, x) {
		vType = *x
	}
	if !(v.Type() == vType) {
		return fmt.Errorf("field Type: not equal: want %v, got %v", v.Type(), vType)
	}

	return v.UnmarshalFieldsFrom(reader)
}

func (v *EventNotify) UnmarshalFieldsFrom(reader *encoding.Reader) error {
	if x, ok := reader.ReadString(2); ok {
		v.ConversationId = x
	}
	if x, ok := reader.ReadString(3); ok {
		v.EventId = x
	}

	seen, err := reader.Reset(fieldNames_EventNotify)
	if err != nil {
		return encoding.Error{E: err}
	}
	v.fieldsSet = seen
	v.extraData, err = reader.ReadAll()
	if err != nil {
		return encoding.Error{E: err}
	}
	return nil
}

func (v *TxWhen) UnmarshalBinary(data []byte) error {
	return v.UnmarshalBinaryFrom(bytes.NewReader(data))
}

func (v *TxWhen) UnmarshalBinaryFrom(rd io.Reader) error {
	reader := encoding.NewReader(rd)

	if x, ok := reader.ReadTxid(1); ok {
		v.TxID = x
	}
	if x, ok := reader.ReadTime(2); ok {
		v.Time = x
	}

	seen, err := reader.Reset(fieldNames_TxWhen)
	if err != nil {
		return encoding.Error{E: err}
	}
	v.fieldsSet = seen
	v.extraData, err = reader.ReadAll()
	if err != nil {
		return encoding.Error{E: err}
	}
	return nil
}

func (v *AccountStatus) MarshalJSON() ([]byte, error) {
	u := struct {
		Account    *types.Account `json:"account,omitempty"`
		Balance    *string        `json:"balance,omitempty"`
		Deposits   *string        `json:"deposits,omitempty"`
		Registered *url.TxID      `json:"registered,omitempty"`
		Staked     *TxWhen        `json:"staked,omitempty"`
		LastChange *TxWhen        `json:"lastChange,omitempty"`
	}{}
	if !(v.Account == nil) {
		u.Account = v.Account
	}
	if !(v.Balance == nil) {
		u.Balance = encoding.BigintToJSON(v.Balance)
	}
	if !(v.Deposits == nil) {
		u.Deposits = encoding.BigintToJSON(v.Deposits)
	}
	if !(v.Registered == nil) {
		u.Registered = v.Registered
	}
	if !(v.Staked == nil) {
		u.Staked = v.Staked
	}
	if !(v.LastChange == nil) {
		u.LastChange = v.LastChange
	}
	return json.Marshal(&u)
}

func (v *AccountStatusRequest) MarshalJSON() ([]byte, error) {
	u := struct {
		Type    MessageType `json:"type"`
		Account *url.URL    `json:"account,omitempty"`
	}{}
	u.Type = v.Type()
	if !(v.Account == nil) {
		u.Account = v.Account
	}
	return json.Marshal(&u)
}

func (v *AccountStatusResponse) MarshalJSON() ([]byte, error) {
	u := struct {
		Type  MessageType    `json:"type"`
		Value *AccountStatus `json:"value,omitempty"`
	}{}
	u.Type = v.Type()
	if !(v.Value == nil) {
		u.Value = v.Value
	}
	return json.Marshal(&u)
}

func (v *EventNotify) MarshalJSON() ([]byte, error) {
	u := struct {
		Type           MessageType `json:"type"`
		ConversationId string      `json:"conversationId,omitempty"`
		EventId        string      `json:"eventId,omitempty"`
	}{}
	u.Type = v.Type()
	if !(len(v.ConversationId) == 0) {
		u.ConversationId = v.ConversationId
	}
	if !(len(v.EventId) == 0) {
		u.EventId = v.EventId
	}
	return json.Marshal(&u)
}

func (v *AccountStatus) UnmarshalJSON(data []byte) error {
	u := struct {
		Account    *types.Account `json:"account,omitempty"`
		Balance    *string        `json:"balance,omitempty"`
		Deposits   *string        `json:"deposits,omitempty"`
		Registered *url.TxID      `json:"registered,omitempty"`
		Staked     *TxWhen        `json:"staked,omitempty"`
		LastChange *TxWhen        `json:"lastChange,omitempty"`
	}{}
	u.Account = v.Account
	u.Balance = encoding.BigintToJSON(v.Balance)
	u.Deposits = encoding.BigintToJSON(v.Deposits)
	u.Registered = v.Registered
	u.Staked = v.Staked
	u.LastChange = v.LastChange
	if err := json.Unmarshal(data, &u); err != nil {
		return err
	}
	v.Account = u.Account
	if x, err := encoding.BigintFromJSON(u.Balance); err != nil {
		return fmt.Errorf("error decoding Balance: %w", err)
	} else {
		v.Balance = x
	}
	if x, err := encoding.BigintFromJSON(u.Deposits); err != nil {
		return fmt.Errorf("error decoding Deposits: %w", err)
	} else {
		v.Deposits = x
	}
	v.Registered = u.Registered
	v.Staked = u.Staked
	v.LastChange = u.LastChange
	return nil
}

func (v *AccountStatusRequest) UnmarshalJSON(data []byte) error {
	u := struct {
		Type    MessageType `json:"type"`
		Account *url.URL    `json:"account,omitempty"`
	}{}
	u.Type = v.Type()
	u.Account = v.Account
	if err := json.Unmarshal(data, &u); err != nil {
		return err
	}
	if !(v.Type() == u.Type) {
		return fmt.Errorf("field Type: not equal: want %v, got %v", v.Type(), u.Type)
	}
	v.Account = u.Account
	return nil
}

func (v *AccountStatusResponse) UnmarshalJSON(data []byte) error {
	u := struct {
		Type  MessageType    `json:"type"`
		Value *AccountStatus `json:"value,omitempty"`
	}{}
	u.Type = v.Type()
	u.Value = v.Value
	if err := json.Unmarshal(data, &u); err != nil {
		return err
	}
	if !(v.Type() == u.Type) {
		return fmt.Errorf("field Type: not equal: want %v, got %v", v.Type(), u.Type)
	}
	v.Value = u.Value
	return nil
}

func (v *EventNotify) UnmarshalJSON(data []byte) error {
	u := struct {
		Type           MessageType `json:"type"`
		ConversationId string      `json:"conversationId,omitempty"`
		EventId        string      `json:"eventId,omitempty"`
	}{}
	u.Type = v.Type()
	u.ConversationId = v.ConversationId
	u.EventId = v.EventId
	if err := json.Unmarshal(data, &u); err != nil {
		return err
	}
	if !(v.Type() == u.Type) {
		return fmt.Errorf("field Type: not equal: want %v, got %v", v.Type(), u.Type)
	}
	v.ConversationId = u.ConversationId
	v.EventId = u.EventId
	return nil
}
