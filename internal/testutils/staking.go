package testutils

import (
	"crypto/sha256"
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/accumulatenetwork/accumulate/pkg/client/signing"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	. "gitlab.com/accumulatenetwork/accumulate/protocol"
	. "gitlab.com/accumulatenetwork/accumulate/test/harness"
	. "gitlab.com/accumulatenetwork/accumulate/test/helpers"
	acctesting "gitlab.com/accumulatenetwork/accumulate/test/testing"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/cmd/daggit/app"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/internal"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/pkg/types"
)

func InitDaggits(t testing.TB, sim *Sim, params *app.Parameters, valCount int) (daggitKey *internal.Wallet, valKeys []*internal.Wallet) {
	daggitKey = internal.NewWalletFromKey(acctesting.GenerateKey(params.ADI.Service))
	valKeys = make([]*internal.Wallet, valCount)
	for i := range valKeys {
		valKeys[i] = internal.NewWalletFromKey(acctesting.GenerateKey(params.ADI.Service, i))
	}

	MakeIdentity(t, sim.DatabaseFor(params.ADI.Service), params.ADI.Service, daggitKey.GetPublicKey())
	MakeKeyPage(t, sim.DatabaseFor(params.ADI.Service), params.Account.Authority)
	UpdateAccount(t, sim.DatabaseFor(params.ADI.Service), params.Account.Validators, func(p *KeyPage) {
		p.CreditBalance = 1e9
		p.AcceptThreshold = uint64(valCount)
		for i, k := range valKeys {
			p.AddKeySpec(&KeySpec{PublicKeyHash: k.GetKeyHash(), Delegate: AccountUrl(fmt.Sprintf("val%d", i))})
		}
	})
	MakeAccount(t, sim.DatabaseFor(params.ADI.Service), &DataAccount{Url: params.Account.ApprovedADIs, AccountAuth: AccountAuth{Authorities: []AuthorityEntry{{Url: params.Account.Validators.Identity()}}}})
	MakeAccount(t, sim.DatabaseFor(params.ADI.Service), &DataAccount{Url: params.Account.Requests, AccountAuth: AccountAuth{Authorities: []AuthorityEntry{{Url: params.Account.Validators.Identity(), Disabled: true}}}})
	MakeAccount(t, sim.DatabaseFor(params.ADI.Service), &DataAccount{Url: params.Account.RegisteredADIs, AccountAuth: AccountAuth{Authorities: []AuthorityEntry{{Url: params.Account.Validators.Identity()}}}})
	MakeAccount(t, sim.DatabaseFor(params.ADI.Service), &DataAccount{Url: params.Account.Actions, AccountAuth: AccountAuth{Authorities: []AuthorityEntry{{Url: params.Account.Validators.Identity()}}}})

	MakeAccount(t, sim.DatabaseFor(params.ADI.Service), &DataAccount{Url: params.Account.Epoch, AccountAuth: AccountAuth{Authorities: []AuthorityEntry{{Url: params.Account.Validators.Identity()}}}})

	UpdateAccount(t, sim.DatabaseFor(params.Account.TokenIssuance), params.Account.TokenIssuance, func(i *TokenIssuer) {
		i.Authorities = nil
		i.AddAuthority(params.Account.Authority)
	})

	data := DoubleHashDataEntry{}
	for i := range valKeys {
		dig := types.Daggit{}
		dig.Identity = url.MustParse(fmt.Sprintf("test-validator-%d", i))
		dig.ProfileId = fmt.Sprintf("%d", i)
		dm, err := dig.MarshalBinary()
		require.NoError(t, err)
		data.Data = append(data.Data, dm)
	}
	MakeAccount(t, sim.DatabaseFor(params.ADI.Service),
		&DataAccount{Url: params.Account.Operators,
			AccountAuth: AccountAuth{Authorities: []AuthorityEntry{{Url: params.Account.Validators.Identity()}}},
			Entry:       &data})

	return daggitKey, valKeys
}

func MustGetKey(t testing.TB, wallet *internal.Wallet) signing.Signer {
	locked, _, err := wallet.GetLockedKey()
	require.NoError(t, err)
	return internal.SecureSigner(locked)
}

func Sha256(b []byte) []byte {
	h := sha256.Sum256(b)
	return h[:]
}

type MajorBlock int

func (m MajorBlock) Satisfied(h *Harness) bool {
	h.TB.Helper()
	var ledger *AnchorLedger
	h.QueryAccountAs(DnUrl().JoinPath(AnchorPool), nil, &ledger)
	return ledger.MajorBlockIndex >= uint64(m) && len(ledger.PendingMajorBlockAnchors) == 0
}

func (m MajorBlock) Format(prefix, suffix string) string {
	return fmt.Sprintf("%smajor block %d%s", prefix, m, suffix)
}
