package fsm

import (
	"context"
	"encoding/json"
	"fmt"
	"math/big"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/accumulatenetwork/accumulate/exp/light"
	"gitlab.com/accumulatenetwork/accumulate/pkg/build"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/network"
	. "gitlab.com/accumulatenetwork/accumulate/protocol"
	. "gitlab.com/accumulatenetwork/accumulate/test/harness"
	. "gitlab.com/accumulatenetwork/accumulate/test/helpers"
	"gitlab.com/accumulatenetwork/accumulate/test/simulator"
	acctesting "gitlab.com/accumulatenetwork/accumulate/test/testing"
	"gitlab.com/accumulatenetwork/accumulate/vdk/node"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/cmd/daggit/app"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/internal/testutils"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/pkg/requests"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/pkg/types"
	"golang.org/x/exp/slog"
)

func init() {
	acctesting.EnableDebugFeatures()
}

func TestUnauthorizedCoreValidator(t *testing.T) {
	var timestamp uint64
	alice := AccountUrl("alice")
	aliceKey := acctesting.GenerateKey(alice)

	params := new(app.Parameters)
	params.Init()

	g := new(network.GlobalValues)
	g.ExecutorVersion = ExecutorVersionLatest
	g.Globals = new(NetworkGlobals)
	g.Globals.MajorBlockSchedule = "* * * * *"
	g.Routing = new(RoutingTable)
	g.Routing.Overrides = []RouteOverride{{
		Account:   params.ADI.Service,
		Partition: Directory,
	}}

	// Initialize the simulator
	sim := NewSim(t,
		simulator.MemoryDatabase,
		simulator.SimpleNetwork(t.Name(), 1, 1),
		simulator.GenesisWith(GenesisTime, g),
	)

	// Set up staking
	_, valKeys := testutils.InitDaggits(t, sim, params, 1)

	// Set up alice
	const initBalance = 1e6
	MakeIdentity(t, sim.DatabaseFor(alice), alice, aliceKey[32:])
	CreditCredits(t, sim.DatabaseFor(alice), alice.JoinPath("book", "1"), 1e9)
	MakeAccount(t, sim.DatabaseFor(alice), &TokenAccount{Url: alice.JoinPath("stake"), TokenUrl: AcmeUrl(), Balance: *big.NewInt(initBalance * AcmePrecision)})

	// Ensure a major block
	sim.StepUntilN(1000,
		testutils.MajorBlock(1))

	// Submit a request to register Alice
	req := sim.BuildAndSubmitTxnSuccessfully(
		build.Transaction().For(alice, "stake").
			UpdateAccountAuth().
			Disable(alice, "book").
			Add(params.Account.Authority).
			SignWith(alice, "book", "1").Version(1).Timestamp(&timestamp).PrivateKey(aliceKey))

	st := sim.BuildAndSubmitTxnSuccessfully(
		build.Transaction().For(params.Account.Requests).
			WriteData().
			DoubleHash(
				"addAccount",
				"account=alice.acme/stake",
				"type=coreValidator",
				"transaction="+req.TxID.String(),
			).
			SignWith(alice, "book", "1").Version(1).Timestamp(&timestamp).PrivateKey(aliceKey))

	sim.StepUntil(
		Txn(st.TxID).Completes())

	ledger := GetAccount[*SystemLedger](t, sim.Database(Directory), DnUrl().JoinPath(Ledger))
	anchors := GetAccount[*AnchorLedger](t, sim.Database(Directory), DnUrl().JoinPath(AnchorPool))
	prevEpoch := node.DidCommitBlock{
		Index: ledger.Index,
		Major: anchors.MajorBlockIndex,
		Time:  anchors.MajorBlockTime,
	}

	lc, err := light.NewClient(
		light.Logger(acctesting.NewTestLogger(t)),
		light.MemoryStore(),
		light.ClientV2(sim.S.ClientV2(Directory)),
	)
	require.NoError(t, err)

	// Initialize the FSM
	fsm := NewStateExecutor(Options{Wallet: valKeys[0], Epoch: 2 * time.Minute, Logger: slog.Default().With("node", 1)})
	fsm.PrevEpochBlock = prevEpoch
	fsm.Light = lc
	fsm.Params = params
	fsm.APIv2 = sim.S.ClientV2(Directory)
	fsm.APIv3 = sim.S.Services()

	// Step until the next epoch
	sim.StepUntilN(1000,
		testutils.MajorBlock(3))

	// Run the FSM
	d := node.DidCommitBlock{
		Index: sim.S.BlockIndex(Directory),
		Time:  time.Now(),
		Major: 3,
	}
	ctx := fsm.getOrCreateStateContext()
	ctx.CurrentStateBlock = d
	for ctx.currentState != StateProcessUserStakingRequests {
		fsm.currentState = fsm.currentState.execute(ctx, &d)
	}

	// Process the request
	ctx.toSign = new(TxnToSign)
	x, err := ctx.loadRequestsAndPull()
	require.NoError(t, err)
	require.Len(t, x.Actions, 1)
	require.IsType(t, (*requests.AddAccount)(nil), x.Actions[0].Action)

	batch := ctx.Executor.Light.OpenDB(false)
	defer batch.Discard()
	err = ctx.executeAction(batch, x.Actions[0], x)
	require.EqualError(t, err, "acc://alice.acme is not approved to be a coreValidator")
}

func advanceSimTo(sim *Sim, ctx *StateContext, d *node.DidCommitBlock, state State) {
	prevState := ctx.currentState
	ctx.CurrentStateBlock = *d
	for ctx.currentState != state {
		ctx.Executor.currentState = ctx.Executor.currentState.execute(ctx, d)
		if ctx.toSubmit != nil {
			//submit any transactions generated
			sim.Submit(ctx.toSubmit)
			ctx.toSubmit = nil
			sim.Step()
		}
		if prevState == ctx.currentState {
			//step the sim if necessary
			sim.Step()
		}
		prevState = ctx.currentState
	}
}

func TestAuthorizedCoreValidator(t *testing.T) {
	var timestamp uint64
	alice := AccountUrl("alice")
	aliceKey := acctesting.GenerateKey(alice)
	aliceLite, err := LiteTokenAddress(aliceKey[32:], ACME, SignatureTypeED25519)
	require.NoError(t, err)

	params := new(app.Parameters)
	params.Init()

	g := new(network.GlobalValues)
	g.ExecutorVersion = ExecutorVersionLatest
	g.Globals = new(NetworkGlobals)
	g.Globals.MajorBlockSchedule = "* * * * *"
	g.Routing = new(RoutingTable)
	g.Routing.Overrides = []RouteOverride{{
		Account:   params.ADI.Service,
		Partition: Directory,
	}}

	// Initialize the simulator
	sim := NewSim(t,
		simulator.MemoryDatabase,
		simulator.SimpleNetwork(t.Name(), 1, 1),
		simulator.GenesisWith(GenesisTime, g),
	)

	// Set up staking
	_, valKeys := testutils.InitDaggits(t, sim, params, 1)

	// Approve Alice
	b, err := json.Marshal(&types.Approved{
		Identity: alice,
		Operator: true,
	})
	require.NoError(t, err)
	st := sim.BuildAndSubmitTxnSuccessfully(
		build.Transaction().For(params.Account.ApprovedADIs).
			WriteData().DoubleHash(b).
			SignWith(params.Account.Validators).Version(1).Timestamp(&timestamp).Signer(testutils.MustGetKey(t, valKeys[0])))

	sim.StepUntil(
		Txn(st.TxID).Completes())

	// Set up alice
	const initBalance = 1e6
	MakeIdentity(t, sim.DatabaseFor(alice), alice, aliceKey[32:])
	CreditCredits(t, sim.DatabaseFor(alice), alice.JoinPath("book", "1"), 1e9)
	MakeAccount(t, sim.DatabaseFor(alice), &TokenAccount{Url: alice.JoinPath("stake"), TokenUrl: AcmeUrl(), Balance: *big.NewInt(initBalance * AcmePrecision)})

	// Ensure a major block
	sim.StepUntilN(1000,
		testutils.MajorBlock(1))

	// Submit a request to register Alice
	req := sim.BuildAndSubmitTxnSuccessfully(
		build.Transaction().For(alice, "stake").
			UpdateAccountAuth().
			Disable(alice, "book").
			Add(params.Account.Authority).
			SignWith(alice, "book", "1").Version(1).Timestamp(&timestamp).PrivateKey(aliceKey))

	st = sim.BuildAndSubmitTxnSuccessfully(
		build.Transaction().For(params.Account.Requests).
			WriteData().
			DoubleHash(
				"addAccount",
				"account=alice.acme/stake",
				"type=coreValidator",
				"transaction="+req.TxID.String(),
			).
			SignWith(alice, "book", "1").Version(1).Timestamp(&timestamp).PrivateKey(aliceKey))

	sim.StepUntil(
		Txn(st.TxID).Completes())

	ledger := GetAccount[*SystemLedger](t, sim.Database(Directory), DnUrl().JoinPath(Ledger))
	anchors := GetAccount[*AnchorLedger](t, sim.Database(Directory), DnUrl().JoinPath(AnchorPool))
	prevEpoch := node.DidCommitBlock{
		Index: ledger.Index,
		Major: anchors.MajorBlockIndex,
		Time:  anchors.MajorBlockTime,
	}

	lc, err := light.NewClient(
		light.Logger(acctesting.NewTestLogger(t)),
		light.MemoryStore(),
		light.ClientV2(sim.S.ClientV2(Directory)),
	)
	require.NoError(t, err)

	// Initialize the FSM
	fsm := NewStateExecutor(Options{Wallet: valKeys[0], Epoch: 2 * time.Minute, Logger: slog.Default().With("node", 1)})
	fsm.PrevEpochBlock = prevEpoch
	fsm.Light = lc
	fsm.Params = params
	fsm.APIv2 = sim.S.ClientV2(Directory)
	fsm.APIv3 = sim.S.Services()

	// Step until the next epoch
	sim.StepUntilN(1000,
		testutils.MajorBlock(3))

	// Run the FSM
	d := node.DidCommitBlock{
		Index: sim.S.BlockIndex(Directory),
		Time:  time.Now(),
		Major: 3,
	}
	ctx := fsm.getOrCreateStateContext()

	advanceSimTo(sim, ctx, &d, StateFinalizeEpoch)

	// Process the request
	ctx.toSign = new(TxnToSign)
	x, err := ctx.loadRequestsAndPull()
	require.NoError(t, err)
	require.Len(t, x.Actions, 1)
	require.IsType(t, (*requests.AddAccount)(nil), x.Actions[0].Action)

	batch := ctx.Executor.Light.OpenDB(false)
	defer batch.Discard()
	err = ctx.executeAction(batch, x.Actions[0], x)
	require.NoError(t, err)

	// submit the withdrawal request between epochs
	st = sim.BuildAndSubmitTxnSuccessfully(
		build.Transaction().For(params.Account.Requests).
			WriteData(&DoubleHashDataEntry{Data: [][]byte{
				[]byte("withdrawTokens"),
				[]byte("account=alice.acme/stake"),
				[]byte(fmt.Sprintf("recipient=%s", aliceLite)),
				[]byte(fmt.Sprintf("amount=%d", uint64(5000*AcmePrecision))),
			}}).
			SignWith(alice, "book", "1").Version(1).Timestamp(&timestamp).PrivateKey(aliceKey))

	sim.StepUntil(
		Txn(st.TxID).Completes())

	//fire up a new fsm epoch without having to complete the old one
	fsm = NewStateExecutor(Options{Wallet: valKeys[0], Epoch: 2 * time.Minute, Logger: slog.Default().With("node", 1)})
	fsm.PrevEpochBlock = d
	fsm.Light = lc
	fsm.Params = params
	fsm.APIv2 = sim.S.ClientV2(Directory)
	fsm.APIv3 = sim.S.Services()

	// go to the next epoch
	//submit a request for token withdraw
	// Step until the next epoch
	sim.StepUntilN(1000,
		testutils.MajorBlock(5))

	// Run the FSM
	d = node.DidCommitBlock{
		Index: sim.S.BlockIndex(Directory),
		Time:  d.Time.Add(2*time.Minute + time.Second),
		Major: 5,
	}
	ctx = fsm.getOrCreateStateContext()
	ctx.CurrentStateBlock = d

	advanceSimTo(sim, ctx, &d, StateFinalizeEpoch)

	// push transactions through system to settle accounts
	sim.StepUntilN(1000,
		testutils.MajorBlock(6))

	// Process the request
	ctx.toSign = new(TxnToSign)
	x, err = ctx.loadRequestsAndPull()
	require.NoError(t, err)
	require.Len(t, x.Actions, 1)
	require.IsType(t, (*requests.WithdrawTokens)(nil), x.Actions[0].Action)

	batch = ctx.Executor.Light.OpenDB(false)
	defer batch.Discard()
	err = ctx.executeAction(batch, x.Actions[0], x)
	require.NoError(t, err)

	err = ctx.Executor.Light.PullAccount(context.TODO(), aliceLite)
	require.NoError(t, err)
	aliceLiteAccount, err := loadAccount[AccountWithTokens](batch, aliceLite)
	require.NoError(t, err)
	require.Equal(t, aliceLiteAccount.TokenBalance().Uint64(), uint64(5000*AcmePrecision))
}
