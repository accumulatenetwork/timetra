package fsm

import (
	"context"
	"strings"

	"gitlab.com/accumulatenetwork/accumulate/exp/light"
	"gitlab.com/accumulatenetwork/accumulate/pkg/api/v3"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/messaging"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/accumulate/vdk/node"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/internal"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/pkg/requests"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/pkg/types"
)

var _ StateDelegate = (*processUserStakingRequests)(nil)

// collect all transactions for accounts that need to be processed for this epoch
// starting from the cutoff time of last epoch.
// verify the user requests by querying the accounts and making sure it is configured correctly.
// if agreeable sign the user initiated transaction.
// store the needed information in the StateContext object (add whatever fields are necessary)
type processUserStakingRequests struct {
}

func (s *processUserStakingRequests) execute(ctx *StateContext, d *node.DidCommitBlock) StateDelegate {
	ctx.toSign = new(TxnToSign)

	x, err := ctx.loadRequestsAndPull()
	if err != nil {
		return ctx.Fatal(errors.UnknownError).WithFormat("load requests: %w", err)
	}

	// Create a batch **after** pulling transactions - otherwise the batch won't
	// be able to see the transactions
	batch := ctx.Executor.Light.OpenDB(false)
	defer batch.Discard()

	// Process requests
	for _, act := range x.Actions {
		err := ctx.executeAction(batch, act, x)
		if err != nil {
			if errors.Code(err).IsServerError() {
				return ctx.Fatal(errors.UnknownError).WithFormat("execute action: %w", err)
			}
			ctx.Executor.Logger.Info("Invalid request", "req", act.Action, "id", act.Transaction.ID(), "error", err)
		}
	}

	// once this is complete, move to do housekeeping requests
	if ctx.AmLeader {
		return ctx.MakeCurrent(StateInitiateStakingRequests)
	}
	return ctx.MakeCurrent(StateDidInitiateStakingRequests)
}

func (s *StateContext) loadRequestsAndPull() (*requestData, error) {
	x, err := s.loadRequests()
	if err != nil {
		return nil, errors.UnknownError.Wrap(err)
	}

	// Pull transactions and accounts
	for _, act := range x.Actions {
		var txns []*url.TxID
		var accounts []*url.URL
		switch req := act.Action.(type) {
		case *requests.RegisterIdentity:
			if req.Identity != nil {
				accounts = append(accounts, req.Identity)
			}
		case *requests.AddAccount:
			if req.Account != nil {
				accounts = append(accounts, req.Account)
			}
			if req.Transaction != nil {
				txns = append(txns, req.Transaction)
			}
		case *requests.WithdrawTokens:
			if req.Account != nil {
				accounts = append(accounts, req.Account)
			}
			if req.Recipient != nil {
				accounts = append(accounts, req.Recipient)
			}
		}

		// We could batch this but then one bad transaction would cause the
		// following ones to fail
		if len(txns) > 0 {
			err = s.Executor.Light.PullTransactions(context.TODO(), txns...)
			if err != nil {
				s.Executor.Logger.Info("Unable to load transaction for request", "req", act.Action, "transaction", act.Transaction.ID(), "error", err)
			}
		}

		for _, url := range accounts {
			// Skip LTAs
			if key, _, _ := protocol.ParseLiteTokenAddress(url); key != nil {
				continue
			}

			err = s.Executor.Light.PullAccount(context.TODO(), url)
			if err != nil {
				s.Executor.Logger.Info("Unable to load account for request", "req", act.Action, "account", url, "error", err)
			}
		}
	}

	return x, nil
}

type requestData struct {
	Actions    []*internal.RequestTx
	Registered map[[32]byte]*types.Identity
	Approved   map[[32]byte]*types.Approved
}

// loadRequests loads pending requests and the registration list.
func (s *StateContext) loadRequests() (*requestData, error) {
	batch := s.Executor.Light.OpenDB(false)
	defer batch.Discard()

	// Load registered
	allReg, err := internal.LoadAllRegistered(batch, s.Executor.Params)
	if err != nil {
		return nil, errors.UnknownError.Wrap(err)
	}
	registered := map[[32]byte]*types.Identity{}
	for _, r := range allReg {
		registered[r.Identity.AccountID32()] = r
	}

	allApproved, err := internal.LoadAllApproved(batch, s.Executor.Params)
	if err != nil {
		return nil, errors.UnknownError.Wrap(err)
	}
	approved := map[[32]byte]*types.Approved{}
	for _, r := range allApproved {
		approved[r.Identity.AccountID32()] = r
	}

	// Load requests received during the epoch. LoadRequests expects a
	// [start,end) range so we need to increment the indices.
	epoch := [2]uint64{
		s.Executor.PrevEpochBlock.Major + 1,
		s.Executor.CurrentEpochBlock.Major + 1,
	}
	actions, invalid, err := internal.LoadRequests(s.Executor.Params, batch, epoch)
	if err != nil {
		return nil, errors.UnknownError.Wrap(err)
	}
	for _, i := range invalid {
		s.Executor.Logger.Info("Invalid request", "id", i.Transaction.ID(), "error", i.Error)
	}

	return &requestData{
		Actions:    actions,
		Registered: registered,
		Approved:   approved,
	}, nil
}

// executeAction executes an action from a request.
func (s *StateContext) executeAction(batch *light.DB, txn *internal.RequestTx, x *requestData) error {
	var onSuccess []func()
	var account protocol.FullAccount
	switch req := txn.Action.(type) {
	case *requests.RegisterIdentity:
		if req.Identity == nil {
			return errors.BadRequest.With("missing identity")
		}
		if !req.Identity.IsRootIdentity() {
			return errors.BadRequest.With("cannot register non-root identity")
		}
		id := req.Identity.AccountID32()
		if x.Registered[id] != nil {
			s.Executor.Logger.Debug("Already registered", "req", req, "id", txn.Transaction.ID())
			return nil
		}

		var err error
		account, err = loadAccount[*protocol.ADI](batch, req.Identity)
		if err != nil {
			return errors.BadRequest.WithFormat("load identity %v: %w", req.Identity, err)
		}

		x.Registered[id] = &types.Identity{Identity: req.Identity}
		onSuccess = append(onSuccess, func() {
			s.didUpdate(txn.Transaction.ID(), &types.Identity{Identity: req.Identity})
		})
	case *requests.WithdrawTokens:
		if req.Account == nil {
			return errors.BadRequest.With("missing account")
		}

		if req.Recipient == nil {
			return errors.BadRequest.With("missing recipient")
		}

		if req.Amount.Sign() < 0 {
			return errors.BadRequest.With("amount is less than zero")
		}

		stakedAccount, err := loadAccount[*protocol.TokenAccount](batch, req.Account)
		// can't execute if we don't own it.
		if !internal.IsOwnedByStaking(s.Executor.Params, stakedAccount) {
			return errors.BadRequest.WithFormat("%v is not owned by %v", req.Account, s.Executor.Params.Account.Authority)
		}

		// check if account is locked
		if internal.IsAccountLocked(s.Executor.Params, stakedAccount) {
			return errors.BadRequest.WithFormat("%v currently locked by %v", req.Account, s.Executor.Params.Account.Authority)
		}

		//set the full account for processing later on...
		account = stakedAccount

		withdraw := new(protocol.SendTokens)
		withdraw.To = append(withdraw.To, &protocol.TokenRecipient{Url: req.Recipient, Amount: *req.Amount})
		tx := new(protocol.Transaction)
		tx.Body = withdraw
		tx.Header.Principal = req.Account

		md := new(types.StateHeader)
		md.RootAnchor = s.Executor.CurrentEpochHash
		b, err := md.MarshalJSON()
		if err != nil {
			return errors.BadRequest.WithFormat("encode send tokens metadata: %w", err)
		}
		tx.Header.Metadata = b

		// Put on signing queue
		onSuccess = append(onSuccess, func() {
			s.shouldSignUserAction(tx)
		})

	case *requests.AddAccount:
		// Account and type are required
		if req.Account == nil {
			return errors.BadRequest.With("missing account")
		}
		if req.Type == 0 {
			return errors.BadRequest.With("missing type")
		}

		// The account must be an ADI token account
		theAccount, err := loadAccount[*protocol.TokenAccount](batch, req.Account)
		if err != nil {
			return errors.UnknownError.Wrap(err)
		}
		account = theAccount

		//// The payout (if specified) must be a token account
		//if req.Payout != nil {
		//	tokenAccount, err := loadAccount[protocol.AccountWithTokens](batch, req.Payout)
		//	if err != nil {
		//		return errors.UnknownError.Wrap(err)
		//	}
		//	//check to make sure the token account is an acme account
		//	if !tokenAccount.GetTokenUrl().Equal(protocol.AcmeUrl()) {
		//		return errors.UnknownError.WithFormat("payout account %s is not an ACME token account", tokenAccount.GetUrl())
		//	}
		//}

		if req.Transaction == nil {
			// If the transaction is omitted, the account must be owned by staking
			if !internal.IsOwnedByStaking(s.Executor.Params, theAccount) {
				return errors.BadRequest.WithFormat("%v is not owned by %v", req.Account, s.Executor.Params.Account.Authority)
			}

		} else {
			// TODO Verify that the transaction is pending

			// If the transaction is included, it must be a conversion transaction
			var msg *messaging.TransactionMessage
			err = batch.Message(req.Transaction.Hash()).Main().GetAs(&msg)
			if err != nil {
				return errors.BadRequest.WithFormat("load %v: %w", req.Transaction, err)
			}
			if !internal.IsStakingRequest(s.Executor.Params, msg.GetTransaction()) {
				return errors.BadRequest.WithFormat("%v is not a staking request", req.Transaction)
			}

			// Sign it
			onSuccess = append(onSuccess, func() {
				s.shouldSignFromUser(msg.Transaction)
			})
		}

		acct := new(types.Account)
		acct.Url = req.Account
		acct.Type = req.Type
		//if req.Payout == nil {
		//	acct.Payout = acct.Url
		//} else {
		//	acct.Payout = req.Payout
		//}

		approved := x.Approved[acct.Url.RootIdentity().AccountID32()]
		switch acct.Type {
		case types.AccountTypePure:
			if approved == nil || !approved.Pure {
				return errors.BadRequest.WithFormat("%v is not approved to be a pure staker", acct.Url.RootIdentity())
			}
		case types.AccountTypeDelegated:
			if req.Delegate == nil {
				return errors.BadRequest.With("missing delegate")
			}
		default:
			if approved == nil || !approved.Operator {
				return errors.BadRequest.WithFormat("%v is not approved to be a %v", acct.Url.RootIdentity(), acct.Type)
			}
		}

		// Is the account/identity already registered?
		iid := req.Account.RootIdentity().AccountID32()
		id, ok := x.Registered[iid]
		if ok {
			for _, a := range id.Accounts {
				if a.Url.Equal(req.Account) {
					return errors.Conflict.WithFormat("%v is already registered", req.Account)
				}
			}
		} else {
			id = &types.Identity{Identity: req.Account.RootIdentity()}
			x.Registered[iid] = id
		}

		// Register it
		id.Accounts = append(id.Accounts, acct)
		onSuccess = append(onSuccess, func() {
			s.didUpdate(txn.Transaction.ID(), id)
		})

	default:
		return errors.BadRequest.WithFormat("action %v is not supported", req.ActionType())
	}

	err := isSignedV1(s, account, txn.Transaction.ID())
	if err != nil {
		return errors.UnknownError.Wrap(err)
	}

	err = isSignedV2(s, account, txn.Transaction.ID())
	if err != nil {
		return errors.UnknownError.Wrap(err)
	}

	for _, fn := range onSuccess {
		fn()
	}
	return nil
}

// loadAccount loads an account and verifies that it is the expected type.
func loadAccount[T protocol.Account](batch *light.DB, url *url.URL) (T, error) {
	var z T
	a, err := batch.Account(url).Main().Get()
	switch {
	case err == nil:
		// Ok
	case errors.Is(err, errors.NotFound):
		return z, errors.BadRequest.WithFormat("%v does not exist", url)
	default:
		return z, errors.UnknownError.WithFormat("load %v: %w", url, err)
	}
	ta, ok := a.(T)
	if !ok {
		return z, errors.BadRequest.WithFormat("%v is not a token account", url)
	}
	return ta, nil
}

func isSignedV1(ctx *StateContext, account protocol.FullAccount, txid *url.TxID) error {
	if ctx.executorVersion.V2() {
		return nil
	}

	// We can only handle one page with M=1
	var book *protocol.KeyBook
	for _, auth := range account.GetAuth().Authorities {
		if auth.Url.Equal(ctx.Executor.Params.Account.Authority) {
			continue
		}
		if book != nil {
			return errors.NotAllowed.With("complex authorization structures are not supported at this time")
		}
		_, err := api.Querier2{Querier: ctx.Executor.APIv3}.QueryAccountAs(context.TODO(), auth.Url, nil, &book)
		if err != nil {
			return errors.FatalError.WithFormat("query %v: %w", auth.Url, err)
		}
		if book.PageCount > 1 {
			return errors.NotAllowed.With("complex authorization structures are not supported at this time")
		}
	}

	var page *protocol.KeyPage
	_, err := api.Querier2{Querier: ctx.Executor.APIv3}.QueryAccountAs(context.TODO(), book.Url.JoinPath("1"), nil, &page)
	if err != nil {
		return errors.FatalError.WithFormat("query %v/1: %w", book.Url, err)
	}
	if page.AcceptThreshold > 1 {
		return errors.NotAllowed.With("complex authorization structures are not supported at this time")
	}

	// Check for a signature
	r, err := api.Querier2{Querier: ctx.Executor.APIv3}.QueryTransaction(context.TODO(), txid, nil)
	if err != nil {
		return errors.UnknownError.WithFormat("query %v: %w", txid, err)
	}

	for _, set := range r.Signatures.Records {
		if set.Account.GetUrl().Equal(page.Url) {
			return nil // Found a signature
		}
	}
	return errors.BadRequest.WithFormat("%v is not signed by %v", txid, book.Url)
}

func isSignedV2(ctx *StateContext, account protocol.FullAccount, txid *url.TxID) error {
	if !ctx.executorVersion.V2() {
		return nil
	}

	authorities := map[[32]byte]*url.URL{}
	for _, auth := range account.GetAuth().Authorities {
		if auth.Url.Equal(ctx.Executor.Params.Account.Authority) {
			continue
		}
		authorities[auth.Url.AccountID32()] = auth.Url
	}

	authSigs, err := loadAuthoritySignaturesV2(ctx, txid, account.GetUrl())
	if err != nil {
		return errors.UnknownError.Wrap(err)
	}
	for _, sig := range authSigs {
		// Only count votes, not intermediate delegated authority signatures
		if len(sig.Delegator) > 0 {
			continue
		}
		delete(authorities, sig.Authority.AccountID32())
	}
	if len(authorities) > 0 {
		var s []string
		for _, u := range authorities {
			s = append(s, u.String())
		}
		return errors.BadRequest.WithFormat("%v is not signed by %v", txid, strings.Join(s, ", "))
	}
	return nil
}

func loadAuthoritySignaturesV2(ctx *StateContext, txid *url.TxID, account *url.URL) ([]*protocol.AuthoritySignature, error) {
	res, err := api.Querier2{Querier: ctx.Executor.APIv3}.QueryTransaction(context.TODO(), txid, nil)
	if err != nil {
		return nil, errors.UnknownError.WithFormat("query %v: %w", txid, err)
	}

	var sigs []*protocol.AuthoritySignature
	for _, set := range res.Signatures.Records {
		if !set.Account.GetUrl().Equal(txid.Account()) {
			continue
		}

		for _, sig := range set.Signatures.Records {
			msg, ok := sig.Message.(*messaging.SignatureMessage)
			if !ok {
				continue
			}
			auth, ok := msg.Signature.(*protocol.AuthoritySignature)
			if !ok {
				continue
			}
			sigs = append(sigs, auth)
		}
	}
	return sigs, nil
}
