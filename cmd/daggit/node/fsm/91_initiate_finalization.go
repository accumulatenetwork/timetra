package fsm

import (
	"encoding/json"

	"gitlab.com/accumulatenetwork/accumulate/pkg/build"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/accumulate/vdk/node"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/internal"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/pkg/types"
)

var _ StateDelegate = (*initiateFinalization)(nil)

type initiateFinalization struct {
}

func (s *initiateFinalization) execute(ctx *StateContext, d *node.DidCommitBlock) StateDelegate {
	//store the ctx.Executor.CurrentEpochBlock in the data account to mark success.
	ss := types.SaveState{}
	ss.CommitBlock.Time = ctx.Executor.CurrentEpochBlock.Time
	ss.CommitBlock.Index = ctx.Executor.CurrentEpochBlock.Index
	ss.CommitBlock.Major = ctx.Executor.CurrentEpochBlock.Major
	ss.Leader = *ctx.leader

	if ctx.payoutTx == nil {
		//nothing was done for the update
		return ctx.MakeCurrent(StateReset)
	}
	md := new(types.StateHeader)
	md.RootAnchor = ctx.Executor.CurrentEpochHash
	md.Transactions = append(md.Transactions, ctx.payoutTx.ID())
	mdd, err := md.MarshalJSON()
	if err != nil {
		return ctx.Fatal(errors.EncodingError).WithFormat("unable to marshal save state metadata: %w", err)
	}

	data, err := json.Marshal(&ss)
	if err != nil {
		return ctx.Fatal(errors.EncodingError).WithFormat("unable to marshal save state: %w", err)
	}

	key, keyType, err := ctx.Executor.Wallet.GetLockedKey()
	if err != nil {
		return ctx.Fatal(errors.Unauthorized).WithFormat("failed to get key from wallet: %v", err)
	}
	defer key.Destroy()

	//build base transaction
	env, err := build.Transaction().For(ctx.Executor.Params.Account.Epoch).Metadata(mdd).
		WriteData(&protocol.DoubleHashDataEntry{Data: [][]byte{data}}).
		SignWith(ctx.Executor.Params.Account.Validators).
		Version(ctx.validators.Version).
		Timestamp(&ctx.timestamp).
		Type(keyType).Signer(internal.SecureSigner(key)).
		Done()

	if err != nil {
		return ctx.Fatal(errors.UnknownError).WithFormat("failed to build save state transaction in finalize epoch: %w", err)
	}

	err = ctx.Executor.dispatchTransaction(env)
	if err != nil {
		return ctx.Fatal(errors.FatalError).WithFormat("failed to dispatch request: %w", err)
	}
	ctx.toFinalize = env.Transaction[0]
	ctx.Executor.PrevEpochBlock = ctx.Executor.CurrentEpochBlock
	return ctx.MakeCurrent(StateDidProcessFinalization)
}
