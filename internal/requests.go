package internal

import (
	"gitlab.com/accumulatenetwork/accumulate/exp/light"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/cmd/daggit/app"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/pkg/requests"
)

// BadRequestTx represents an invalid request on the requests account.
// BadRequestTx is used for requests that are malformed, e.g. cannot be parsed,
// _not_ for actions that fail, such as attempting to register an account that
// does not exist.
type BadRequestTx struct {
	TxExecuted
	Data  protocol.DataEntry
	Error error
}

// RequestTx is an action and the transaction it came from.
type RequestTx struct {
	TxExecuted
	Action requests.Action
}

// LoadRequests loads requests from the requests account for the given epoch.
func LoadRequests(params *app.Parameters, batch *light.DB, epoch [2]uint64) ([]*RequestTx, []*BadRequestTx, error) {
	var actions []*RequestTx
	var invalid []*BadRequestTx
	err := scanTransactions(batch, params.Account.Requests, func(info *TxExecuted) (stop bool, err error) {
		if info.MajorBlock < epoch[0] {
			// Ignore transactions before the beginning
			return false, nil
		}
		if info.MajorBlock >= epoch[1] {
			// Stop once we reach the end
			return true, nil
		}

		// Get the data entry
		var entry protocol.DataEntry
		switch body := info.Transaction.Body.(type) {
		case *protocol.WriteData:
			entry = body.Entry
		case *protocol.WriteDataTo:
			entry = body.Entry
		case *protocol.SyntheticWriteData:
			entry = body.Entry
		case *protocol.SystemWriteData:
			entry = body.Entry
		default:
			// Ignore non-data transactions
			return false, nil
		}

		// Parse the actions
		a, err := requests.ParseActions(entry.GetData())
		if err != nil {
			invalid = append(invalid, &BadRequestTx{*info, entry, err})
			return false, nil
		}

		// Construct a RequestTx for each action
		for _, a := range a {
			actions = append(actions, &RequestTx{*info, a})
		}
		return false, nil
	})
	return actions, invalid, errors.UnknownError.Wrap(err)
}
