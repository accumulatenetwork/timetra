package internal

import (
	"context"
	"fmt"

	"gitlab.com/accumulatenetwork/accumulate/exp/light"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/cmd/daggit/app"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/pkg/api"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/pkg/requests"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/pkg/types"
)

type StakingService struct {
	Client *light.Client
	Params *app.Parameters
}

var _ api.StakingService = (*StakingService)(nil)

func (s *StakingService) SubmitAction(ctx context.Context, action *requests.Action) error {
	return fmt.Errorf("TODO: implement submit action")
}

func (s *StakingService) AccountStatus(ctx context.Context, account *url.URL, _ api.AccountStatusOptions) (*api.AccountStatus, error) {
	batch := s.Client.OpenDB(false)
	defer batch.Discard()

	id, err := LoadRegisteredIdentity(batch, s.Params, account)
	if err != nil {
		return nil, errors.UnknownError.Wrap(err)
	}

	var acct *types.Account
	for _, a := range id.Accounts {
		if a.Url.Equal(account) {
			acct = a
			break
		}
	}
	if acct == nil {
		return nil, errors.NotFound.WithFormat("account %v has not been registered", account)
	}

	var stake protocol.AccountWithTokens
	err = batch.Account(acct.Url).Main().GetAs(&stake)
	if err != nil {
		return nil, errors.UnknownError.WithFormat("load %v: %w", acct.Url, err)
	}

	status := new(api.AccountStatus)
	status.Account = acct
	status.Balance = stake.TokenBalance()

	err = scanTransactions(batch, acct.Url, func(info *TxExecuted) (stop bool, err error) {
		if IsStakingRequest(s.Params, info.Transaction) {
			status.Staked = &api.TxWhen{
				TxID: info.Transaction.ID(),
				Time: info.Executed.LocalTime,
			}
		}

		switch info.Transaction.Body.(type) {
		case *protocol.SendTokens,
			*protocol.SyntheticDepositTokens,
			*protocol.BurnTokens,
			*protocol.AddCredits:
			status.LastChange = &api.TxWhen{
				TxID: info.Transaction.ID(),
				Time: info.Executed.LocalTime,
			}
		}
		return false, nil
	})
	if err != nil {
		return nil, errors.UnknownError.Wrap(err)
	}
	return status, nil
}
