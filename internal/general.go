package internal

import (
	"context"
	"encoding/json"

	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
)

func GetTokenAccount(c *client.Client, u *url.URL) *protocol.TokenAccount {
	req := new(client.GeneralQuery)
	req.Url = u
	account := new(protocol.TokenAccount)
	_, err := c.QueryAccountAs(context.Background(), req, account)
	if err != nil {
		return nil
	}
	return account
}

func remarshal[U any](v any) (*U, error) {
	u := new(U)
	b, err := json.Marshal(v)
	if err != nil {
		return nil, errors.EncodingError.WithFormat("encode response value: %w", err)
	}
	err = json.Unmarshal(b, u)
	if err != nil {
		return nil, errors.EncodingError.WithFormat("decode response value: %w", err)
	}
	return u, nil
}
