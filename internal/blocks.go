package internal

import (
	"context"
	"fmt"

	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
)

func FindMajorBlock(c *client.Client, partition string, major uint64) (*protocol.IndexEntry, error) {
	chainIndex := major - 1

	for i := int64(major) - 1; i >= 0; i-- {
		anchors := protocol.PartitionUrl(partition).JoinPath(protocol.AnchorPool)
		req := new(client.GeneralQuery)
		req.Url = anchors.WithFragment(fmt.Sprintf("chain/major-block/%d", chainIndex))

		ie := new(protocol.IndexEntry)
		ce := new(client.ChainEntry)
		ce.Value = ie
		res := new(client.ChainQueryResponse)
		res.Data = ce
		err := c.RequestAPIv2(context.Background(), "query", req, res)
		if err != nil {
			return nil, errors.UnknownError.WithFormat("query major block: %w", err)
		}

		if ie.BlockIndex == major {
			return ie, nil
		}
		if ie.BlockIndex < major {
			return nil, errors.NotFound.WithFormat("cannot find major block %d", major)
		}
	}

	return nil, errors.NotFound.WithFormat("cannot find major block %d", major)
}
