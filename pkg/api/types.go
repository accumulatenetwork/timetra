package api

import (
	"gitlab.com/accumulatenetwork/accumulate/pkg/api/v3"
	"gitlab.com/accumulatenetwork/accumulate/pkg/api/v3/message"
)

//go:generate go run gitlab.com/accumulatenetwork/accumulate/tools/cmd/gen-types --package api types.yml --go-include gitlab.com/accumulatenetwork/ecosystem/daggitron/pkg/types

type MessageType = message.Type
type Message = message.Message

const ServiceTypeDaggitron api.ServiceType = 0xF100

// Request types
const (
	MessageTypeAccountStatusRequest MessageType = 0xF101 + iota
	MessageTypeEventNotify
)

// Response types
const (
	MessageTypeAccountStatusResponse MessageType = 0xF121 + iota
	MessageTypeEventNotifyResponse
)

func init() {
	// Register staking's message types
	must(message.RegisterMessageType[*AccountStatusRequest]("accountStatusRequest"))
	must(message.RegisterMessageType[*AccountStatusResponse]("accountStatusResponse"))
}

func must(err error) {
	if err != nil {
		panic(err)
	}
}

func (a *AccountStatus) Compare(b *AccountStatus) int {
	return a.Account.Url.Compare(b.Account.Url)
}
