package fsm

import (
	"gitlab.com/accumulatenetwork/accumulate/vdk/node"
)

var _ StateDelegate = (*tryAgainError)(nil)

type tryAgainError struct {
}

func (s *tryAgainError) execute(ctx *StateContext, d *node.DidCommitBlock) StateDelegate {
	//need to reset appropriate states, invalidate any transactions, and try again in a new round
	ctx.Executor.PrevBlock = ctx.Executor.CurrentEpochBlock
	ctx.retryAttempts++
	ctx.resetForTryAgain()
	return ctx.MakeCurrent(StateCanBegin)
}
