package fsm

import "gitlab.com/accumulatenetwork/accumulate/vdk/node"

var _ StateDelegate = (*finalizeEpoch)(nil)

type finalizeEpoch struct {
}

func (s *finalizeEpoch) execute(ctx *StateContext, d *node.DidCommitBlock) StateDelegate {
	//reset all the information in context and wait for next epoch
	if ctx.AmLeader {
		return ctx.MakeCurrent(StateInitiateFinalization)
	}
	return ctx.MakeCurrent(StateDidInitiateFinalization)
}
