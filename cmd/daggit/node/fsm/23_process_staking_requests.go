package fsm

import (
	"crypto/sha256"
	"encoding/json"

	"gitlab.com/accumulatenetwork/accumulate/pkg/build"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/accumulate/vdk/node"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/internal"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/pkg/types"
)

var _ StateDelegate = (*processStakingRequests)(nil)

// Process staking requests
//  1. query the transaction via the one stored in the context.
//  2. build the transaction body and compare the transaction with the one that has been initiated by the leader
//  3. if the transaction's match, then sign the one submitted by the leader,
//  4. if the transactions do not match execute a failure state, select new leader, and try again in a new round
type processStakingRequests struct {
}

func (s *processStakingRequests) execute(ctx *StateContext, d *node.DidCommitBlock) StateDelegate {
	// Build a map of the transactions we expect to find
	expected := map[[32]byte]*types.Identity{}
	for _, up := range ctx.updates {
		b, err := up.StateHeader.MarshalBinary()
		if err != nil {
			return ctx.Fatal(errors.EncodingError).WithFormat("marshal request metadata: %w", err)
		}
		expected[sha256.Sum256(b)] = up.Identity
	}

	// For each staking action to sign
	for _, txn := range ctx.toSign.StakingActions {
		// Verify the metadata
		var md types.StateHeader
		err := md.UnmarshalJSON(txn.Header.Metadata)
		if err != nil {
			return ctx.TryAgain(errors.BadRequest).WithFormat("decode staking action metadata: %w", err)
		}
		if md.RootAnchor != ctx.Executor.CurrentEpochHash {
			return ctx.TryAgain(errors.BadRequest).WithFormat("staking action is for the wrong block: %w", err)
		}

		body, ok := txn.Body.(*protocol.WriteData)
		if !ok || len(body.Entry.GetData()) != 1 {
			return ctx.TryAgain(errors.BadRequest).With("invalid staking action: expected WriteData with one part")
		}

		// Parse the entry
		var data *types.Identity
		err = json.Unmarshal(body.Entry.GetData()[0], &data)
		if err != nil {
			return ctx.TryAgain(errors.BadRequest).With("invalid staking action: %w", err)
		}

		// Compare the metadata to the expected list
		b, err := md.MarshalBinary()
		if err != nil {
			return ctx.Fatal(errors.EncodingError).WithFormat("marshal request metadata: %w", err)
		}
		h := sha256.Sum256(b)
		if id, ok := expected[h]; ok {
			if !id.Equal(data) {
				return ctx.TryAgain(errors.Conflict).With("conflicting registration list entry")
			}
			delete(expected, h)
		} else {
			return ctx.TryAgain(errors.Conflict).With("found unexpected registration list entry")
		}
	}
	if len(expected) != 0 {
		return ctx.TryAgain(errors.Conflict).With("missing expected registration list entry")
	}

	// Sign the transaction. Don't omit the timestamp because of a weird bug.
	key, keyType, err := ctx.Executor.Wallet.GetLockedKey()
	if err != nil {
		return ctx.Fatal(errors.UnknownError).WithFormat("failed to get key from wallet %w", err)
	}
	defer key.Destroy()

	for _, txn := range ctx.toSign.all() {
		env, err := build.SignatureForTransaction(txn).
			Url(ctx.Executor.Params.Account.Validators).
			Version(ctx.validators.Version).Timestamp(&ctx.timestamp).
			Type(keyType).Signer(internal.SecureSigner(key)).
			Done()
		if err != nil {
			return ctx.Fatal(errors.UnknownError).Wrap(err)
		}

		ctx.Executor.Logger.Info("Signed request", "txid", txn.ID())
		ctx.shouldSubmit(env)
	}

	return ctx.MakeCurrent(StateDidProcessStakingRequests)
}
