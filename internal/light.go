package internal

import (
	"context"
	"encoding/json"
	"fmt"
	"math"
	"math/big"
	"sort"

	"gitlab.com/accumulatenetwork/accumulate/exp/light"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/messaging"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/network"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/cmd/daggit/app"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/pkg/types"
	"golang.org/x/exp/slog"
)

// StakingInfo records the status of a staking account at the end of a staking
// epoch.
type StakingInfo struct {
	Identity         *types.Identity
	Account          *types.Account
	CurrentState     *protocol.TokenAccount
	Transactions     []*TxExecuted
	EffectiveType    types.AccountType
	Balance          *big.Int
	Deposits         *big.Int
	Withdraws        *big.Int
	WeightedBalance  *big.Rat
	DelegatedBalance *big.Rat

	DelegatedType   types.AccountType
	DelegatedPayout *url.URL
}

// TxExecuted is a transaction and information about when it was executed.
type TxExecuted struct {
	Transaction *protocol.Transaction
	Executed    *light.EventMetadata
	MajorBlock  uint64
}

// CalculateBudget calculates the budget for a staking epoch.
func CalculateBudget(batch *light.DB, params *app.Parameters, epoch [2]uint64) (*big.Rat, error) {
	// Load the issuer
	var issuer *protocol.TokenIssuer
	err := batch.Account(params.Account.TokenIssuance).Main().GetAs(&issuer)
	if err != nil {
		return nil, errors.UnknownError.WithFormat("load %v: %w", params.Account.TokenIssuance, err)
	}

	// Roll back changes that occurred after the end of the epoch
	unissued := new(big.Int)
	unissued.Sub(issuer.SupplyLimit, &issuer.Issued)
	err = scanTransactions(batch, params.Account.TokenIssuance, func(info *TxExecuted) (stop bool, err error) {
		if info.MajorBlock < epoch[1] {
			// Ignore anything before the end of the epoch
			return false, nil
		}

		switch body := info.Transaction.Body.(type) {
		case *protocol.BlockValidatorAnchor:
			unissued.Sub(unissued, &body.AcmeBurnt)
		case *protocol.IssueTokens:
			unissued.Add(unissued, &body.Amount)
		case *protocol.SyntheticBurnTokens:
			unissued.Sub(unissued, &body.Amount)
		}
		return false, nil
	})
	if err != nil {
		return nil, errors.UnknownError.Wrap(err)
	}

	// Budget = unissued * rate / 365 * 7
	budget := big.NewRat(1, protocol.AcmePrecision)
	budget.Num().Set(unissued)
	//budget.Mul(budget, params.TokenIssuanceRate.BigRat())
	//budget.Mul(budget, big.NewRat(7, 365))
	return budget, nil
}

// Staking analyzes all staked accounts and determines the payout due to each
// for the given epoch. Staking returns the status of each staking account and a
// list of payouts.
//
//   - majorBlock specifies the start (inclusive) and end (exclusive) of the
//     epoch.
//   - budget is the total payout budget.
//   - weights determines the weight for each staking account type, with a
//     default weight of 1 (if a given type is not present in the map).
//   - delegationFee is the percentage of a delegator's payout that is
//     transferred to their delegate.
func Staking(c *light.Client, params *app.Parameters, majorBlock [2]uint64, budget *big.Rat) ([]*StakingInfo, []*protocol.TokenRecipient, error) {
	if majorBlock[0] >= majorBlock[1] {
		return nil, nil, errors.BadRequest.WithFormat("invalid major block range: %d ≥ %d", majorBlock[0], majorBlock[1])
	}

	batch := c.OpenDB(false)
	defer batch.Discard()

	// Verify that the end of the epoch is a known major block. If it is, the
	// start must also be. If it is not, the database may be out of date.
	{
		majorIndex, err := batch.Index().Account(protocol.DnUrl().JoinPath(protocol.AnchorPool)).Chain("major-block").Index().Get()
		if err != nil {
			return nil, nil, errors.UnknownError.WithFormat("load major block index: %w", err)
		}
		_, _, ok := light.FindEntry(majorIndex, light.ByIndexBlock(majorBlock[1]))
		if !ok {
			return nil, nil, errors.NotFound.WithFormat("cannot locate major block %d", majorBlock[1])
		}
	}

	// Load parameters
	//delegationFee := params.DelegationFee.BigRat()
	//weights := map[types.AccountType]*big.Rat{}
	//for _, w := range params.StakingWeight {
	//	weights[w.Type] = w.BigRat()
	//}

	// Load all the registered accounts
	registered, err := LoadAllRegistered(batch, params)
	if err != nil {
		return nil, nil, errors.UnknownError.Wrap(err)
	}

	// Sort by identity - this is unnecessary but it makes the output more human
	// friendly
	sort.Slice(registered, func(i, j int) bool {
		a, b := registered[i], registered[j]
		return a.Identity.Compare(b.Identity) < 0
	})

	// Load the history of each staking account
	var accounts []*StakingInfo
	delegates := map[[32]byte]*StakingInfo{}
	for _, i := range registered {
		for _, a := range i.Accounts {
			s, err := LoadStakingAccountHistory(params, batch, i, a, majorBlock)
			if err != nil {
				return nil, nil, errors.UnknownError.Wrap(err)
			}
			if s == nil {
				continue
			}

			// If the payout account is invalid, send the payout to the staking
			// account
			payout, err := batch.Account(s.Account.Payout).Main().Get()
			switch {
			case err == nil:
				// It must be a token account
				if _, ok := payout.(protocol.AccountWithTokens); !ok {
					a.Payout = a.Url
				}
			case errors.Is(err, errors.NotFound):
				// A non-existent LTA is OK
				if key, tok, _ := protocol.ParseLiteTokenAddress(a.Payout); key == nil || !protocol.AcmeUrl().Equal(tok) {
					a.Payout = a.Url
				}
			default:
				return nil, nil, errors.UnknownError.Wrap(err)
			}

			// Determine values for delegates
			if i.Type != types.AccountTypeDelegated && !i.RejectDelegates {
				s.DelegatedPayout = i.DelegatorPayout
				s.DelegatedBalance = new(big.Rat)
				delegates[i.Identity.IdentityAccountID32()] = s

				// Delegates inherit the account type from whichever account has
				// the highest weight (the first such account if there are
				// multiple candidates)
				var typ types.AccountType
				//var weight *big.Rat
				var payout *url.URL
				//for _, a := range i.Accounts {
				//	w, ok := weights[a.Type]
				//	if !ok {
				//		w = big.NewRat(1, 1)
				//	}
				//	if weight == nil || weight.Cmp(w) < 0 {
				//		weight, typ, payout = w, a.Type, a.Payout
				//	}
				//}
				s.DelegatedType = typ

				// If the delegate has not specified an explicit payout address,
				// use the account from above
				if s.DelegatedPayout == nil {
					s.DelegatedPayout = payout
				}
			}

			// Calculate the balance
			s.Balance = new(big.Int)
			s.Deposits = new(big.Int)
			s.Withdraws = new(big.Int)
			s.Balance.Add(s.Balance, &s.CurrentState.Balance)
			for _, info := range s.Transactions {
				delta := getBalanceChange(info.Transaction)
				switch {
				case delta == nil:
					// Ignore

				case delta.Sign() > 0:
					// Subtract deposits from the balance (if they're during or after the epoch)
					s.Balance.Sub(s.Balance, delta)

					// And track them if they're during the epoch
					if info.MajorBlock <= majorBlock[1] {
						s.Deposits.Add(s.Deposits, delta)
					}

				case delta.Sign() < 0:
					if info.MajorBlock > majorBlock[1] {
						// Subtract withdraws from the balance if they're after the epoch
						s.Balance.Sub(s.Balance, delta)

					} else {
						// And track them if they're during the epoch
						s.Withdraws.Sub(s.Withdraws, delta)
					}
				}
			}

			// If the balance is below the minimum, zero the effective balance
			var min *big.Int
			if a.Type == types.AccountTypeDelegated {
				min = params.DelegatedMinimum
			} else {
				min = params.FullStakeMinimum
			}
			if s.Balance.Cmp(min) < 0 {
				s.Balance.SetInt64(0)
			}

			// Add the account to the list
			accounts = append(accounts, s)
		}
	}

	// Determine effective list and apply weight
	weightedTotal := new(big.Rat)
	for _, s := range accounts {
		// Determine the effective type and add to the delegated list
		switch {
		case s.Account.Type != types.AccountTypeDelegated:
			s.EffectiveType = s.Account.Type
		case s.Account.Delegate == nil:
			fmt.Printf("Delegated account %v has no delegate\n", s.Account.Url)
			continue
		case delegates[s.Account.Delegate.IdentityAccountID32()] == nil:
			fmt.Printf("%v delegates to %v which is not a staking account\n", s.Account.Url, s.Account.Delegate)
			continue
		default:
			s.EffectiveType = delegates[s.Account.Delegate.IdentityAccountID32()].DelegatedType
		}

		// Apply weight
		s.WeightedBalance = new(big.Rat)
		s.WeightedBalance.SetInt(s.Balance)
		s.WeightedBalance.Mul(s.WeightedBalance, big.NewRat(1, protocol.AcmePrecision))
		//if w, ok := weights[s.EffectiveType]; ok {
		//	s.WeightedBalance.Mul(s.WeightedBalance, w)
		//}

		// Add to total
		weightedTotal.Add(weightedTotal, s.WeightedBalance)
	}

	// Avoid a panic during testing if there aren't any stakers yet
	if weightedTotal.Sign() == 0 {
		return nil, nil, nil
	}

	// Payout per weighed token = budget / weighted token total
	payPer := new(big.Rat)
	payPer.Quo(budget, weightedTotal)

	// Delegator multiplier = 1 - delegation fee
	delegatorMultiplier := big.NewRat(1, 1)
	//delegatorMultiplier.Sub(delegatorMultiplier, delegationFee)

	// addOutput is a callback that will either add a new output for the account
	// or add the amount to its existing output, ensuring that there is only a
	// single output per account
	var outputs []*protocol.TokenRecipient
	index := map[[32]byte]int{}
	addOutput := func(u *url.URL, amount *big.Rat) {
		// Find the existing output or create a new one
		var out *protocol.TokenRecipient
		i, ok := index[u.AccountID32()]
		if ok {
			out = outputs[i]
		} else {
			index[u.AccountID32()] = len(outputs)
			out = &protocol.TokenRecipient{Url: u}
			outputs = append(outputs, out)
		}

		// Output += num(amount) * ACME precision / denom(amount)
		a := new(big.Int).Mul(amount.Num(), big.NewInt(protocol.AcmePrecision))
		a.Quo(a, amount.Denom())
		out.Amount.Add(&out.Amount, a)
	}

	// For each staking account
	for _, a := range accounts {
		if a.WeightedBalance == nil {
			continue
		}

		// Calculate payout amount
		amount := new(big.Rat).Mul(a.WeightedBalance, payPer)

		// If it's not delegated, add it directly
		if a.Account.Type != types.AccountTypeDelegated {
			addOutput(a.Account.Payout, amount)
			continue
		}

		// Track the total amount delegated to the delegate (for reporting)
		id := a.Account.Delegate.IdentityAccountID32()
		x := delegates[id].DelegatedBalance
		x.Add(x, a.WeightedBalance)

		// Delegator payout += payout * (1 - delegation fee)
		addOutput(a.Account.Payout, new(big.Rat).Mul(amount, delegatorMultiplier))

		// Delegate payout += payout * delegation fee
		//addOutput(delegates[id].DelegatedPayout, new(big.Rat).Mul(amount, delegationFee))
	}

	return accounts, outputs, nil
}

// GetDirectoryAnchor returns the directory anchor at a given block index
func GetDirectoryAnchor(batch *light.DB, blockIndex uint64) ([]byte, error) {
	// Get the anchor hash
	rootIndex, err := batch.Index().Account(protocol.DnUrl().JoinPath(protocol.Ledger)).Chain("root").Index().Get()
	if err != nil {
		return nil, errors.UnknownError.WithFormat("load root index: %w", err)
	}

	_, rootEntry, ok := light.FindEntry(rootIndex, light.ByIndexBlock(blockIndex))
	if !ok || rootEntry.BlockIndex != blockIndex {
		return nil, errors.NotFound.WithFormat("cannot find anchor for block %d", blockIndex)
	}

	ms, err := batch.Account(protocol.DnUrl().JoinPath(protocol.Ledger)).RootChain().Inner().GetAnyState(int64(rootEntry.Source))
	if err != nil {
		return nil, errors.UnknownError.WithFormat("get merkle state for root chain entry %d: %w", rootEntry.Source, err)
	}

	return ms.Anchor(), nil
}

func LoadAllApproved(batch *light.DB, params *app.Parameters) ([]*types.Approved, error) {
	// Process each entry in reverse order
	seen := map[[32]byte]bool{}
	var accounts []*types.Approved
	err := scanEntries(batch, params.Account.ApprovedADIs, func(entry *types.Approved) (stop bool, err error) {
		// Only include the latest entry for a given identity
		if !seen[entry.Identity.AccountID32()] {
			seen[entry.Identity.AccountID32()] = true
			accounts = append(accounts, entry)
		}
		return false, nil
	})
	return accounts, errors.UnknownError.Wrap(err)
}

func LoadRegisteredIdentity(batch *light.DB, params *app.Parameters, url *url.URL) (*types.Identity, error) {
	var id *types.Identity
	err := scanRegistered(batch, params, func(entry *types.Identity) (stop bool, err error) {
		if entry.Identity.LocalTo(url) {
			id = entry
			return true, nil
		}
		return false, nil
	})
	if err != nil {
		return nil, errors.UnknownError.Wrap(err)
	}
	if id == nil {
		return nil, errors.NotFound.WithFormat("no entry found for %v", url)
	}
	return id, nil
}

// LoadAllRegistered returns the list of registered staking accounts.
func LoadAllRegistered(batch *light.DB, params *app.Parameters) ([]*types.Identity, error) {
	// Process each entry in reverse order
	seen := map[[32]byte]bool{}
	var accounts []*types.Identity
	err := scanRegistered(batch, params, func(entry *types.Identity) (stop bool, err error) {
		// Only include the latest entry for a given identity
		if !seen[entry.Identity.AccountID32()] {
			seen[entry.Identity.AccountID32()] = true
			accounts = append(accounts, entry)
		}
		return false, nil
	})
	return accounts, errors.UnknownError.Wrap(err)
}

func scanRegistered(batch *light.DB, params *app.Parameters, fn func(*types.Identity) (stop bool, err error)) error {
	return scanEntries(batch, params.Account.RegisteredADIs, func(entry *types.Identity) (stop bool, err error) {
		if entry.Identity == nil {
			b, _ := json.Marshal(entry)
			fmt.Printf("Skipping invalid entry: %s\n", b)
			return false, nil
		}

		// Normalize the format
		entry.Normalize()

		return fn(entry)
	})
}

func scanEntries[T any](batch *light.DB, account *url.URL, fn func(*T) (stop bool, err error)) error {
	// Get all of the entries
	head, err := batch.Account(account).MainChain().Head().Get()
	if err != nil {
		return errors.UnknownError.WithFormat("load main chain head: %w", err)
	}
	if head.Count == 0 {
		return nil
	}
	entries, err := batch.Account(account).MainChain().Inner().GetRange(0, head.Count)
	if err != nil {
		return errors.UnknownError.WithFormat("load main chain entries: %w", err)
	}

	// Process each entry in reverse order
	for i := len(entries) - 1; i >= 0; i-- {
		// Load the transaction
		var msg *messaging.TransactionMessage
		err = batch.Message2(entries[i]).Main().GetAs(&msg)
		if err != nil {
			return errors.UnknownError.WithFormat("load transaction: %w", err)
		}

		// Ignore anything that's not a data transaction
		body, ok := msg.Transaction.Body.(*protocol.WriteData)
		if !ok {
			continue
		}

		// Parse the entry (and skip any that are invalid)
		b := body.Entry.GetData()[0]
		entry := new(T)
		err = json.Unmarshal(b, &entry)
		if err != nil {
			fmt.Printf("Skipping invalid entry: %s\n", b)
			continue
		}

		stop, err := fn(entry)
		if stop || err != nil {
			return errors.UnknownError.Wrap(err)
		}
	}

	return nil
}

// LoadStakingAccountHistory loads the history of a staking account for the given epoch.
func LoadStakingAccountHistory(params *app.Parameters, batch *light.DB, identity *types.Identity, account *types.Account, majorBlock [2]uint64) (*StakingInfo, error) {
	history := new(StakingInfo)
	history.Identity = identity
	history.Account = account

	// Load the account
	state, err := batch.Account(account.Url).Main().Get()
	switch {
	case err == nil:
		// Ok
	case errors.Is(err, errors.NotFound):
		return nil, nil
	default:
		return nil, errors.UnknownError.WithFormat("load account state: %w", err)
	}

	// The account must be a token account
	var ok bool
	history.CurrentState, ok = state.(*protocol.TokenAccount)
	if !ok {
		return nil, nil
	}

	// The account must be owned by staking to receive rewards
	if !IsOwnedByStaking(params, history.CurrentState) {
		return nil, nil
	}

	err = scanTransactions(batch, account.Url, func(info *TxExecuted) (stop bool, err error) {
		// The account's authority transfer must have been executed prior to the
		// end of the staking period
		if IsStakingRequest(params, info.Transaction) && info.MajorBlock > majorBlock[1] {
			history = nil
			return true, nil
		}

		// Ignore anything before the beginning of the period
		if info.MajorBlock <= majorBlock[0] {
			return false, nil
		}

		history.Transactions = append(history.Transactions, info)
		return false, nil
	})
	return history, errors.UnknownError.Wrap(err)
}

func scanTransactions(batch *light.DB, url *url.URL, fn func(*TxExecuted) (stop bool, err error)) error {
	// Load the DN's root and major block indices
	rootIndex, err := batch.Index().Account(protocol.DnUrl().JoinPath(protocol.Ledger)).Chain("root").Index().Get()
	if err != nil {
		return errors.UnknownError.WithFormat("load minor block index: %w", err)
	}
	majorIndex, err := batch.Index().Account(protocol.DnUrl().JoinPath(protocol.AnchorPool)).Chain("major-block").Index().Get()
	if err != nil {
		return errors.UnknownError.WithFormat("load major block index: %w", err)
	}

	// For each main chain entry
	head, err := batch.Account(url).MainChain().Head().Get()
	if err != nil {
		return errors.UnknownError.WithFormat("load main chain head: %w", err)
	}
	if head.Count == 0 {
		return nil
	}
	entries, err := batch.Account(url).MainChain().Inner().GetRange(0, head.Count)
	if err != nil {
		return errors.UnknownError.WithFormat("load main chain entries: %w", err)
	}
	for _, e := range entries {
		// Load the transaction
		hash := *(*[32]byte)(e)
		var msg *messaging.TransactionMessage
		err = batch.Message(hash).Main().GetAs(&msg)
		if err != nil {
			return errors.UnknownError.WithFormat("load transaction: %w", err)
		}

		// Ignore forwarded signatures
		if msg.Transaction.Body.Type() == protocol.TransactionTypeSyntheticForwardTransaction {
			continue
		}

		// Load the execution metadata
		info := new(TxExecuted)
		info.Transaction = msg.Transaction
		info.Executed, err = batch.Index().Transaction(hash).Executed().Get()
		if err != nil {
			return errors.UnknownError.WithFormat("load transaction executed metadata: %w", err)
		}

		// Find the DN major block
		i, _, ok := light.FindEntry(rootIndex, light.ByIndexBlock(info.Executed.DirectoryBlock))
		if !ok {
			return errors.InternalError.WithFormat("cannot locate root index entry for directory block %d", info.Executed.DirectoryBlock)
		}

		if _, major, ok := light.FindEntry(majorIndex, light.ByIndexRootIndexIndex(uint64(i))); ok {
			info.MajorBlock = major.BlockIndex
		} else {
			// If the transaction is more recent than the last major block,
			// record the major block as 2^64-1 so that it appears to be later
			// than the latest major block
			info.MajorBlock = math.MaxUint64
		}
		stop, err := fn(info)
		if stop || err != nil {
			return errors.UnknownError.Wrap(err)
		}
	}

	return nil
}

// IsStakingRequest returns true if the transaction is an UpdateAccountAuth that
// transfers authority from the account to staking.acme/book
func IsStakingRequest(params *app.Parameters, txn *protocol.Transaction) bool {
	body, ok := txn.Body.(*protocol.UpdateAccountAuth)
	if !ok {
		return false
	}

	var didAdd, didDisable bool
	for _, op := range body.Operations {
		switch op := op.(type) {
		case *protocol.AddAccountAuthorityOperation:
			if !params.Account.Authority.Equal(op.Authority) {
				return false
			}
			didAdd = true

		case *protocol.DisableAccountAuthOperation:
			didDisable = true

		default:
			return false
		}
	}
	return didAdd && didDisable
}

// IsAccountLocked returns true if the account is unlocked, false otherwise
func IsAccountLocked(params *app.Parameters, account *protocol.TokenAccount) bool {
	//TODO: implement me when ready...
	return false
}

// IsOwnedByStaking returns true if the account is governed by staking.acme/book
// and all other authorities are disabled.
func IsOwnedByStaking(params *app.Parameters, account *protocol.TokenAccount) bool {
	// Staking must own the account
	auth, ok := account.GetAuthority(params.Account.Authority)
	if !ok || auth.Disabled {
		return false
	}

	// All other authorities must be disabled
	for _, auth := range account.Authorities {
		if params.Account.Authority.Equal(auth.Url) {
			continue
		}

		if !auth.Disabled {
			return false
		}
	}

	return true
}

// getBalanceChange returns the change in the account's balance due to the
// transaction.
func getBalanceChange(txn *protocol.Transaction) *big.Int {
	amount := new(big.Int)
	switch body := txn.Body.(type) {
	case *protocol.SendTokens:
		for _, to := range body.To {
			amount.Sub(amount, &to.Amount)
		}
	case *protocol.BurnTokens:
		amount.Neg(&body.Amount)
	case *protocol.AddCredits:
		amount.Neg(&body.Amount)
	case *protocol.SyntheticDepositTokens:
		amount = &body.Amount
	default:
		return nil
	}
	return amount
}

// PullStaking pulls all of the account data required by staking.
func PullStaking(ctx context.Context, c *light.Client, params *app.Parameters) error {
	log := slog.Default()

	// Pull the globals accounts
	log.Info("Load globals")
	err := c.PullAccount(ctx, protocol.DnUrl().JoinPath(protocol.Ledger))
	if err != nil {
		return errors.UnknownError.Wrap(err)
	}
	err = c.PullAccount(ctx, protocol.DnUrl().JoinPath(protocol.Globals))
	if err != nil {
		return errors.UnknownError.Wrap(err)
	}
	err = c.PullAccount(ctx, protocol.DnUrl().JoinPath(protocol.Network))
	if err != nil {
		return errors.UnknownError.Wrap(err)
	}
	err = c.PullAccount(ctx, protocol.DnUrl().JoinPath(protocol.Oracle))
	if err != nil {
		return errors.UnknownError.Wrap(err)
	}
	err = c.PullAccount(ctx, protocol.DnUrl().JoinPath(protocol.Routing))
	if err != nil {
		return errors.UnknownError.Wrap(err)
	}

	// Load the globals
	g := new(network.GlobalValues)
	batch := c.OpenDB(false)
	defer batch.Discard()
	err = g.Load(protocol.DnUrl(), func(accountUrl *url.URL, target interface{}) error {
		return batch.Account(accountUrl).Main().GetAs(target)
	})
	if err != nil {
		return errors.UnknownError.Wrap(err)
	}

	for _, p := range g.Network.Partitions {
		log.Info("Load ledger", "partition", p.ID)

		// Pull the partition's system ledger
		u := protocol.PartitionUrl(p.ID)
		err = c.PullAccount(ctx, u.JoinPath(protocol.Ledger))
		if err != nil {
			return errors.UnknownError.Wrap(err)
		}
		err = c.IndexAccountChains(ctx, u.JoinPath(protocol.Ledger))
		if err != nil {
			return errors.UnknownError.Wrap(err)
		}

		// Pull the partition's anchor ledger
		err = c.PullAccount(ctx, u.JoinPath(protocol.AnchorPool))
		if err != nil {
			return errors.UnknownError.Wrap(err)
		}
		err = c.IndexAccountChains(ctx, u.JoinPath(protocol.AnchorPool))
		if err != nil {
			return errors.UnknownError.Wrap(err)
		}

		// Pull the partition's anchors
		err = c.PullTransactionsForAccount(ctx, u.JoinPath(protocol.AnchorPool), "anchor-sequence")
		if err != nil {
			return errors.UnknownError.Wrap(err)
		}

		// Index the anchors
		err = c.IndexAnchors(ctx, u)
		if err != nil {
			return errors.UnknownError.Wrap(err)
		}
	}

	// Load ACME
	err = c.PullAccount(ctx, params.Account.TokenIssuance)
	if err != nil {
		return errors.UnknownError.Wrap(err)
	}
	err = c.PullTransactionsForAccount(ctx, params.Account.TokenIssuance, "main", "scratch")
	if err != nil {
		return errors.UnknownError.Wrap(err)
	}
	err = c.IndexAccountChains(ctx, params.Account.TokenIssuance)
	if err != nil {
		return errors.UnknownError.Wrap(err)
	}
	err = c.IndexAccountTransactions(ctx, params.Account.TokenIssuance)
	if err != nil {
		return errors.UnknownError.Wrap(err)
	}

	// Pull staking accounts
	log.Info("Load staking")
	err = c.PullAccount(ctx, params.Account.Validators)
	if err != nil {
		return errors.UnknownError.Wrap(err)
	}
	err = c.PullAccount(ctx, params.Account.ApprovedADIs)
	if err != nil {
		return errors.UnknownError.Wrap(err)
	}
	err = c.PullTransactionsForAccount(ctx, params.Account.ApprovedADIs, "main", "scratch")
	if err != nil {
		return errors.UnknownError.Wrap(err)
	}
	err = c.PullAccount(ctx, params.Account.RegisteredADIs)
	if err != nil {
		return errors.UnknownError.Wrap(err)
	}
	err = c.PullTransactionsForAccount(ctx, params.Account.RegisteredADIs, "main", "scratch")
	if err != nil {
		return errors.UnknownError.Wrap(err)
	}
	err = c.PullAccount(ctx, params.Account.Requests)
	if err != nil {
		return errors.UnknownError.Wrap(err)
	}
	err = c.PullTransactionsForAccount(ctx, params.Account.Requests, "main", "scratch")
	if err != nil {
		return errors.UnknownError.Wrap(err)
	}
	err = c.IndexAccountChains(ctx, params.Account.Requests)
	if err != nil {
		return errors.UnknownError.Wrap(err)
	}
	err = c.IndexAccountTransactions(ctx, params.Account.Requests)
	if err != nil {
		return errors.UnknownError.Wrap(err)
	}

	// Scan the registered list
	identities, err := LoadAllRegistered(batch, params)
	if err != nil {
		return errors.UnknownError.Wrap(err)
	}

	var urls []*url.URL
	for _, i := range identities {
		for _, a := range i.Accounts {
			// Pull and index the staking account
			err = c.PullAccount(ctx, a.Url)
			switch {
			case err == nil:
				// Ok
			case errors.Is(err, errors.NotFound):
				continue // Skip bad accounts
			default:
				return errors.UnknownError.Wrap(err)
			}
			urls = append(urls, a.Url)

			err = c.PullTransactionsForAccount(ctx, a.Url, "main", "scratch")
			if err != nil {
				return errors.UnknownError.Wrap(err)
			}
			err = c.IndexAccountChains(ctx, a.Url)
			if err != nil {
				return errors.UnknownError.Wrap(err)
			}

			// Pull the payout account
			if a.Payout != nil && !a.Url.Equal(a.Payout) {
				err = c.PullAccount(ctx, a.Payout)
				switch {
				case err == nil:
					// Ok
				case errors.Is(err, errors.NotFound):
					continue // Skip bad accounts
				default:
					return errors.UnknownError.Wrap(err)
				}
			}
		}
	}

	err = c.IndexAccountTransactions(ctx, urls...)
	return errors.UnknownError.Wrap(err)
}
