package fsm

import (
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/vdk/node"
)

var _ StateDelegate = (*didProcessStakingRequests)(nil)

// do confirmation that it is correct
type didProcessStakingRequests struct {
}

func (s *didProcessStakingRequests) execute(ctx *StateContext, d *node.DidCommitBlock) StateDelegate {
	// For each transaction we signed, wait until it has been executed
	for _, txn := range ctx.toSign.all() {
		ready, err := ctx.txnIsDone(txn.ID())
		if err != nil {
			return ctx.Fatal(errors.UnknownError).Wrap(err)
		}
		if !ready {
			return s
		}
	}

	return ctx.MakeCurrent(StateFinalizeEpoch)
}
