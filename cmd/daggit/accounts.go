package main

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/spf13/cobra"
	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/cmd/daggit/network"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/pkg/types"
)

var accountCmd = &cobra.Command{
	Use:   "account",
	Short: "Add, remove, or list approved stakers",
}

var accountRequestsCmd = &cobra.Command{
	Use:   "requests",
	Short: "List requests",
	Args:  cobra.NoArgs,
	Run:   listRequests,
}

var accountAddCmd = &cobra.Command{Use: "add", Short: "Add a staker"}
var accountAddCoreCmd = &cobra.Command{Use: "core", Short: "Add a core staker"}
var accountAddStakingCmd = &cobra.Command{Use: "staking", Short: "Add a staking staker"}

var accountAddCoreValidatorCmd = &cobra.Command{
	Use:   "validator [identity] [stake] [rewards]",
	Short: "Add a core validator staker",
	Args:  cobra.ExactArgs(3),
	Run:   add(types.AccountTypeCoreValidator),
}

var accountAddCoreFollowerCmd = &cobra.Command{
	Use:   "follower [identity] [stake] [rewards]",
	Short: "Add a core follower staker",
	Args:  cobra.ExactArgs(3),
	Run:   add(types.AccountTypeCoreFollower),
}

var accountAddStakingValidatorCmd = &cobra.Command{
	Use:   "validator [identity] [stake] [rewards]",
	Short: "Add a staking validator staker",
	Args:  cobra.ExactArgs(3),
	Run:   add(types.AccountTypeStakingValidator),
}

var accountAddPureCmd = &cobra.Command{
	Use:   "pure [identity] [stake] [rewards]",
	Short: "Add a pure staker",
	Args:  cobra.ExactArgs(3),
	Run:   add(types.AccountTypePure),
}

var accountAddDelegatedCmd = &cobra.Command{
	Use:   "delegated [identity] [stake] [rewards] [delegate]",
	Short: "Add a delegated staker",
	Args:  cobra.ExactArgs(4),
	Run:   add(types.AccountTypeDelegated),
}

var accountFlag = struct {
	Json               bool
	Lockup             uint64
	HardLock           bool
	AcceptingDelegates types.AcceptingDelegates
	Status             types.Status
	Columns            []string
	Scan               bool
}{
	AcceptingDelegates: types.AcceptingDelegatesYes,
	Status:             types.StatusRegistered,
}

func init() {
	cmd.AddCommand(accountCmd)
	accountCmd.AddCommand(accountAddCmd, accountRequestsCmd)
	accountAddCmd.AddCommand(accountAddCoreCmd, accountAddStakingCmd, accountAddPureCmd, accountAddDelegatedCmd)
	accountAddCoreCmd.AddCommand(accountAddCoreValidatorCmd, accountAddCoreFollowerCmd)
	accountAddStakingCmd.AddCommand(accountAddStakingValidatorCmd)

	accountCmd.PersistentFlags().Uint64Var(&accountFlag.Lockup, "lockup", 0, "Additional lockup beyond the initial 6 months, measured in quarters")
	accountCmd.PersistentFlags().BoolVar(&accountFlag.HardLock, "hard-lock", false, "Lock without the option of penalized withdraws")
	accountCmd.PersistentFlags().Var(NewEnum(&accountFlag.AcceptingDelegates, types.AcceptingDelegatesByName), "accepts-delegates", "Whether the full-staker accepts delegates")
	accountCmd.PersistentFlags().Var(NewEnum(&accountFlag.Status, types.StatusByName), "status", "Status of the staker")
}

func listRequests(*cobra.Command, []string) {
	req := new(client.DataEntrySetQuery)
	req.Url = protocol.AccountUrl("staking", "requests")
	req.Expand = true
	req.Count = 10
	for {
		res, err := theClient.QueryDataSet(context.Background(), req)
		checkf(err, "query failed")

		for _, item := range res.Items {
			b, err := json.Marshal(item)
			check(err)
			entry := new(client.ResponseDataEntry)
			check(json.Unmarshal(b, entry))

			tx, err := theClient.QueryTx(context.Background(), &client.TxnQuery{TxIdUrl: entry.TxId})
			check(err)

			for _, sig := range tx.Signatures {
				fmt.Printf("Signed by %v\n", sig.GetSigner())
			}

			for _, part := range entry.Entry.GetData() {
				fmt.Printf("  %s\n", part)
			}
			fmt.Println()
		}

		req.Start += uint64(len(res.Items))
		if req.Start >= res.Total {
			break
		}
	}
}

func add(typ types.AccountType) func(_ *cobra.Command, args []string) {
	return func(cmd *cobra.Command, args []string) {
		account := new(types.Identity)
		account.Type = typ
		account.Lockup = accountFlag.Lockup
		account.HardLock = accountFlag.HardLock
		account.AcceptingDelegates = accountFlag.AcceptingDelegates
		account.Status = accountFlag.Status

		if typ == types.AccountTypeDelegated {
			if cmd.Flag("accepts-delegates").Changed {
				fatalf("--accepts-delegates is not applicable to delegated accounts")
			} else {
				account.AcceptingDelegates = types.AcceptingDelegatesUnknown
			}
		}

		var err error
		account.Identity, err = url.Parse(args[0])
		checkf(err, "identity")

		account.Stake, err = url.Parse(args[1])
		checkf(err, "stake")

		account.Rewards, err = url.Parse(args[2])
		checkf(err, "rewards")

		if typ == types.AccountTypeDelegated {
			account.Delegate, err = url.Parse(args[3])
			checkf(err, "delegate")
		}

		b, err := json.Marshal(account)
		checkf(err, "marshal account")

		entry := new(protocol.AccumulateDataEntry)
		entry.Data = [][]byte{b}
		wd := new(protocol.WriteData)
		wd.Entry = entry

		b, err = json.Marshal(wd)
		checkf(err, "marshal transaction body")

		net, err := network.New(flag.Network)
		check(err)
		params, err := net.GetParameters()
		checkf(err, "get parameters")
		fmt.Printf("accumulate tx execute %v KEY --delegator staking.acme/governance/1 '%s'\n", params.Account.RegisteredADIs, b)
	}
}
