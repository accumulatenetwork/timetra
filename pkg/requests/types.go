package requests

//go:generate go run gitlab.com/accumulatenetwork/accumulate/tools/cmd/gen-enum --package requests enums.yml
//go:generate go run gitlab.com/accumulatenetwork/accumulate/tools/cmd/gen-types --package requests --long-union-discriminator types.yml
//go:generate go run gitlab.com/accumulatenetwork/accumulate/tools/cmd/gen-types --package requests --long-union-discriminator --language go-union --out unions_gen.go types.yml

// ActionType is the type of an [Action].
type ActionType uint64

type Action interface {
	CopyAsInterface() any
	ActionType() ActionType
}
