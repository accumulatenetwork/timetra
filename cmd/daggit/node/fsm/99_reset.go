package fsm

import (
	"gitlab.com/accumulatenetwork/accumulate/vdk/node"
)

var _ StateDelegate = (*reset)(nil)

type reset struct {
}

func (s *reset) execute(ctx *StateContext, d *node.DidCommitBlock) StateDelegate {
	//reset all the information in context and wait for next epoch
	ctx.reset()
	return ctx.MakeCurrent(StateCanBegin)
}
