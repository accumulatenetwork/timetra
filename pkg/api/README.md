# Staking API

## Add a new method

1. Add a method to `StakingService` in [api.go](api.go):
   ```go
   <method>(context.Context, <parameters>) (<result>, error)
   ```
2. Add type constants `MessageType<method>Request` and
   `MessageType<method>Response` to the respective `const` blocks in
   [types.go](types.go).
3. Add a `<method>Request` type with the parameter types and a
   `<method>Response` type with a `<result>` Value field and any other types to
   the respective sections in [types.yml](types.yml). The request and response
   types must include `union: { type: message }`.
4. Run type generation for the api package.
5. Register the request and response types in the `init` function in
   [types.go](types.go).
6. Add `case *<method>Request:` to the method dispatch switch statement in
   `NewMessageHandler` in [api.go](api.go).