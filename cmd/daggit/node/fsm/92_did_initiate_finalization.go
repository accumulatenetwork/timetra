package fsm

import (
	"encoding/json"

	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/messaging"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/accumulate/vdk/node"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/pkg/types"
)

var _ StateDelegate = (*didInitiateFinalization)(nil)

type didInitiateFinalization struct {
}

func (s *didInitiateFinalization) execute(ctx *StateContext, d *node.DidCommitBlock) StateDelegate {
	batch, ids, currentOrErrorState := ctx.Executor.didInitiate(ctx.Executor.Params.Account.Epoch)
	if currentOrErrorState != nil {
		return currentOrErrorState
	}
	defer batch.Discard()

	if len(ids) != 1 {
		//if we have more than 1 pending transaction, then for now this is an error, but maybe need to handle more gracefully?
		return ctx.Fatal(errors.InternalError).WithFormat("expecting only one pending transaction for the finalization")
	}

	// need to look for the latest entry for this epoch
	// pull the pending transaction and make sure it matches the current epoch.
	var saveState types.SaveState
	for _, id := range ids {
		var msg *messaging.TransactionMessage
		err := batch.Message(id.Hash()).Main().GetAs(&msg)
		if err != nil {
			return ctx.Fatal(errors.UnknownError).WithFormat("load pending registration transaction %v: %w", id, err)
		}
		// If the body is not WriteData, assume it's garbage
		body, ok := msg.Transaction.Body.(*protocol.WriteData)
		if !ok {
			continue
		}

		// Parse the metadata - assume it's garbage if it fails
		var md types.StateHeader
		err = md.UnmarshalJSON(msg.Transaction.Header.Metadata)
		if err != nil {
			continue
		}

		// If the root anchor doesn't match, it must belong to a different block
		if md.RootAnchor != ctx.Executor.CurrentEpochHash {
			continue
		}

		if len(body.Entry.GetData()) == 0 {
			// should not get here, but if we do...
			//no data so this is an invalid entry, so try again if there is more, if not we error
			ctx.Executor.Logger.Info("empty entry sets received for txid %x", id.Hash())
			continue
		}

		err = json.Unmarshal(body.Entry.GetData()[0], &saveState)
		if !saveState.Leader.Equal(ctx.leader) {
			//we have a bogus entry here, so log it and see if there is another tx
			ctx.Executor.Logger.Info("leader of block is not the expected for txid %x", id.Hash())
			continue
		}

		commit := types.CommitBlock{Major: ctx.Executor.CurrentEpochBlock.Major,
			Index: ctx.Executor.CurrentEpochBlock.Index,
			Time:  ctx.Executor.CurrentEpochBlock.Time}
		if !saveState.CommitBlock.Equal(&commit) {
			//commit block state does not agree, potential bogus entry, so log it an see if there is another tx
			ctx.Executor.Logger.Info("commit block state does not agree with save state for txid %x", id.Hash())
			continue
		}

		//if we get here, the transaction is accurate
		ctx.toFinalize = msg.Transaction
	}

	if ctx.toFinalize == nil {
		//if we get here there is no valid transaction, so this is bad...
		ctx.Fatal(errors.InternalError).WithFormat("no finalization transaction available for epoch")
	}

	return ctx.MakeCurrent(StateProcessFinalization)
}
