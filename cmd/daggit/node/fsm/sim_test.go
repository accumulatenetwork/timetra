package fsm

import (
	"context"
	"encoding/json"
	"fmt"
	"math/big"
	"os"
	"runtime"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/accumulatenetwork/accumulate/exp/light"
	"gitlab.com/accumulatenetwork/accumulate/pkg/build"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/messaging"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/network"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	. "gitlab.com/accumulatenetwork/accumulate/protocol"
	. "gitlab.com/accumulatenetwork/accumulate/test/harness"
	. "gitlab.com/accumulatenetwork/accumulate/test/helpers"
	"gitlab.com/accumulatenetwork/accumulate/test/simulator"
	acctesting "gitlab.com/accumulatenetwork/accumulate/test/testing"
	"gitlab.com/accumulatenetwork/accumulate/vdk/node"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/logging"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/cmd/daggit/app"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/internal/testutils"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/pkg/types"
	"golang.org/x/exp/slog"
)

func init() {
	acctesting.EnableDebugFeatures()
}

func TestSimulate(t *testing.T) {

	t.Skip("skip update for daggitron")
	slog.SetDefault(slog.New(slog.HandlerOptions{ReplaceAttr: logging.Jsonify}.NewTextHandler(os.Stderr)))

	var timestamp uint64
	alice := AccountUrl("alice")
	aliceKey := acctesting.GenerateKey(alice)
	aliceLite, err := LiteTokenAddress(aliceKey[32:], ACME, SignatureTypeED25519)
	require.NoError(t, err)
	bob := AccountUrl("bob")
	bobKey := acctesting.GenerateKey(bob)

	params := new(app.Parameters)
	params.Init()

	g := new(network.GlobalValues)
	g.ExecutorVersion = ExecutorVersionV1DoubleHashEntries
	g.Globals = new(NetworkGlobals)
	g.Globals.MajorBlockSchedule = "* * * * *"
	g.Routing = new(RoutingTable)
	g.Routing.Overrides = []RouteOverride{{
		Account:   params.ADI.Service,
		Partition: Directory,
	}}

	// Initialize the simulator
	sim := NewSim(t,
		simulator.MemoryDatabase,
		simulator.SimpleNetwork(t.Name(), 1, 1),
		simulator.GenesisWith(GenesisTime, g),
	)

	// Set up staking
	_, wallets := testutils.InitDaggits(t, sim, params, 2)

	// Set up alice
	const initBalance = 1e6
	MakeIdentity(t, sim.DatabaseFor(alice), alice, aliceKey[32:])
	CreditCredits(t, sim.DatabaseFor(alice), alice.JoinPath("book", "1"), 1e9)
	MakeAccount(t, sim.DatabaseFor(alice), &TokenAccount{Url: alice.JoinPath("stake1"), TokenUrl: AcmeUrl(), Balance: *big.NewInt(initBalance * AcmePrecision)})
	MakeAccount(t, sim.DatabaseFor(alice), &TokenAccount{Url: alice.JoinPath("stake2"), TokenUrl: AcmeUrl(), Balance: *big.NewInt(1e6 * AcmePrecision)})

	// Set up an account not owned by alice
	MakeIdentity(t, sim.DatabaseFor(bob), bob, bobKey[32:])
	CreditCredits(t, sim.DatabaseFor(bob), bob.JoinPath("book", "1"), 1e9)
	MakeAccount(t, sim.DatabaseFor(alice), &TokenAccount{Url: alice.JoinPath("stake3"), TokenUrl: AcmeUrl(), AccountAuth: AccountAuth{Authorities: []AuthorityEntry{{Url: AccountUrl("bob", "book")}}}})

	// Register stake1
	UpdateAccount(t, sim.DatabaseFor(alice), alice.JoinPath("stake1"), func(t *TokenAccount) {
		t.Authorities[0].Disabled = true
		t.AddAuthority(params.Account.Authority)
	})
	b, err := json.Marshal(&types.Identity{
		Identity: alice,
		Accounts: []*types.Account{{
			Type:   types.AccountTypeCoreValidator,
			Url:    alice.JoinPath("stake1"),
			Payout: alice.JoinPath("stake1"),
		}},
	})
	require.NoError(t, err)

	st := sim.BuildAndSubmitTxnSuccessfully(
		build.Transaction().For(params.Account.RegisteredADIs).
			WriteData(&DoubleHashDataEntry{Data: [][]byte{b}}).
			SignWith(params.Account.Validators).Version(1).Timestamp(&timestamp).
			Signer(testutils.MustGetKey(t, wallets[0])).
			SignWith(params.Account.Validators).Version(1).Timestamp(&timestamp).
			Signer(testutils.MustGetKey(t, wallets[1])))

	sim.StepUntil(
		Txn(st.TxID).Completes())

	// Ensure a major block
	sim.StepUntilN(1000,
		testutils.MajorBlock(1))

	// Submit a request to register Alice
	req := sim.BuildAndSubmitTxnSuccessfully(
		build.Transaction().For(alice, "stake2").
			UpdateAccountAuth().
			Disable(alice, "book").
			Add(params.Account.Authority).
			SignWith(alice, "book", "1").Version(1).Timestamp(&timestamp).PrivateKey(aliceKey))

	st = sim.BuildAndSubmitTxnSuccessfully(
		build.Transaction().For(params.Account.Requests).
			WriteData(&DoubleHashDataEntry{Data: [][]byte{
				[]byte("addAccount"),
				[]byte("account=alice.acme/stake2"),
				[]byte("type=delegated"),
				[]byte("delegate=foo.acme/bar"),
				[]byte("transaction=" + req.TxID.String()),
			}}).
			SignWith(alice, "book", "1").Version(1).Timestamp(&timestamp).PrivateKey(aliceKey))

	sim.StepUntil(
		Txn(st.TxID).Completes())

	// Submit a request signed by the wrong authority
	badReq := sim.BuildAndSubmitTxnSuccessfully(
		build.Transaction().For(alice, "stake3").
			UpdateAccountAuth().
			Disable(bob, "book").
			Add(params.Account.Authority).
			SignWith(bob, "book", "1").Version(1).Timestamp(&timestamp).PrivateKey(bobKey))

	st = sim.BuildAndSubmitTxnSuccessfully(
		build.Transaction().For(params.Account.Requests).
			WriteData(&DoubleHashDataEntry{Data: [][]byte{
				[]byte("addAccount"),
				[]byte("account=alice.acme/stake3"),
				[]byte("type=delegated"),
				[]byte("delegate=foo.acme/bar"),
				[]byte("transaction=" + req.TxID.String()),
			}}).
			SignWith(alice, "book", "1").Version(1).Timestamp(&timestamp).PrivateKey(aliceKey))

	sim.StepUntil(
		Txn(st.TxID).Completes())

	ledger := GetAccount[*SystemLedger](t, sim.Database(Directory), DnUrl().JoinPath(Ledger))
	anchors := GetAccount[*AnchorLedger](t, sim.Database(Directory), DnUrl().JoinPath(AnchorPool))
	prevEpoch := node.DidCommitBlock{
		Index: ledger.Index,
		Major: anchors.MajorBlockIndex,
		Time:  anchors.MajorBlockTime,
	}

	lc, err := light.NewClient(
		light.Logger(acctesting.NewTestLogger(t)),
		light.MemoryStore(),
		light.ClientV2(sim.S.ClientV2(Directory)),
		light.Querier(sim.Query()),
	)
	require.NoError(t, err)

	// Initialize the FSM
	fsm1 := NewStateExecutor(Options{Wallet: wallets[0], Epoch: 2 * time.Minute, Logger: slog.Default().With("node", 1)})
	fsm1.PrevEpochBlock = prevEpoch
	fsm1.Light = lc
	fsm1.Params = params
	fsm1.APIv2 = sim.S.ClientV2(Directory)
	fsm1.APIv3 = sim.S.Services()

	fsm2 := NewStateExecutor(Options{Wallet: wallets[1], Epoch: 2 * time.Minute, Logger: slog.Default().With("node", 2)})
	fsm2.PrevEpochBlock = prevEpoch
	fsm2.Light = lc
	fsm2.Params = params
	fsm2.APIv2 = sim.S.ClientV2(Directory)
	fsm2.APIv3 = sim.S.Services()

	// Run the FSM with a short queue depth to avoid too much slippage,
	// otherwise StepUntil doesn't work well
	ctx, cancel := context.WithCancel(context.Background())
	wg := new(sync.WaitGroup)
	defer func() { cancel(); wg.Wait() }()
	ch := fsm1.Start(ctx, sim.S.EventBus(Directory), 1, wg)
	go sendEnvelopes(t, sim, ch)
	ch = fsm2.Start(ctx, sim.S.EventBus(Directory), 1, wg)
	go sendEnvelopes(t, sim, ch)

	// Step until the next epoch
	sim.StepUntilN(1000,
		testutils.MajorBlock(3))

	// Step until the FSM starts then completes
	sim.StepUntilN(1000, &EpochComplete{fsm1, 3})
	sim.StepUntilN(1000, &EpochComplete{fsm2, 3})

	// Verify the request completes
	sim.StepUntil(
		Txn(req.TxID).Completes())

	// Verify the bad request does not complete
	sim.StepUntil(
		Txn(badReq.TxID).IsPending())

	//wait for prev payout transaction fields to be set
	sim.StepUntil(True(func(h *Harness) bool {
		return fsm1.PrevPayout != nil && fsm2.PrevPayout != nil
	}))

	// Verify the payout completes
	sim.StepUntil(
		Txn(fsm1.PrevPayout).Completes())

	// Verify staking rewards were deposited
	s1 := GetAccount[*TokenAccount](t, sim.DatabaseFor(alice), alice.JoinPath("stake1"))
	require.Greater(t, s1.Balance.Uint64(), uint64(initBalance*AcmePrecision))

	// submit the withdrawal request
	st = sim.BuildAndSubmitTxnSuccessfully(
		build.Transaction().For(params.Account.Requests).
			WriteData(&DoubleHashDataEntry{Data: [][]byte{
				[]byte("withdrawTokens"),
				[]byte("account=alice.acme/stake1"),
				[]byte(fmt.Sprintf("recipient=%s", aliceLite)),
				[]byte(fmt.Sprintf("amount=%d", uint64(5000*AcmePrecision))),
			}}).
			SignWith(alice, "book", "1").Version(1).Timestamp(&timestamp).PrivateKey(aliceKey))

	sim.StepUntil(
		Txn(st.TxID).Completes())

	//now advance to the next epoch
	sim.StepUntilN(1000,
		testutils.MajorBlock(5))

	// Step until the FSM starts then completes
	sim.StepUntilN(1000, &EpochComplete{fsm1, 6})
	sim.StepUntilN(1000, &EpochComplete{fsm2, 6})

	// Step until we see the pending transaction
	var txn *protocol.Transaction
	for i := 0; i < 1000 && txn == nil; i++ {
		sim.Step()
		runtime.Gosched()

		txn, err = findPendingTransaction(
			lc, sim.Query(), params.Account.Validators,
			func(sig Signature) (bool, error) { return true, nil },
			func(txn *Transaction) (bool, error) {
				// Check the body
				body, ok := txn.Body.(*SendTokens)
				return ok &&
					// Check the principal
					txn.Header.Principal.Equal(alice.JoinPath("stake1")) &&
					// Check the recipient
					len(body.To) == 1 && body.To[0].Url.Equal(aliceLite), nil
			},
		)
		if !errors.Is(err, errors.NotFound) {
			require.NoError(t, err)
		}
	}
	require.NotNil(t, txn, "Condition not satisfied after 1000 blocks")

	// Wait for it to complete
	sim.StepUntil(
		Txn(txn.ID()).Completes())

	// Verify the transfer
	s2 := GetAccount[*LiteTokenAccount](t, sim.DatabaseFor(aliceLite), aliceLite)
	require.Equal(t, s2.Balance.Uint64(), uint64(5e3*AcmePrecision))
}

type EpochComplete struct {
	FSM   *StateExecutor
	Block uint64
}

func (e *EpochComplete) Satisfied(h *Harness) bool {
	h.TB.Helper()
	require.NoError(h.TB, e.FSM.context.lastError)
	return e.FSM.PrevEpochBlock.Major == e.Block
}

func (e *EpochComplete) Format(prefix, suffix string) string {
	return fmt.Sprintf("%sFSM completes epoch %v%s", prefix, e.FSM.PrevEpochBlock.Major, suffix)
}

func sendEnvelopes(t *testing.T, sim *Sim, ch <-chan *messaging.Envelope) {
	for env := range ch {
		st := sim.Submit(env)
		for _, st := range st {
			assert.NoError(t, st.AsError())
		}
	}
}
