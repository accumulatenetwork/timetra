package internal

import (
	"context"
	"encoding/json"
	"fmt"
	"math/big"
	stdurl "net/url"
	"strconv"
	"time"

	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/pkg/api"
)

type AccountStatus struct {
	api.AccountStatus

	client *client.Client
}

type TxWhen = api.TxWhen

func (a *AccountStatus) SetClient(c *client.Client) {
	a.client = c
}

func (a *AccountStatus) Compare(b *AccountStatus) int {
	return a.Account.Url.Compare(b.Account.Url)
}

func (a *AccountStatus) GetStakeBalance() {
	account := GetTokenAccount(a.client, a.Account.Url)
	if account != nil {
		a.Balance = &account.Balance
	}
}

func (a *AccountStatus) FindDepositsFor(majorBlock, endMajorBlock uint64) error {
	// Get the balance today
	a.GetStakeBalance()
	if a.Balance == nil {
		a.Balance = new(big.Int)
	}
	a.Deposits = new(big.Int)

	// Scan every transaction that has ever happened
	return a.scanAccountTransaction(a.Account.Url, func(tx *client.TransactionQueryResponse) (bool, error) {
		switch tx.Transaction.Body.Type() {
		// Initial staking request
		case protocol.TransactionTypeUpdateAccountAuth:
			t, err := getFirstReceivedTime(a.client, tx.Txid)
			if err != nil {
				return false, errors.UnknownError.Wrap(err)
			}
			// Determine when the first signature was received
			if a.Staked == nil {
				a.Staked = &TxWhen{TxID: tx.Txid, Time: t}
			}
			return true, nil

		// Process transactions that affect the token balance
		case protocol.TransactionTypeSendTokens,
			protocol.TransactionTypeBurnTokens,
			protocol.TransactionTypeAddCredits,
			protocol.TransactionTypeSyntheticDepositTokens:
			break

		// Ignore everything else
		default:
			return true, nil
		}

		// Get the timestamp
		r, err := getMainChainReceipt(a.client, tx.Txid)
		if err != nil {
			return false, errors.UnknownError.WithFormat("get receipt for %v (%v): %w", tx.Txid, tx.Transaction.Body.Type(), err)
		}

		// If the transaction occurred prior to the start block, ignore it
		if r.MajorBlock != 0 && r.MajorBlock <= majorBlock {
			return a.Staked == nil, nil
		}

		// Get the amount
		amount := new(big.Int)
		switch body := tx.Transaction.Body.(type) {
		case *protocol.SendTokens:
			for _, to := range body.To {
				amount.Sub(amount, &to.Amount)
			}
		case *protocol.BurnTokens:
			amount.Neg(&body.Amount)
		case *protocol.AddCredits:
			amount.Neg(&body.Amount)
		case *protocol.SyntheticDepositTokens:
			amount = &body.Amount
		}

		// If it's after the end block, subtract the change from the balance
		if r.MajorBlock == 0 || r.MajorBlock > endMajorBlock {
			a.Balance.Sub(a.Balance, amount)
			return true, nil
		}

		// If it's a deposit, subtract it from the balance and add it to the deposits
		if amount.Sign() > 0 {
			a.Deposits.Add(a.Deposits, amount)
			a.Balance.Sub(a.Balance, amount)
			return true, nil
		}

		// If it's a withdraw, do nothing. Thus, withdraws are immediately
		// deducted from the staked balance.
		return true, nil
	})
}

func (a *AccountStatus) FindLastChange() error {
	return a.scanAccountTransaction(a.Account.Url, func(tx *client.TransactionQueryResponse) (bool, error) {
		switch tx.Transaction.Body.Type() {
		case protocol.TransactionTypeUpdateAccountAuth:
			// Determine when the first signature was received
			if a.Staked == nil {
				t, err := getFirstReceivedTime(a.client, tx.Txid)
				if err != nil {
					return false, errors.UnknownError.Wrap(err)
				}
				a.Staked = &TxWhen{TxID: tx.Txid, Time: t}
			}

		case protocol.TransactionTypeSendTokens,
			protocol.TransactionTypeBurnTokens,
			protocol.TransactionTypeAddCredits,
			protocol.TransactionTypeSyntheticDepositTokens:
			// Determine when the transaction resolved
			if a.LastChange == nil {
				r, err := getMainChainReceipt(a.client, tx.Txid)
				if err != nil {
					return false, errors.UnknownError.WithFormat("get receipt for %v: %w", tx.Txid, err)
				}
				a.LastChange = &api.TxWhen{
					Time: *r.LocalBlockTime,
					TxID: tx.Txid,
				}
			}
		}

		return a.Staked == nil || a.LastChange == nil, nil
	})
}

func (a *AccountStatus) scanAccountTransaction(u *url.URL, fn func(*client.TransactionQueryResponse) (bool, error)) error {
	// Get the total number of entries
	req := new(client.TxHistoryQuery)
	req.Url = u
	req.Count = 1
	res, err := a.client.QueryTxHistory(context.Background(), req)
	if err != nil {
		return errors.UnknownError.WithFormat("query %v tx history: %w", u, err)
	}

	// Scan entries in reverse order
	x := res.Total
	const N = 50
	for x > 0 {
		if x < N {
			req.Start = 0
			req.Count = x
			x = 0
		} else {
			req.Start = x - N
			req.Count = N
			x -= N
		}

		res, err = a.client.QueryTxHistory(context.Background(), req)
		if err != nil {
			return errors.UnknownError.WithFormat("query %v tx history: %w", u, err)
		}

		for i := len(res.Items) - 1; i >= 0; i-- {
			tx, err := remarshal[client.TransactionQueryResponse](res.Items[i])
			if err != nil {
				return errors.UnknownError.Wrap(err)
			}
			ok, err := fn(tx)
			if err != nil {
				return errors.UnknownError.Wrap(err)
			}
			if !ok {
				return nil
			}
		}
	}
	return nil
}

func getMainChainReceipt(c *client.Client, txid *url.TxID) (*client.TxReceipt, error) {
	txq := new(client.TxnQuery)
	txq.TxIdUrl = txid
	txq.Prove = true
	txr, err := c.QueryTx(context.Background(), txq)
	if err != nil {
		return nil, errors.UnknownError.WithFormat("query %v: %w", txid, err)
	}

	var r *client.TxReceipt
	for _, rr := range txr.Receipts {
		if rr.Chain == "main" && rr.Account.Equal(txid.Account()) {
			r = rr
		}
	}
	if r == nil {
		return nil, errors.NotFound.With("cannot find receipt")
	}
	return r, nil
}

func getFirstReceivedTime(c *client.Client, txid *url.TxID) (time.Time, error) {
	req := json.RawMessage(fmt.Sprintf(`{"txIdUrl": "%v", "includeRemote": true}`, txid))

	// Ask every partition for the transaction and get time of the earliest
	// Received block
	var t time.Time
	err := forEachPartition(c, func(p string, c *client.Client) error {
		res := new(client.TransactionQueryResponse)
		err := c.RequestAPIv2(context.Background(), "query-tx-local", req, res)
		if err != nil {
			return nil
		}
		if res.Status.Received == 0 {
			return nil
		}

		blq := new(client.GeneralQuery)
		blq.Url = protocol.PartitionUrl(p).JoinPath(protocol.Ledger, strconv.FormatUint(res.Status.Received, 10))
		blk := new(protocol.BlockLedger)
		_, err = c.QueryAccountAs(context.Background(), blq, blk)
		if err != nil {
			return errors.UnknownError.WithFormat("get %s block %d: %w", p, res.Status.Received, err)
		}
		if t == (time.Time{}) || t.After(blk.Time) {
			t = blk.Time
		}
		return nil
	})
	if err != nil {
		return time.Time{}, errors.UnknownError.Wrap(err)
	}

	if t == (time.Time{}) {
		return time.Time{}, errors.NotFound.WithFormat("no partition recorded a received block for %v", txid)
	}
	return t, nil
}

func forEachPartition(c *client.Client, fn func(string, *client.Client) error) error {
	desc, err := c.Describe(context.Background())
	if err != nil {
		return errors.UnknownError.WithFormat("query routing table: %w", err)
	}

	for _, p := range desc.Network.Partitions {
		if len(p.Nodes) == 0 {
			return errors.InternalError.WithFormat("partition %s has no nodes", p.Id)
		}

		u, err := stdurl.Parse(p.Nodes[0].Address)
		if err != nil {
			return errors.InternalError.WithFormat("partition %s node %d address is invalid: %w", p.Id, 0, err)
		}
		port, err := strconv.ParseUint(u.Port(), 10, 32)
		if err != nil {
			return errors.InternalError.WithFormat("partition %s node %d address is invalid: %w", p.Id, 0, err)
		}
		server := fmt.Sprintf("%s://%s:%d", u.Scheme, u.Hostname(), port+4)
		c, err := client.New(server)
		if err != nil {
			return errors.InternalError.WithFormat("partition %s node %d address is invalid: %w", p.Id, 0, err)
		}

		err = fn(p.Id, c)
		if err != nil {
			return errors.UnknownError.Wrap(err)
		}
	}
	return nil
}
