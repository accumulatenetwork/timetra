package fsm

import (
	"context"

	"github.com/AccumulateNetwork/jsonrpc2/v15"
	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/vdk/node"
)

var _ StateDelegate = (*didProcessFinalization)(nil)

type didProcessFinalization struct {
}

func (s *didProcessFinalization) execute(ctx *StateContext, d *node.DidCommitBlock) StateDelegate {
	//wait for the finalization to appear on-chain...
	res, err := ctx.Executor.APIv2.QueryTx(context.TODO(), &client.TxnQuery{TxIdUrl: ctx.toFinalize.ID()})
	if err != nil {
		// Wait if the transaction is not found
		var jerr jsonrpc2.Error
		if !errors.As(err, &jerr) ||
			jerr.Code != client.ErrCodeNotFound {
			return ctx.TryAgain(errors.UnknownError).WithFormat("get status of %v: %w", ctx.toFinalize.ID(), err)
		}
		return s
	}

	// Wait if the transaction is pending
	if res.Status.Pending() {
		ctx.Executor.Logger.Info("status pending in didProcessFinalization")
		return s
	}

	//update the previous epoch to the current one
	ctx.Executor.PrevEpochBlock = ctx.Executor.CurrentEpochBlock
	ctx.Executor.PrevPayout = ctx.payoutTx.ID()
	return ctx.MakeCurrent(StateReset)
}
