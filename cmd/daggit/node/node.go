package node

import (
	"flag"
	"time"

	"gitlab.com/accumulatenetwork/accumulate/exp/light"
	"gitlab.com/accumulatenetwork/accumulate/pkg/api/v3/jsonrpc"
	"gitlab.com/accumulatenetwork/accumulate/pkg/api/v3/p2p"
	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	testhttp "gitlab.com/accumulatenetwork/accumulate/test/util/http"
	"gitlab.com/accumulatenetwork/accumulate/vdk/node"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/cmd/daggit/app"
	fsm "gitlab.com/accumulatenetwork/ecosystem/daggitron/cmd/daggit/node/fsm"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/internal"
	twitter "gitlab.com/accumulatenetwork/ecosystem/daggitron/internal/twitter"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/pkg/api"
)

// Seqence:
// User authenticates app with twitter
// User pulls daggitron.acme/daggits data account with twitter profile id's.

func Init(p *node.Daemon, walletAddress string, keyName string) (*fsm.StateExecutor, error) {
	token := flag.String("token", "", "twitter API token")
	//	text := flag.String("text", "", "twitter text")
	flag.Parse()

	w, err := internal.NewWallet(walletAddress, keyName)
	if err != nil {
		return nil, errors.UnknownError.Wrap(err)
	}

	opts := fsm.Options{TransitionRate: 100, Epoch: 24 * 7 * time.Hour, Wallet: w}
	ex := fsm.NewStateExecutor(opts)
	ex.Params = new(app.Parameters)
	ex.Params.Init()

	// Get the key-value store from the daemon
	store, err := p.DB_TESTONLY().Store()
	if err != nil {
		return nil, errors.UnknownError.Wrap(err)
	}

	//// Get a direct API client
	ex.APIv2, err = client.New("http://direct/v2")
	if err != nil {
		return nil, errors.UnknownError.Wrap(err)
	}

	ex.APIv2.Client.Client = *testhttp.DirectHttpClient(p.API())
	ex.APIv3 = &jsonrpc.Client{Server: "http://direct/v3", Client: *testhttp.DirectHttpClient(p.API())}

	//Create the light client
	ex.Light, err = light.NewClient(
		light.ClientV2(ex.APIv2),
		light.Store(store, "Light"),
	)
	if err != nil {
		return nil, errors.UnknownError.Wrap(err)
	}

	twitter.NewTwitterClient(ex.Light, *token)

	err = RegisterServices(ex, p.P2P_TESTONLY())
	if err != nil {
		return nil, errors.UnknownError.Wrap(err)
	}

	err = ex.Load()
	if err != nil {
		//there will be no entry if this is the first attempt
		//if we aren't able to load the last state, we will just default from start.
		ex.Logger.Error("error loading last state", "error", err)
	}

	return ex, nil
}

func RegisterServices(ex *fsm.StateExecutor, node *p2p.Node) error {
	svc := &internal.StakingService{Client: ex.Light, Params: ex.Params}
	ok := node.RegisterService(api.ServiceTypeDaggitron.Address(), api.NewMessageHandler(svc, nil))
	if !ok {
		return errors.InternalError.With("unable to register staking service")
	}
	return nil
}
