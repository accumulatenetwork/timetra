package internal

import (
	"context"
	"fmt"
	"net/http"

	"github.com/g8rswimmer/go-twitter/v2"
	"gitlab.com/accumulatenetwork/accumulate/exp/light"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
)

type authorize struct {
	Token string
}

func (a authorize) Add(req *http.Request) {
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", a.Token))
}

type Twitter struct {
	Client    *twitter.Client
	Light     *light.Client
	ProfileId string
}

func NewTwitterClient(client *light.Client, token string) *Twitter {
	twit := new(Twitter)
	twit.Client = &twitter.Client{
		Authorizer: authorize{
			Token: token,
		},
		Client: http.DefaultClient,
		Host:   "https://api.twitter.com",
	}

	twit.Light = client
	return twit
}

func (t *Twitter) Authenticate(operators *url.URL) error {
	t.Light.PullAccount(context.TODO(), operators)

	opts := twitter.UserLookupOpts{}
	opts.UserFields = append(opts.UserFields, twitter.UserFieldID)
	resp, err := t.Client.AuthUserLookup(context.TODO(), opts)
	if err != nil {
		return err
	}

	t.ProfileId = resp.Raw.Users[0].ID
	return nil
}

func (t *Twitter) NewTransaction(entry *protocol.DoubleHashDataEntry) (eventId int, conversationId int, err error) {
	if len(t.ProfileId) == 0 {
		return 0, 0, fmt.Errorf("not authenticated")
	}

	//accumulate query authorities
	authorities := []string{}
	dm := twitter.CreateGroupDMConversation(authorities,
		fmt.Sprintf("initiate request %x", entry.Hash()),
		nil)

	resp, err := t.Client.InitiateGroupDMConversation(context.TODO(), dm)
	if err != nil {
		return 0, 0, err
	}

	return resp.EventId, resp.ConversationId, nil
}

func (t *Twitter) QueryEvents() ([]int, error) {
	opts := twitter.UserLookupOpts{}
	opts.UserFields = append(opts.UserFields, twitter.UserFieldID)
	raw, err := t.Client.AuthUserLookup(context.TODO(), opts)
	if err != nil {
		return nil, err
	}
	_ = raw

	//pretend we got a list of events that happened since last query
	eventIds := []int{}
	// build a list of events
	eventIds = append(eventIds, 0)
	return eventIds, nil
}

//
//func NewTwitterReactor(client *light.Client, token string) *Twitter {
//	t := NewTwitterClient(client, token)
//	//the reactor contains a client for the operator, and a go routine
//	// Create a pipe that is closed once the context is done
//	// the input will be a group dm message initiation. that will relay the input to the
//	// processor.
//	in, out := fsm.CancelableChannel[twitter.GroupDMResponse](context.TODO(), 1)
//
//}
