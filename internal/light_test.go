package internal

import (
	"bytes"
	"context"
	"fmt"
	"math/big"
	"os"
	"path/filepath"
	"sort"
	"testing"
	"text/tabwriter"

	"github.com/stretchr/testify/require"
	"gitlab.com/accumulatenetwork/accumulate/exp/light"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	acctesting "gitlab.com/accumulatenetwork/accumulate/test/testing"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/cmd/daggit/app"
)

func init() {
	acctesting.EnableDebugFeatures()
}

func TestStaking(t *testing.T) {
	acctesting.SkipCI(t, "Manual")

	params := new(app.Parameters)
	params.Init()
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	_ = ctx

	home, err := os.UserHomeDir()
	require.NoError(t, err)

	c, err := light.NewClient(
		light.Logger(NewLogger()),
		light.Server("mainnet"),
		light.BadgerStore(filepath.Join(home, ".accumulate", "staking.db")),
	)
	require.NoError(t, err)
	defer c.Close()

	// err = PullStaking(ctx, c, params)
	// require.NoError(t, err)

	// Calculate the budget
	batch := c.OpenDB(false)
	defer batch.Discard()
	budget, err := CalculateBudget(batch, params, [2]uint64{273, 287})
	require.NoError(t, err)

	accounts, outputs, err := Staking(c, params, [2]uint64{273, 287}, budget)
	require.NoError(t, err)

	{
		fmt.Println(`{ "type": "issueTokens", "to": [`)
		tw := tabwriter.NewWriter(os.Stderr, 0, 1, 1, ' ', 0)
		for i, o := range outputs {
			var comma string
			if i < len(outputs)-1 {
				comma = ","
			}
			fmt.Fprintf(tw, `  { "url": "%s",`+"\t"+`"amount": "%s"}%s`+"\n", o.Url, &o.Amount, comma)
		}
		tw.Flush()
		fmt.Println(`]}`)
	}
	fmt.Println()

	outputMap := map[[32]byte]*protocol.TokenRecipient{}
	for _, out := range outputs {
		outputMap[out.Url.AccountID32()] = out
	}

	tw := tabwriter.NewWriter(os.Stderr, 1, 3, 1, ' ', 0)
	fmt.Fprintf(tw, "Identity\tStaking Type\tStaking Account\tReward Account\tDelegate\tEffective Type\tWeighted Balance\tReward\n")
	for _, a := range accounts {
		var del string
		if a.Account.Delegate != nil {
			del = a.Account.Delegate.String()
		}
		payout := new(big.Rat)
		if x, ok := outputMap[a.Account.Payout.AccountID32()]; ok {
			delete(outputMap, a.Account.Payout.AccountID32())
			payout.SetInt(&x.Amount)
			payout.Mul(payout, big.NewRat(1, protocol.AcmePrecision))
		}
		fmt.Fprintf(tw, "%v\t%v\t%v\t%v\t%v\t%v\t%v\t%v\n", a.Identity.Identity, a.Account.Type, a.Account.Url, a.Account.Payout, del, a.EffectiveType, a.WeightedBalance.FloatString(8), payout.FloatString(8))
	}

	// Print any unclaimed outputs (if a full staker's delegated rewards address
	// does not correspond to any account)
	ids := make([][32]byte, 0, len(outputMap))
	for id := range outputMap {
		ids = append(ids, id)
	}
	sort.Slice(ids, func(i, j int) bool { return bytes.Compare(ids[i][:], ids[j][:]) < 0 })

	for _, id := range ids {
		out := outputMap[id]
		payout := new(big.Rat)
		payout.SetInt(&out.Amount)
		payout.Mul(payout, big.NewRat(1, protocol.AcmePrecision))
		fmt.Fprintf(tw, "%v\t\t\t%v\t\t\t\t%v\n", out.Url.RootIdentity(), out.Url, payout.FloatString(8))
	}

	tw.Flush()
}
