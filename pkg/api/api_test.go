package api_test

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"
	"time"

	dht "github.com/libp2p/go-libp2p-kad-dht"
	"github.com/stretchr/testify/require"
	"gitlab.com/accumulatenetwork/accumulate/pkg/api/v3/message"
	"gitlab.com/accumulatenetwork/accumulate/pkg/api/v3/p2p"
	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	acctesting "gitlab.com/accumulatenetwork/accumulate/test/testing"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/cmd/daggit/network"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/internal"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/pkg/api"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/pkg/requests"
)

func TestService(t *testing.T) {
	acctesting.SkipCI(t, "Manual")
	var err error
	svc := new(StakingServiceAPI)
	svc.Client, err = client.New("mainnet")
	require.NoError(t, err)

	info, err := svc.AccountStatus(context.Background(), protocol.AccountUrl("accumulate"), api.AccountStatusOptions{})
	require.NoError(t, err)

	fmt.Println()
	b, err := json.Marshal(info)
	require.NoError(t, err)
	fmt.Printf("%s\n", b)
}

func TestMessaging(t *testing.T) {
	acctesting.SkipCI(t, "Manual")
	var err error
	svc := new(StakingServiceAPI)
	svc.Client, err = client.New("mainnet")
	require.NoError(t, err)

	serverNode, err := p2p.New(p2p.Options{
		Network:       "MainNet",
		DiscoveryMode: dht.ModeServer,
	})
	require.NoError(t, err)
	defer serverNode.Close()

	clientNode, err := p2p.New(p2p.Options{
		Network:       "MainNet",
		DiscoveryMode: dht.ModeServer,
	})
	require.NoError(t, err)
	defer clientNode.Close()

	require.NoError(t, serverNode.ConnectDirectly(clientNode))

	serverNode.RegisterService(api.ServiceTypeDaggitron.Address(), api.NewMessageHandler(svc, nil))
	time.Sleep(time.Second)

	client := &api.MessageClient{
		Transport: &message.RoutedTransport{
			Network: "MainNet",
			Dialer:  clientNode.DialNetwork(),
		},
	}

	info, err := client.AccountStatus(context.Background(), protocol.AccountUrl("accumulate"), api.AccountStatusOptions{})
	require.NoError(t, err)

	fmt.Println()
	b, err := json.Marshal(info)
	require.NoError(t, err)
	fmt.Printf("%s\n", b)
}

type StakingServiceAPI struct {
	Client *client.Client
}

var _ api.StakingService = (*StakingServiceAPI)(nil)

func (s *StakingServiceAPI) AccountStatus(ctx context.Context, account *url.URL, _ api.AccountStatusOptions) (*api.AccountStatus, error) {
	net := network.NewWith(s.Client)
	params, err := net.GetParameters()
	if err != nil {
		return nil, errors.InternalError.WithFormat("load parameters: %w", err)
	}

	id, txid, err := internal.GetRegisteredIdentity(s.Client, params, account.RootIdentity())
	if err != nil {
		return nil, errors.UnknownError.Wrap(err)
	}

	info, err := internal.GetAccountStatus(s.Client, id, txid, account)
	if err != nil {
		return nil, errors.UnknownError.Wrap(err)
	}

	info.GetStakeBalance()
	err = info.FindLastChange()
	if err != nil {
		return nil, errors.UnknownError.Wrap(err)
	}

	return &info.AccountStatus, nil
}

func (s *StakingServiceAPI) SubmitAction(ctx context.Context, action *requests.Action) error {
	return fmt.Errorf("implement me")
}
