package fsm

import (
	"encoding/json"
	"fmt"

	"gitlab.com/accumulatenetwork/accumulate/pkg/build"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/messaging"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/accumulate/vdk/node"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/internal"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/pkg/types"
)

var _ StateDelegate = (*initiateStakingRequests)(nil)

// take the scraped data from the context, build appropriate subsequent transaction(s) and initiate.
// take the transaction hash(es) from the initiated transactions and publish to a data chain to be processed by the
// other validators.
type initiateStakingRequests struct {
}

func (s *initiateStakingRequests) execute(ctx *StateContext, d *node.DidCommitBlock) StateDelegate {
	// Sign requests. We aren't initiating the transaction so don't add a timestamp.
	key, keyType, err := ctx.Executor.Wallet.GetLockedKey()
	if err != nil {
		return ctx.Fatal(errors.UnknownError).WithFormat("failed to get key from wallet %w", err)
	}
	defer key.Destroy()

	for _, req := range ctx.toSign.FromUsers {
		env, err := build.SignatureForTransaction(req).
			Url(ctx.Executor.Params.Account.Validators).
			Version(ctx.validators.Version).
			Type(keyType).Signer(internal.SecureSigner(key)).
			Done()
		if err != nil {
			return ctx.Fatal(errors.UnknownError).Wrap(err)
		}

		ctx.Executor.Logger.Info("Signed request", "txid", req.ID())
		ctx.shouldSubmit(env)
	}

	for _, req := range ctx.toSign.UserActions {
		env, err := build.SignatureForTransaction(req).
			Url(ctx.Executor.Params.Account.Validators).
			Version(ctx.validators.Version).Timestamp(&ctx.timestamp).
			Type(keyType).Signer(internal.SecureSigner(key)).
			Done()
		if err != nil {
			return ctx.Fatal(errors.UnknownError).Wrap(err)
		}

		ctx.Executor.Logger.Info("Signed action", "txid", req.ID())
		ctx.shouldSubmit(env)
	}

	// Update registration. This loop _must_ happen after the previous one since
	// this loop adds to ctx.toSign.
	for _, up := range ctx.updates {
		env, err := s.updateRegistration(ctx, d, up)
		if err != nil {
			return ctx.Fatal(errors.UnknownError).WithFormat("update registration for %v: %w", up.Identity.Identity, err)
		}

		ctx.Executor.Logger.Info("Registered account(s)", "txid", env.Transaction[0].ID(), "identity", up.Identity.Identity)
		ctx.shouldSubmit(env)

		// Add the update to the list to make sure we wait for them to complete
		ctx.shouldSignStakingAction(env.Transaction[0])
	}

	// Submit the list of transactions to the actions data account
	{
		md := new(types.StateHeader)
		md.RootAnchor = ctx.Executor.CurrentEpochHash
		b, err := md.MarshalJSON()
		if err != nil {
			return ctx.Fatal(errors.EncodingError).WithFormat("encode staking actions metadata: %w", err)
		}

		var parts [][]byte
		for _, txn := range ctx.toSign.FromUsers {
			parts = append(parts, []byte(fmt.Sprintf("fromUser=%s", txn.ID())))
		}
		for _, txn := range ctx.toSign.StakingActions {
			parts = append(parts, []byte(fmt.Sprintf("stakingAction=%s", txn.ID())))
		}
		for _, txn := range ctx.toSign.UserActions {
			parts = append(parts, []byte(fmt.Sprintf("userAction=%s", txn.ID())))
		}

		env, err := build.Transaction().
			For(ctx.Executor.Params.Account.Actions).
			Metadata(b).
			WriteData(&protocol.DoubleHashDataEntry{Data: parts}).
			SignWith(ctx.Executor.Params.Account.Validators).
			Version(ctx.validators.Version).Timestamp(&ctx.timestamp).
			Type(keyType).Signer(internal.SecureSigner(key)).
			Done()
		if err != nil {
			return ctx.Fatal(errors.UnknownError).Wrap(err)
		}
		ctx.shouldSubmit(env)
		ctx.toSign.ActionList = env.Transaction[0]
	}

	return ctx.MakeCurrent(StateDidProcessStakingRequests)
}

func (s *initiateStakingRequests) updateRegistration(ctx *StateContext, d *node.DidCommitBlock, up *RegisteredUpdate) (*messaging.Envelope, error) {
	data, err := json.Marshal(up.Identity)
	if err != nil {
		return nil, errors.EncodingError.WithFormat("marshal registration: %w", err)
	}

	meta, err := json.Marshal(&up.StateHeader)
	if err != nil {
		return nil, errors.EncodingError.WithFormat("marshal state header metadata: %w", err)
	}

	key, keyType, err := ctx.Executor.Wallet.GetLockedKey()
	if err != nil {
		return nil, errors.UnknownError.WithFormat("failed to get key from wallet %w", err)
	}
	defer key.Destroy()

	env, err := build.Transaction().For(ctx.Executor.Params.Account.RegisteredADIs).
		Metadata(meta).WriteData(&protocol.DoubleHashDataEntry{Data: [][]byte{data}}).
		SignWith(ctx.Executor.Params.Account.Validators).
		Version(ctx.validators.Version).Timestamp(&ctx.timestamp).Type(keyType).Signer(internal.SecureSigner(key)).
		Done()
	if err != nil {
		return nil, errors.EncodingError.WithFormat("construct envelope: %w", err)
	}

	return env, nil
}
