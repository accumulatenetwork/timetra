package fsm

//go:generate go run gitlab.com/accumulatenetwork/accumulate/tools/cmd/gen-enum --package fsm --out state_gen.go state.yml

import (
	"context"
	"encoding/json"
	"sync"
	"time"

	"gitlab.com/accumulatenetwork/accumulate/exp/light"
	api_v3 "gitlab.com/accumulatenetwork/accumulate/pkg/api/v3"
	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/messaging"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/accumulate/vdk/node"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/cmd/daggit/app"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/internal"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/pkg/types"
	"golang.org/x/exp/slog"
)

type State int64

type Options struct {
	TransitionRate uint64
	Epoch          time.Duration //7 days
	KeyName        string
	Logger         *slog.Logger
	Wallet         *internal.Wallet
}

type StateDelegate interface {
	execute(e *StateContext, d *node.DidCommitBlock) StateDelegate
}

type APIv3 interface {
	api_v3.Querier
	api_v3.NetworkService
}

type StateExecutor struct {
	Options
	Params            *app.Parameters
	APIv2             *client.Client
	APIv3             APIv3
	Light             *light.Client
	CurrentEpochBlock node.DidCommitBlock     //block at the start of the current epoch in process
	CurrentStateBlock node.DidCommitBlock     //block of the current state being processed
	CurrentEpochHash  [32]byte                //anchor for the current epoch
	PrevBlock         node.DidCommitBlock     //previous block checked
	PrevEpochBlock    node.DidCommitBlock     //previous block that was processed
	PrevPayout        *url.TxID               //
	currentState      StateDelegate           //current state being executed
	States            map[State]StateDelegate //list of all states at the high level.
	context           *StateContext
}

// didInitiate returns a database batch and list of pending transaction id's initiated. If failure, then a failure
// state delegate is returned
func (s *StateExecutor) didInitiate(account *url.URL) (*light.DB, []*url.TxID, StateDelegate) {
	// Get the list of pending transactions
	err := s.Light.PullPendingTransactionsForAccount(context.TODO(), account)
	if err != nil {
		if err != nil {
			return nil, nil, s.context.Fatal(errors.UnknownError).WithFormat("pull pending transactions for %s: %w", account, err)
		}
	}

	batch := s.Light.OpenDB(false)
	ids, err := batch.Account(account).Pending().Get()
	if err != nil {
		batch.Discard()
		return nil, nil, s.context.Fatal(errors.UnknownError).WithFormat("load pending transactions for %s: %w",
			account, err)
	}
	if len(ids) == 0 {
		batch.Discard()
		// No pending transactions yet, nothing to do
		return nil, nil, s.currentState
	}

	return batch, ids, nil
}

//State diagram
//|__CanBegin
//  |__SelectLeader
//     |__CanProcessStakingRequests (sync point?)
//        |__(LeaderExecutor)
//           |__ProcessUserStakingRequests (Build Transaction Body)
//           ?__CanInitiateStakingRequests
//           |__InitiateStakingRequests (submit transaction)
//           |__DidProcessStakingRequests (i.e. wait for multisig to be processed by followers)
//              |__Success
//                 |__ComputePayoutTransaction
//                    |__Success
//                       |__InitiatePayoutsTransaction
//                    |__Fail
//  	        |__Fail
//                 |__goto SelectLeader and try again...
//
//        |__(Follower)
//           |__ProcessUserStakingRequests (Build Transaction Body)
//           |__CanProcessStakingRequestsTransaction
//              |__Success
//                 |__ProcessStakingRequestsTransaction (i.e. sign multisig)
//                    |__DidProcessStakingRequests
//                       |__Success
//                          |__ComputePayoutTransaction
//                          |__DidInitiatePayoutsTransactionRequests
//                             |__ProcessPayoutsTransaction (i.e. sign multisig)
//                                |__DidProcessPayoutsTransaction
//                                   |__Success - done
//                                   |__Fail
//                                      |__goto SelectLeader and try again
//                             |__Fail/Timeout
//                       |__Fail
//                          |__goto SelectLeader and try again
//  	        |__Fail
//                 |__goto SelectLeader and try again...
//
//

func NewStateExecutor(opts Options) *StateExecutor {
	st := StateExecutor{Options: opts}
	if st.Logger == nil {
		st.Logger = slog.Default()
	}

	st.States = map[State]StateDelegate{
		StateReset:                      new(reset),
		StateCanBegin:                   new(canBegin),
		StateSelectLeader:               new(selectLeader),
		StateProcessUserStakingRequests: new(processUserStakingRequests),
		StateInitiateStakingRequests:    new(initiateStakingRequests),
		StateDidProcessStakingRequests:  new(didProcessStakingRequests),
		StateDidInitiateStakingRequests: new(didInitiateStakingRequests),
		StateProcessStakingRequests:     new(processStakingRequests),
		StateFinalizeEpoch:              new(finalizeEpoch),
		StateInitiateFinalization:       new(initiateFinalization),
		StateDidInitiateFinalization:    new(didInitiateFinalization),
		StateProcessFinalization:        new(processFinalization),
		StateDidProcessFinalization:     new(didProcessFinalization),
		StateFatalError:                 new(fatalError),
		StateTryAgainError:              new(tryAgainError),
	}
	st.getOrCreateStateContext().MakeCurrent(StateReset)
	return &st
}

func (e *StateExecutor) Load() error {
	err := e.Light.PullAccount(context.TODO(), e.Params.Account.Epoch)
	if err != nil {
		return errors.InternalError.Wrap(err)
	}
	batch := e.Light.OpenDB(false)
	defer batch.Discard()

	acct := batch.Account(e.Params.Account.Epoch)

	// Get latest entry
	head, err := acct.MainChain().Head().Get()
	if err != nil {
		return errors.UnknownError.WithFormat("load main chain head: %w", err)
	}
	if head.Count == 0 {
		return nil
	}

	lastEntry, err := acct.MainChain().Inner().GetRange(head.Count-1, head.Count)
	if err != nil {
		return errors.UnknownError.WithFormat("load main chain entries: %w", err)
	}
	var msg *messaging.TransactionMessage

	if len(lastEntry) != 1 {
		return errors.UnknownError.WithFormat("no last entry is available")
	}
	err = batch.Message2(lastEntry[0]).Main().GetAs(&msg)
	if err != nil {
		return errors.UnknownError.WithFormat("load transaction: %w", err)
	}

	// Ignore anything that's not a data transaction
	body, ok := msg.Transaction.Body.(*protocol.WriteData)
	if !ok {
		return errors.UnknownError.WithFormat("body is not a write data entry")
	}

	data := body.Entry.GetData()[0]
	ss := types.SaveState{}
	err = ss.UnmarshalBinary(data)
	if err != nil {
		return errors.EncodingError.WithFormat("invalid save state data for the epoch")
	}
	e.PrevEpochBlock = node.DidCommitBlock{Index: ss.CommitBlock.Index, Major: ss.CommitBlock.Major, Time: ss.CommitBlock.Time}

	// Parse the metadata - assume it's garbage if it fails
	var md types.StateHeader
	err = md.UnmarshalJSON(msg.Transaction.Header.Metadata)
	if err != nil {
		e.Logger.Error("metadata not defined for loading save state", "error", err)
	}

	e.PrevBlock = e.PrevEpochBlock
	return nil
}

func (e *StateExecutor) getOrCreateStateContext() *StateContext {
	if e.context == nil {
		e.context = NewStateContext(e)
	}
	return e.context
}

func (e *StateExecutor) DidSaveSnapshot(_ node.DidSaveSnapshot) error {
	return nil
}

// Start starts a goroutine to process block events concurrently. By using a
// channel and a goroutine, Start somewhat decouples event generation from event
// processing such that delays in block processing time will not block the core
// follower, as long as the delays do not exceed the buffer.
//
// Start will not release the wait group until and will leak goroutines unless
// the context is canceled.
func (e *StateExecutor) Start(ctx context.Context, bus *node.Bus, depth int, wg *sync.WaitGroup) <-chan *messaging.Envelope {
	// Create a pipe that is closed once the context is done
	in, out := CancelableChannel[node.DidCommitBlock](ctx, depth)

	// Create a channel for outgoing envelopes
	envch := make(chan *messaging.Envelope, 10)

	// Feed events into the pipe
	node.SubscribeSync(bus, func(e node.DidCommitBlock) error {
		select {
		case <-ctx.Done():
		case in <- e:
		}
		return nil
	})

	// If a waitgroup is provided, increment it
	if wg != nil {
		wg.Add(1)
	}

	// Process events
	go func() {
		// If a waitgroup is provided, decrement it once we're done
		if wg != nil {
			defer wg.Done()
		}

		// Close the outgoing envelope channel once we're done
		defer close(envch)

		for {
			// Get the next event from the pipe
			var d node.DidCommitBlock
			select {
			case <-ctx.Done():
				return
			case d = <-out:
			}

			// Process it
			env, err := e.DidCommitBlock(d)
			if err != nil {
				e.Logger.Error("Fatal error while processing events", "error", err)
				return
			}

			// Send out the envelope if there is one
			if env != nil {
				envch <- env
			}
		}
	}()

	return envch
}

// Remarshal uses mapstructure to convert a generic JSON-decoded map into a struct.
func Remarshal(src interface{}, dst interface{}) error {
	data, err := json.Marshal(src)
	if err != nil {
		return err
	}
	return json.Unmarshal(data, dst)
}

func (s *StateExecutor) dispatchTransaction(env *messaging.Envelope) error {
	xr := new(client.ExecuteRequest)
	xr.Envelope = env
	res, err := s.APIv2.ExecuteDirect(context.TODO(), xr)
	if err != nil {
		return err
	}
	if res.Code != 0 {
		result := new(protocol.TransactionStatus)
		if Remarshal(res.Result, result) != nil {
			return errors.EncodingError.With(res.Message)
		}
		return result.Error
	}
	return nil
}

func (e *StateExecutor) DidCommitBlock(d node.DidCommitBlock) (*messaging.Envelope, error) {
	ctx := e.getOrCreateStateContext()

	// Keep processing states until executing a state does not transition to
	// another state
	for {
		ctx.CurrentStateBlock = d
		s := ctx.currentState
		e.currentState = e.currentState.execute(ctx, &d)
		if s == ctx.currentState {
			break
		}
	}

	e.PrevBlock = d

	s := e.getOrCreateStateContext()
	env := s.toSubmit
	s.toSubmit = nil
	return env, nil
}

func CancelableChannel[V any](ctx context.Context, depth int) (chan<- V, <-chan V) {
	if depth%2 == 1 {
		depth++
	}

	in := make(chan V, depth/2)
	out := make(chan V, depth/2)

	go func() {
		defer close(out)

		for {
			var v V
			select {
			case <-ctx.Done():
				return
			case v = <-in:
			}

			select {
			case <-ctx.Done():
				return
			case out <- v:
			}
		}
	}()

	return in, out
}

// findPendingTransaction scans the signer's signature chain for a signature and
// transaction that match the given predicates.
func findPendingTransaction(light *light.Client, apiv3 api_v3.Querier, signer *url.URL, checkSig func(protocol.Signature) (bool, error), checkTxn func(*protocol.Transaction) (bool, error)) (*protocol.Transaction, error) {
	// Get the list of pending transactions

	err := light.PullAccount(context.TODO(), signer)
	if err != nil {
		return nil, errors.UnknownError.WithFormat("pull pending registration transactions: %w", err)
	}
	err = light.PullMessagesForAccount(context.TODO(), signer, "signature")
	if err != nil {
		return nil, errors.UnknownError.WithFormat("pull pending registration transactions: %w", err)
	}

	// Find the leader's signature
	batch := light.OpenDB(false)
	defer batch.Discard()
	head, err := batch.Account(signer).SignatureChain().Head().Get()
	if err != nil {
		return nil, errors.UnknownError.WithFormat("load %v signature chain head: %w", signer, err)
	}

	for i := head.Count; i > 0; i-- {
		// Get the signature
		entry, err := batch.Account(signer).SignatureChain().Entry(i - 1)
		if err != nil {
			return nil, errors.UnknownError.WithFormat("load %v signature chain entry %d: %w", signer, i-1, err)
		}

		var msg messaging.MessageWithSignature
		err = batch.Message2(entry).Main().GetAs(&msg)
		if err != nil {
			return nil, errors.UnknownError.WithFormat("load %v: %w", signer.WithTxID(*(*[32]byte)(entry)), err)
		}

		// Check the signature
		ok, err := checkSig(msg.GetSignature())
		if err != nil {
			return nil, errors.UnknownError.Wrap(err)
		}
		if !ok {
			continue
		}

		// Get the transaction
		r, err := api_v3.Querier2{Querier: apiv3}.QueryTransaction(context.TODO(), msg.GetTxID(), nil)
		if err != nil {
			return nil, errors.UnknownError.WithFormat("query %v: %w", msg.GetTxID(), err)
		}

		// Check the transaction
		ok, err = checkTxn(r.Message.Transaction)
		if err != nil {
			return nil, errors.UnknownError.Wrap(err)
		}
		if ok {
			return r.Message.Transaction, nil
		}
	}

	return nil, errors.NotFound
}
