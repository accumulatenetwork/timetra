package fsm

import (
	"bytes"
	"context"
	"time"

	"github.com/AccumulateNetwork/jsonrpc2/v15"
	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/messaging"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/accumulate/vdk/node"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/internal"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/pkg/types"
)

type StateContext struct {
	Executor          *StateExecutor
	AmLeader          bool
	AmValidator       bool
	CurrentStateBlock node.DidCommitBlock
	currentState      State
	lastError         error
	retryAttempts     int
	leader            *protocol.KeySpec
	validators        *protocol.KeyPage
	leaderPool        []*protocol.KeySpec
	updates           []*RegisteredUpdate
	toSign            *TxnToSign
	payout            []*protocol.TokenRecipient
	payoutTx          *protocol.Transaction
	toSubmit          *messaging.Envelope
	toFinalize        *protocol.Transaction
	timestamp         uint64
	executorVersion   protocol.ExecutorVersion
}

// RegisteredUpdate is a pending update to the registration list.
type RegisteredUpdate struct {
	types.StateHeader
	Identity *types.Identity
}

type TxnToSign struct {
	// FromUsers are transactions submitted by users such as staking authority
	// transfers
	FromUsers []*protocol.Transaction

	// UserActions are the individual actions for the epoch on the user's account
	UserActions []*protocol.Transaction

	// StakingActions are the individual actions for the epoch on the staking account
	StakingActions []*protocol.Transaction

	// ActionList is the list of actions for the epoch
	ActionList *protocol.Transaction
}

func (t *TxnToSign) all() []*protocol.Transaction {
	all := make([]*protocol.Transaction, 1+len(t.FromUsers)+len(t.StakingActions)+len(t.UserActions))
	all[0] = t.ActionList
	n := copy(all[1:], t.FromUsers)
	n += copy(all[n+1:], t.StakingActions)
	copy(all[n+1:], t.UserActions)
	return all
}

func NewStateContext(exec *StateExecutor) *StateContext {
	sc := new(StateContext)
	sc.Executor = exec
	sc.currentState = StateCanBegin
	return sc
}

// reset clears the context's transient state fields.
func (s *StateContext) reset() {
	s.CurrentStateBlock = node.DidCommitBlock{Init: false, Time: time.UnixMilli(0), Major: 0}
	s.leaderPool = nil
	s.validators = nil
	s.resetForTryAgain()
}

// resetForTryAgain clears the subset of the context's transient state fields
// that should not be preserved across retries.
func (s *StateContext) resetForTryAgain() {
	s.AmLeader = false
	s.leader = nil
	s.updates = nil
	s.toSign = nil
	s.payout = nil
	s.payoutTx = nil
}

func (s *StateContext) GetCurrent() State {
	return s.currentState
}

func (s *StateContext) MakeCurrent(state State) StateDelegate {
	s.Executor.Logger.Info("Transition", "to", state, "leader", s.AmLeader, "major", s.Executor.CurrentEpochBlock.Major, "minor", s.CurrentStateBlock.Index)
	s.currentState = state
	s.Executor.currentState = s.Executor.States[state]
	return s.Executor.currentState
}

func (s *StateContext) makeCurrentError(state State, err error) StateDelegate {
	switch state {
	case StateFatalError:
		s.Executor.Logger.Error("Fatal error", "error", err, "state", s.currentState)
	default:
		s.Executor.Logger.Error("Error", "error", err, "state", s.currentState)
	}
	s.lastError = err
	return s.MakeCurrent(state)
}

// didUpdate records a pending update to the registration list.
func (s *StateContext) didUpdate(req *url.TxID, id *types.Identity) {
	ptr, _ := internal.BinaryInsert(&s.updates, func(other *RegisteredUpdate) int {
		return other.Identity.Identity.Compare(id.Identity)
	})
	up := *ptr
	if up == nil {
		up = new(RegisteredUpdate)
		up.RootAnchor = s.Executor.CurrentEpochHash
		*ptr = up
	}
	up.Identity = id

	p2, _ := internal.BinaryInsert(&up.Transactions, func(other *url.TxID) int {
		return other.Compare(req)
	})
	*p2 = req
}

// shouldSignFromUser records a transaction from a user that should be signed.
func (s *StateContext) shouldSignFromUser(txn *protocol.Transaction) {
	ptr, _ := internal.BinaryInsert(&s.toSign.FromUsers, func(other *protocol.Transaction) int {
		return bytes.Compare(other.GetHash(), txn.GetHash())
	})
	*ptr = txn
}

// shouldSignAction records a staking action that should be signed.
func (s *StateContext) shouldSignUserAction(txn *protocol.Transaction) {
	ptr, _ := internal.BinaryInsert(&s.toSign.UserActions, func(other *protocol.Transaction) int {
		return bytes.Compare(other.GetHash(), txn.GetHash())
	})
	*ptr = txn
}

// shouldSignStakingAction records a staking action that should be signed.
func (s *StateContext) shouldSignStakingAction(txn *protocol.Transaction) {
	ptr, _ := internal.BinaryInsert(&s.toSign.StakingActions, func(other *protocol.Transaction) int {
		return bytes.Compare(other.GetHash(), txn.GetHash())
	})
	*ptr = txn
}

// shouldSubmit records a signature that should be submitted.
func (s *StateContext) shouldSubmit(env *messaging.Envelope) {
	if s.toSubmit == nil {
		s.toSubmit = new(messaging.Envelope)
	}
	s.toSubmit.Transaction = append(s.toSubmit.Transaction, env.Transaction...)
	s.toSubmit.Signatures = append(s.toSubmit.Signatures, env.Signatures...)
}

func (s *StateContext) txnIsDone(id *url.TxID) (bool, error) {
	res, err := s.Executor.APIv2.QueryTx(context.TODO(), &client.TxnQuery{TxIdUrl: id})
	if err != nil {
		// Not found?
		var jerr jsonrpc2.Error
		if errors.As(err, &jerr) &&
			jerr.Code == client.ErrCodeNotFound {
			return false, nil
		}

		return false, errors.UnknownError.WithFormat("get status of %v: %w", id, err)
	}

	if res.Status.Failed() {
		return false, errors.UnknownError.WithFormat("transaction %v failed: %w", id, res.Status.AsError())
	}

	return res.Status.Code == errors.Delivered, nil
}
