package internal

import (
	"context"
	"crypto/ed25519"
	"crypto/sha256"
	"fmt"
	"os"

	"github.com/awnumar/memguard"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/db"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd"
	api2 "gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/wallet"
)

type Key struct {
	KeyName   string            //name of key stored in wallet
	Key       api2.Key          //do not use the PrivateKey inside this struct
	PkEnclave *memguard.Enclave //use this instead
	KeyHash   []byte
}

type Wallet struct {
	pwEnclave *memguard.Enclave
	client    *wallet.Client
	KeyStore  Key
}

// authenticator because the password is stored persistently in memory, it uses a memory guard to add extra security
// to the collected password
func (a *Wallet) authenticate(ar *api2.AuthenticationRequired) ([]byte, error) {
	if a.pwEnclave != nil {
		locked, err := a.pwEnclave.Open()
		if err == nil {
			return locked.Bytes(), nil
		}
	}

	name := ar.Vault.Name
	if name == "" {
		name = ar.Vault.FilePath
	}

	short := "Password: "
	prompt := fmt.Sprintf("Unlock %s vault", name)
	if !ar.Exists {
		short = "New password: "
		prompt = fmt.Sprintf("Create %s vault", name)
	}
	pass, err := walletd.GetPassword(prompt, short, os.Stdin, os.Stderr, true)
	if err != nil {
		return nil, db.ErrNoPassword
	}

	a.pwEnclave = memguard.NewEnclave(pass)
	memguard.WipeBytes(pass)
	locked, err := a.pwEnclave.Open()
	if err != nil {
		return nil, err
	}

	return locked.Bytes(), nil
}

func (w *Wallet) unlock() error {
	unlock := api2.UnlockVaultRequest{}
	//this will prompt user for password and secure it in memory
	unlockResp, err := w.client.UnlockVault(context.TODO(), &unlock)
	if err != nil {
		return errors.UnknownError.Wrap(err)
	}
	if !unlockResp.Success {
		return errors.UnknownError.With("an error unlocking database occurred")
	}
	return nil
}

func (w *Wallet) lock() error {
	//lock it
	lock := api2.LockVaultRequest{}
	lock.Close = true
	_, err := w.client.LockVault(context.TODO(), &lock)
	if err != nil {
		return err
	}

	return nil
}

func (w *Wallet) resolveKey(keyName string) error {
	req := api2.ResolveKeyRequest{}
	req.Value = keyName
	req.IncludePrivateKey = true
	resp, err := w.client.ResolveKey(context.TODO(), &req)
	if err != nil {
		return err
	}
	w.KeyStore.Key.KeyInfo = *resp.Info
	w.KeyStore.PkEnclave = memguard.NewEnclave(resp.PrivateKey)
	w.KeyStore.Key.PublicKey = resp.PublicKey
	w.KeyStore.Key.Labels = resp.Labels
	w.KeyStore.KeyHash = resp.PublicKeyHash
	memguard.WipeBytes(resp.PrivateKey)
	return nil
}

func NewWalletFromKey(key ed25519.PrivateKey) *Wallet {
	wallet := new(Wallet)
	wallet.KeyStore.Key.PublicKey = make([]byte, 32)
	copy(wallet.KeyStore.Key.PublicKey, key[32:])
	wallet.KeyStore.PkEnclave = memguard.NewEnclave(key)
	h := sha256.Sum256(wallet.KeyStore.Key.PublicKey)
	wallet.KeyStore.KeyHash = h[:]
	wallet.KeyStore.Key.KeyInfo.Type = protocol.SignatureTypeED25519
	return wallet
}

func NewWallet(walletAddress string, keyName string) (w *Wallet, err error) {
	w = new(Wallet)
	w.client, err = wallet.New(walletAddress)
	if err != nil {
		return nil, err
	}

	w.client.Transport = &wallet.InteractiveAuthnTransport{
		Transport:   w.client.Transport,
		GetPassword: w.authenticate,
	}

	err = w.resolveKey(keyName)
	if err != nil {
		return nil, err
	}

	return w, nil
}

func (w *Wallet) GetLockedKey() (*memguard.LockedBuffer, protocol.SignatureType, error) {
	locked, err := w.KeyStore.PkEnclave.Open()
	if err != nil {
		return nil, protocol.SignatureTypeUnknown, err
	}
	return locked, w.KeyStore.Key.KeyInfo.Type, nil
}

func (w *Wallet) GetKey() ([]byte, protocol.SignatureType, error) {
	if w.KeyStore.Key.PrivateKey == nil {
		return nil, protocol.SignatureTypeUnknown, errors.FatalError.With("wallet has not been unlocked")
	}
	return w.KeyStore.Key.PrivateKey, w.KeyStore.Key.KeyInfo.Type, nil
}

func (w *Wallet) GetPublicKey() []byte {
	return w.KeyStore.Key.PublicKey
}

func (w *Wallet) GetKeyInfo() *api2.KeyInfo {
	return &w.KeyStore.Key.KeyInfo
}

func (w *Wallet) GetKeyHash() []byte {
	return w.KeyStore.KeyHash
}

func (w *Wallet) Open() error {
	lockedKey, err := w.KeyStore.PkEnclave.Open()
	if err != nil {
		return err
	}

	w.KeyStore.Key.PrivateKey = make([]byte, len(lockedKey.Data()))
	copy(w.KeyStore.Key.PrivateKey, lockedKey.Data())
	return nil
}

func (w *Wallet) Close() error {
	memguard.WipeBytes(w.KeyStore.Key.PrivateKey)
	w.KeyStore.Key.PrivateKey = []byte{}
	return nil
}
