package fsm

import (
	"gitlab.com/accumulatenetwork/accumulate/vdk/node"
)

var _ StateDelegate = (*fatalError)(nil)

type fatalError struct {
}

func (s *fatalError) execute(ctx *StateContext, d *node.DidCommitBlock) StateDelegate {
	//store the ctx.Executor.CurrentEpochBlock in the data account to mark success.
	//todo: determine if any extra data is needed to be stored.
	ctx.Executor.PrevBlock = ctx.Executor.CurrentEpochBlock

	// If we've reached a fatal error we need human intervention, so stop
	// progressing the state machine
	return s
}
