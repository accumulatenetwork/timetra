package types

//go:generate go run gitlab.com/accumulatenetwork/accumulate/tools/cmd/gen-enum --package types enums.yml
//go:generate go run gitlab.com/accumulatenetwork/accumulate/tools/cmd/gen-types --package types types.yml

// AccountType is the stake type of a staking account.
type AccountType uint64

// AcceptingDelegates indicates if a full staker is accepting delegates.
type AcceptingDelegates uint64

// Status is the status of a staking account.
type Status uint64
