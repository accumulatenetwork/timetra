// Copyright 2023 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

package node

import (
	"github.com/kardianos/service"
	"gitlab.com/accumulatenetwork/accumulate/vdk/logger"
	"gitlab.com/accumulatenetwork/accumulate/vdk/node"
)

type Options struct {
	WorkDir    string
	NodeId     int
	LogFile    string
	WalletAddr string
	KeyName    string
}

type Program struct {
	Options
	Node *node.Daemon
}

func NewProgram(opts Options) *Program {
	p := new(Program)
	p.Options = opts
	return p
}

func (p *Program) Start(s service.Service) (err error) {
	lwc := logger.LogWriterConfig{Service: s, LogFile: p.LogFile}
	lw, err := logger.NewLogWriter(lwc)
	if err != nil {
		return err
	}

	p.Node, err = node.NewNode(p.WorkDir, lw, p.NodeId)
	if err != nil {
		return err
	}

	return p.Node.Start()
}

func (p *Program) Stop(service.Service) error {
	return p.Node.Stop()
}
