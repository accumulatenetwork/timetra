package internal

import (
	"context"
	"encoding/json"
	"fmt"

	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/cmd/daggit/app"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/pkg/types"
)

func GetRegisteredIdentity(c *client.Client, params *app.Parameters, idUrl *url.URL) (*types.Identity, *url.TxID, error) {
	idUrl = idUrl.RootIdentity()

	req := new(client.DataEntrySetQuery)
	req.Url = params.Account.RegisteredADIs
	req.Expand = true

	// Get the total number of entries
	req.Count = 1
	res, err := c.QueryDataSet(context.Background(), req)
	if err != nil {
		return nil, nil, errors.InternalError.WithFormat("query %v data set: %w", req.Url, err)
	}

	// Scan entries in reverse order
	x := res.Total
	const N = 50
	for x > 0 {
		if x < N {
			req.Start = 0
			req.Count = x
			x = 0
		} else {
			req.Start = x - N
			req.Count = N
			x -= N
		}

		res, err := c.QueryDataSet(context.Background(), req)
		if err != nil {
			return nil, nil, errors.InternalError.WithFormat("query %v data set: %w", req.Url, err)
		}

		for i := len(res.Items) - 1; i >= 0; i-- {
			entry, err := remarshal[client.ResponseDataEntry](res.Items[i])
			if err != nil {
				return nil, nil, errors.UnknownError.Wrap(err)
			}

			id := new(types.Identity)
			err = json.Unmarshal(entry.Entry.GetData()[0], id)
			if err != nil {
				return nil, nil, errors.EncodingError.WithFormat("decode response value: %w", err)
			}
			if id.Identity == nil {
				// Bad entry, skip it
				fmt.Printf("Skipping bad entry %s\n", entry.Entry.GetData()[0])
				continue
			}

			id.Normalize()

			if idUrl.Equal(id.Identity) {
				return id, entry.TxId, nil
			}
		}
	}
	return nil, nil, errors.NotFound.WithFormat("no staking account found for %v", idUrl)
}

func NewAccountStatus(c *client.Client, txid *url.TxID, account *types.Account) *AccountStatus {
	info := new(AccountStatus)
	info.client = c
	info.Account = account
	info.Registered = txid
	return info
}

func GetAccountStatus(c *client.Client, identity *types.Identity, txid *url.TxID, account *url.URL) (*AccountStatus, error) {
	for _, a := range identity.Accounts {
		if a.Url.Equal(account) {
			return NewAccountStatus(c, txid, a), nil
		}
	}
	return nil, errors.NotFound.WithFormat("no staking account found for %v", account)
}

func ScanRegistered(c *client.Client, params *app.Parameters) ([]*AccountStatus, error) {
	req := new(client.DataEntrySetQuery)
	req.Url = params.Account.RegisteredADIs
	req.Expand = true

	// Get the total number of entries
	req.Count = 1
	res, err := c.QueryDataSet(context.Background(), req)
	if err != nil {
		return nil, errors.UnknownError.WithFormat("query %v: %w", req.Url, err)
	}

	// Track accounts that have been seen
	seen := map[[32]byte]bool{}

	// Scan entries in reverse order
	x := res.Total
	var all []*AccountStatus
	const N = 50
	for x > 0 {
		if x < N {
			req.Start = 0
			req.Count = x
			x = 0
		} else {
			req.Start = x - N
			req.Count = N
			x -= N
		}

		res, err := c.QueryDataSet(context.Background(), req)
		if err != nil {
			return nil, errors.UnknownError.WithFormat("query %v: %w", req.Url, err)
		}

		for i := len(res.Items) - 1; i >= 0; i-- {
			// Remarshal from any to the response type
			entry, err := remarshal[client.ResponseDataEntry](res.Items[i])
			if err != nil {
				return nil, errors.UnknownError.Wrap(err)
			}

			// Unmarshal the account
			identity := new(types.Identity)
			err = json.Unmarshal(entry.Entry.GetData()[0], identity)
			if err != nil {
				return nil, errors.EncodingError.WithFormat("decode response value: %w", err)
			}
			if identity.Identity == nil {
				// Bad entry, skip it
				continue
			}
			identity.Normalize()

			// Ignore older records for the same account
			id := identity.Identity.IdentityAccountID32()
			if seen[id] {
				continue
			}
			seen[id] = true

			for _, a := range identity.Accounts {
				info := new(AccountStatus)
				info.Account = a
				info.Registered = entry.TxId
				info.client = c

				all = append(all, info)
			}
		}
	}

	return all, nil
}
