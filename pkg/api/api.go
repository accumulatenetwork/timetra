package api

import (
	"context"
	"io"
	"runtime/debug"

	"github.com/libp2p/go-libp2p/core/network"
	"gitlab.com/accumulatenetwork/accumulate/pkg/api/v3/message"
	"gitlab.com/accumulatenetwork/accumulate/pkg/api/v3/p2p"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/ecosystem/daggitron/pkg/requests"
	"golang.org/x/exp/slog"
)

type StakingService interface {
	AccountStatus(context.Context, *url.URL, AccountStatusOptions) (*AccountStatus, error)
	SubmitAction(ctx context.Context, action *requests.Action) error
}

func NewMessageHandler(svc StakingService, logger *slog.Logger) p2p.MessageStreamHandler {
	if logger == nil {
		logger = slog.Default()
	}
	return func(s message.Stream) {
		// Panic protection
		defer func() {
			if r := recover(); r != nil {
				if err, ok := r.(error); ok {
					logger.Error("Panicked while handling stream", "error", err, "stack", debug.Stack())
				} else {
					logger.Error("Panicked while handling stream", "error", r, "stack", debug.Stack())
				}
			}
		}()

		// Gotta have that context 👌
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		for {
			// Read the next request
			req, err := s.Read()
			switch {
			case err == nil:
				// Ok

			case errors.Is(err, io.EOF),
				errors.Is(err, network.ErrReset),
				errors.Is(err, context.Canceled):
				// Done
				return

			default:
				logger.Error("Unable to decode request from peer", "error", err)
				return
			}

			// Strip off addressing
			for {
				if r, ok := req.(*message.Addressed); ok {
					req = r.Message
				} else {
					break
				}
			}

			// Dispatch the call
			var res Message
			switch req := req.(type) {
			case *AccountStatusRequest:
				r := new(AccountStatusResponse)
				r.Value, err = svc.AccountStatus(ctx, req.Account, req.AccountStatusOptions)
				res = r
			default:
				res = &message.ErrorResponse{Error: errors.NotAllowed.WithFormat("%v not supported", req.Type())}
			}
			if err != nil {
				res = &message.ErrorResponse{Error: errors.UnknownError.Wrap(err).(*errors.Error)}
			}

			// Send the response
			err = s.Write(res)
			if err != nil {
				logger.Error("Unable to send response to peer", "error", err)
				return
			}
		}
	}
}

type MessageClient struct {
	Transport message.Transport
}

// AccountStatus implements [api.StakingService.AccountStatus].
func (c *MessageClient) AccountStatus(ctx context.Context, account *url.URL, opts AccountStatusOptions) (*AccountStatus, error) {
	// Wrap the request as an AccountStatusRequest and expect an
	// AccountStatusResponse, which is unpacked into a AccountStatus
	return typedRequest[*AccountStatusResponse, *AccountStatus](c.Transport, ctx, &AccountStatusRequest{Account: account, AccountStatusOptions: opts})
}

// typedRequest executes a round-trip call, sending the request and expecting a
// response of the given type.
func typedRequest[M response[T], T any](t message.Transport, ctx context.Context, req Message) (T, error) {
	// Address the message to the staking service
	req = &message.Addressed{
		Address: ServiceTypeDaggitron.Address().Multiaddr(),
		Message: req,
	}

	var typRes M
	var errRes *message.ErrorResponse
	err := t.RoundTrip(ctx, []Message{req}, func(res, _ Message) error {
		switch res := res.(type) {
		case *message.ErrorResponse:
			errRes = res
			return nil
		case M:
			typRes = res
			return nil
		default:
			return errors.Conflict.WithFormat("invalid response type %T", res)
		}
	})
	var z T
	if err != nil {
		return z, err
	}
	if errRes != nil {
		return z, errRes.Error
	}
	return typRes.rval(), nil
}

// response exists for typedRequest.
type response[T any] interface {
	Message
	rval() T
}

func (r *AccountStatusResponse) rval() *AccountStatus { return r.Value } //nolint:unused
